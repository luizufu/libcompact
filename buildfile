./: {*/ -build/ -benchmarks/} manifest

# Don't install tests.
#
tests/: install = false
benchmarks/: install = false
