#include <libculcompact/bit-vector.hxx>

#include <chrono>
#include <vector>
#include <array>
#include <map>
#include <string>
#include <iostream>
#include <functional>
#include <random>
#include <bitset>
#include <utility>
#include <tuple>
#include <fstream>

#define RUNS 1000

using namespace std;
using namespace culcompact;


template <typename Func>
struct named_func
{
    std::string name;
    Func func;

    auto operator()()
    {
        return func();
    }
};

struct csv_row
{
    size_t percentage;
    size_t samples;
    std::string data_structure;
    std::string algoritm;

    friend std::ostream& operator<<(ostream& out, const csv_row& row)
    {
        out << row.percentage << ", "
            << row.samples << ", \""
            << row.algoritm << "\", \""
            << row.data_structure << "\"";

        return out;
    }
};


struct csv_time_row
{
    csv_row id;
    double time;

    friend std::ostream& operator<<(ostream& out, const csv_time_row& row)
    {
        out << row.id << ", " << row.time;

        return out;
    }
} time_row;

struct csv_space_row
{
    csv_row id;
    uint64_t space;

    friend std::ostream& operator<<(ostream& out, const csv_space_row& row)
    {
        out << row.id << ", " << row.space;
        return out;
    }
} space_row;

using space_f = named_func<std::function<uint64_t()>>;
using time_f = named_func<std::function<void()>>;


void SBENCHMARK(const char* title, std::vector<space_f> fs, std::ostream& out)
{
    std::cout << "    Benchmark: " << title << "\n";
    space_row.id.algoritm = title;

    for(auto f : fs)
    {
        std::cout << "      Running " << f.name << "...\n";

        for(size_t i = 0; i < RUNS; ++i)
        {
            auto start = chrono::steady_clock::now();
            uint64_t bytes = f();
            f();
            auto end = chrono::steady_clock::now();

            auto tmp = space_row;
            tmp.id.data_structure = f.name;
            tmp.space = chrono::duration<double, milli>(end - start).count();
            out << tmp << '\n';
        }
    }
}

void TBENCHMARK(const char* title, std::vector<time_f> fs, std::ostream& out)
{
    std::cout << "    Benchmark: " << title << "\n";
    time_row.id.algoritm = title;


    for(auto f : fs)
    {
        std::cout << "      Running " << f.name << "...\n";

        for(size_t i = 0; i < RUNS; ++i)
        {
            auto start = chrono::steady_clock::now();
            f();
            auto end = chrono::steady_clock::now();

            auto tmp = time_row;
            tmp.id.data_structure = f.name;
            tmp.time = chrono::duration<double, milli>(end - start).count();
            out << tmp << '\n';
        }
    }
}

std::random_device rd;
std::mt19937 gen(rd());

template <size_t P1 = 50>
bool next_bit_with_p1()
{
    static std::discrete_distribution<> d({100 - P1, P1});
    return (bool) d(gen);
}

template <size_t P1>
std::vector<bool> make_bits(size_t n)
{
    std::vector<bool> bits(n);

    for(size_t i = 0; i < n; ++i)
        bits[i] = next_bit_with_p1<P1>();

    return bits;
}

#define USE

template <typename T>
void NOT_OPTIMIZE(T&& t)
{
    static size_t useless __attribute__((__used__)) = t.size();
}

template <size_t P1, size_t N>
void run_space_test(std::ostream& out)
{
    std::cout << "  " << N << " bits"  << std::endl;
    time_row.id.percentage = P1;
    time_row.id.samples = N;

    auto bits = make_bits<P1>(N);
    SBENCHMARK("bit construction", {
        {"vector<bool>",
            [&bits]() {
                vector<bool> tmp(bits.begin(), bits.end());
                return tmp.size() * sizeof(bool);
            }
        },
        {"plain_bit_vector<uint32_t>",
            [&bits]() {
                plain_bit_vector<uint32_t, N> tmp(bits.begin(), bits.end());
                return tmp.size_of();
            }
        },
    }, out);
}

template <size_t P1, size_t N>
void run_time_test(std::ostream& out)
{
    std::cout << "  " << N << " bits"  << std::endl;
    time_row.id.percentage = P1;
    time_row.id.samples = N;

    auto bits = make_bits<P1>(N);
    TBENCHMARK("bit construction", {
        {"vector<bool>",
            [&bits]() {
                NOT_OPTIMIZE(
                    vector<bool>(bits.begin(), bits.end()));
            }
        },
        {"plain_bit_vector<uint32_t>",
            [&bits]() {
                NOT_OPTIMIZE(
                    plain_bit_vector<uint32_t, N>(bits.begin(), bits.end()));
            }
        },
    }, out);
}

int main()
{
    {
        ofstream out("bit_time_benchmark.csv");
        out << "\"one_percentage\", \"n_samples\", \"algorithm\", \"data_structure\", \"time\"\n";

        std::cout << "Bit  Time Benchmark\n";
        std::cout << "10% 1's\n";
        run_time_test<10, 100>(out);
        run_time_test<10, 10000>(out);
        run_time_test<10, 100000>(out);
        run_time_test<10, 1000000>(out);

        std::cout << "30% 1's\n";
        run_time_test<30, 100>(out);
        run_time_test<30, 10000>(out);
        run_time_test<30, 100000>(out);
        run_time_test<30, 1000000>(out);

        std::cout << "50% 1's\n";
        run_time_test<50, 100>(out);
        run_time_test<50, 10000>(out);
        run_time_test<50, 100000>(out);
        run_time_test<50, 1000000>(out);

        std::cout << "70% 1's\n";
        run_time_test<70, 100>(out);
        run_time_test<70, 10000>(out);
        run_time_test<70, 100000>(out);
        run_time_test<70, 1000000>(out);

        std::cout << "90% 1's\n";
        run_time_test<90, 100>(out);
        run_time_test<90, 10000>(out);
        run_time_test<90, 100000>(out);
        run_time_test<90, 1000000>(out);
    }

    {
        ofstream out("bit_space_benchmark.csv");
        out << "\"one_percentage\", \"n_samples\", \"algorithm\", \"data_structure\", \"space\"\n";

        std::cout << "Bit Space Benchmark\n";
        std::cout << "10% 1's\n";
        run_space_test<10, 100>(out);
        run_space_test<10, 10000>(out);
        run_space_test<10, 100000>(out);
        run_space_test<10, 1000000>(out);

        std::cout << "30% 1's\n";
        run_space_test<30, 100>(out);
        run_space_test<30, 10000>(out);
        run_space_test<30, 100000>(out);
        run_space_test<30, 1000000>(out);

        std::cout << "50% 1's\n";
        run_space_test<50, 100>(out);
        run_space_test<50, 10000>(out);
        run_space_test<50, 100000>(out);
        run_space_test<50, 1000000>(out);

        std::cout << "70% 1's\n";
        run_space_test<70, 100>(out);
        run_space_test<70, 10000>(out);
        run_space_test<70, 100000>(out);
        run_space_test<70, 1000000>(out);

        std::cout << "90% 1's\n";
        run_space_test<90, 100>(out);
        run_space_test<90, 10000>(out);
        run_space_test<90, 100000>(out);
        run_space_test<90, 1000000>(out);
    }
}
