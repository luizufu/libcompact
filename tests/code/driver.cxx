#include <tests/test.hxx>
#include <tests/type-name.hxx>

#include <tests/code/data.hxx>

#include <libculcompact/code/unary-coder.hxx>
#include <libculcompact/code/gamma-coder.hxx>
#include <libculcompact/code/delta-coder.hxx>
#include <libculcompact/code/rice-coder.hxx>
#include <libculcompact/code/shannonfano-coder.hxx>
#include <libculcompact/code/huffman-coder.hxx>
#include <libculcompact/code/chuffman-coder.hxx>
#include <libculcompact/code/vbyte-coder.hxx>


using namespace std;
using namespace culcompact;


template <typename Coder, typename Codes, typename Vec, typename... Args>
void run_tests(const Codes& codes, const Vec& vec, Args... args)
{
    std::cout << "Testing " << type_name<Coder>() << "\n";

    TEST("encode(size_t)",
         [coder = Coder(args...), &codes]() {
             for(size_t i = 0; i < numbers.size(); ++i)
                 ASSERT_EQ(coder.encode(numbers[i]), codes[i]);
         });

    TEST("decode(code)",
         [coder = Coder(args...), &codes]() {
             for(size_t i = 0; i < numbers.size(); ++i)
                 ASSERT_EQ(coder.decode(codes[i]), numbers[i]);
         });

    TEST("decode_next(bit_vector, &i)",
         [coder = Coder(args...), &vec]() {
             size_t pos = 0;
             for(size_t i = 0; i < numbers.size(); ++i)
                 ASSERT_EQ(coder.decode_next(vec, pos), numbers[i]);
         });

    TEST("skip_next(bit_vector, &i)",
         [coder = Coder(args...), &vec]() {
             size_t pos = 0;
             for(size_t i = 0; i < numbers.size(); ++i)
                if(i % 3 != 0)
                    coder.skip_next(vec, pos);
                else
                    ASSERT_EQ(coder.decode_next(vec, pos), numbers[i]);
         });

    std::cout << std::endl;
}

template <typename Coder, typename LenOrProb>
void run_tests2(const LenOrProb& len_or_prob)
{
    std::cout << "Testing " << type_name<Coder>() << "\n";

    Coder coder(symbols, len_or_prob);

    std::vector<culcompact::code<int_t>> codes;
    plain_bit_vector<size_t> vec;

    for(auto symbol : symbols)
    {
        codes.push_back(coder.encode(symbol));
        size_t size = vec.size();
        vec.resize(size + codes.back().length);
        vec.write_int(size, size + codes.back().length, codes.back().value);
    }

    TEST("encode(size_t)",
         [coder = Coder(symbols, len_or_prob), &codes]() {
             for(size_t i = 0; i < symbols.size(); ++i)
                 ASSERT_EQ(coder.encode(symbols[i]), codes[i]);
         });

    TEST("decode(code)",
         [coder = Coder(symbols, len_or_prob), &codes]() {
             for(size_t i = 0; i < codes.size(); ++i)
                 ASSERT_EQ(coder.decode(codes[i]), symbols[i]);
         });

    TEST("decode_next(bit_vector, &i)",
         [coder = Coder(symbols, len_or_prob), &vec]() {
             size_t pos = 0;
             for(size_t i = 0; i < symbols.size(); ++i)
                 ASSERT_EQ(coder.decode_next(vec, pos), symbols[i]);
         });

    TEST("skip_next(bit_vector, &i)",
         [coder = Coder(symbols, len_or_prob), &vec]() {
             size_t pos = 0;
             for(size_t i = 0; i < symbols.size(); ++i)
                if(i % 3 != 0)
                    coder.skip_next(vec, pos);
                else
                    ASSERT_EQ(coder.decode_next(vec, pos), symbols[i]);
         });

    std::cout << std::endl;
}


int main()
{
    run_tests<unary_coder<int_t>>(unary_codes, unary_vec);
    run_tests<gamma_coder<int_t>>(gamma_codes, gamma_vec);
    run_tests<delta_coder<int_t>>(delta_codes, delta_vec);
    run_tests<rice_coder<int_t>>(rice_codes, rice_vec, 4);
    run_tests2<shannonfano_coder<int_t>>(lengths);
    run_tests2<huffman_coder<int_t>>(probs);
    run_tests2<chuffman_coder<int_t>>( probs);
    run_tests<vbyte_coder<int_t>>(vbyte_codes, delta_vec);
}

