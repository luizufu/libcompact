#include <cstdint>
#include <array>

#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/code/block-coder.hxx>
#include <tests/test.hxx>
#include <tests/bit/data.hxx>

using namespace std;
using namespace culcompact;


using block_t = plain_bit_vector<>;
using code_t = block_code<size_t>;

const std::array<block_t, 16> blocks = {
    block_t{ 0, 0, 0, 0 },

    block_t{ 0, 0, 0, 1 },
    block_t{ 0, 0, 1, 0 },
    block_t{ 0, 1, 0, 0 },
    block_t{ 1, 0, 0, 0 },

    block_t{ 0, 0, 1, 1 },
    block_t{ 0, 1, 0, 1 },
    block_t{ 0, 1, 1, 0 },
    block_t{ 1, 0, 0, 1 },
    block_t{ 1, 0, 1, 0 },
    block_t{ 1, 1, 0, 0 },

    block_t{ 0, 1, 1, 1 },
    block_t{ 1, 0, 1, 1 },
    block_t{ 1, 1, 0, 1 },
    block_t{ 1, 1, 1, 0 },

    block_t{ 1, 1, 1, 1 }
};

const plain_bit_vector<> bit_container = {
     0, 0, 0, 0,

     0, 0, 0, 1,
     0, 0, 1, 0,
     0, 1, 0, 0,
     1, 0, 0, 0,

     0, 0, 1, 1,
     0, 1, 0, 1,
     0, 1, 1, 0,
     1, 0, 0, 1,
     1, 0, 1, 0,
     1, 1, 0, 0,

     0, 1, 1, 1,
     1, 0, 1, 1,
     1, 1, 0, 1,
     1, 1, 1, 0,

     1, 1, 1, 1
};

constexpr std::array<code_t, 16> codes = {
    code_t{ 0, 0 },

    code_t{ 1, 0 },
    code_t{ 1, 1 },
    code_t{ 1, 2 },
    code_t{ 1, 3 },

    code_t{ 2, 0 },
    code_t{ 2, 1 },
    code_t{ 2, 2 },
    code_t{ 2, 3 },
    code_t{ 2, 4 },
    code_t{ 2, 5 },

    code_t{ 3, 0 },
    code_t{ 3, 1 },
    code_t{ 3, 2 },
    code_t{ 3, 3 },

    code_t{ 4, 0 },
};

void run_block_coder_tests()
{
    std::cout << "Testing block_coder<T>\n";

    block_coder<size_t> coder(4);

    TEST("block_coder<T>::encode(BitContainer&)",
         [coder, bs = blocks, cs = codes]() {
             for(size_t i = 0; i < blocks.size(); ++i)
             {
                 auto code = coder.encode(bs[i]);
                 ASSERT_EQ(code.c, cs[i].c);
                 ASSERT_EQ(code.o, cs[i].o);
             }
         });

    TEST("block_coder<T>::encode_next(BitContainer&)",
         [coder, bc = bit_container, cs = codes]() {
            size_t k = 0;
             for(size_t i = 0; i < cs.size(); ++i)
             {
                 auto code = coder.encode_next(bc, k);
                 ASSERT_EQ(code.c, cs[i].c);
                 ASSERT_EQ(code.o, cs[i].o);
             }
         });

    TEST("block_coder<T>::decode(block_code&)",
         [coder, bs = blocks, cs = codes]() {
             for(size_t i = 0; i < blocks.size(); ++i)
             {
                 auto block = coder.decode(cs[i]);

                 for(size_t k = 0; k < bs[i].size(); ++k)
                     ASSERT_EQ(block.read_bit(k), bs[i].read_bit(k));
             }
         });

    TEST("block_coder<T>::skip_next(BitContainer&)",
         [coder, bc = bit_container, cs = codes]() {
            size_t k = 0;
             for(size_t i = 0; i < cs.size(); ++i)
             {
                if(i % 3 == 0)
                {
                    auto code = coder.encode_next(bc, k);
                    ASSERT_EQ(code.c, cs[i].c);
                    ASSERT_EQ(code.o, cs[i].o);
                }
                else
                {
                    coder.skip_next(bc, k);
                }
             }
         });

    std::cout << std::endl;
}
