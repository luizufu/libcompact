#include <tests/test.hxx>
#include <tests/type-name.hxx>

#include <tests/bit/data.hxx>

#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/bit/rrr-bit-vector.hxx>
#include <libculcompact/bit/sparse-bit-vector.hxx>
#include <libculcompact/bit/run-bit-vector.hxx>

using namespace std;
using namespace culcompact;


template <typename Bits, typename Ext>
void run_tests()
{
    cout << "Testing " << type_name<Bits>() << "\n";

    TEST("read_bit(size_t)",
         [bits = Bits(binary.begin(), binary.end())]() {
             for(size_t i = 0; i < binary.size(); ++i)
                 ASSERT_EQ(bits.read_bit(i), binary[i]);
         });

    TEST("operator[](size_t)",
         [bits = Bits(binary.begin(), binary.end())]() mutable {
             for(size_t i = 0; i < binary.size(); ++i)
                 ASSERT_EQ(bits[i], binary[i]);
         });

    TEST("iterator",
         [bits = Bits(binary.begin(), binary.end())]() {
            size_t i = 0;
            for(bool bit : bits)
                ASSERT_EQ(bit, binary[i++]);
         });


    if constexpr(std::is_same_v<Bits, plain_bit_vector<>>)
    {
        TEST("set_bit(size_t)",
             [bits = Bits(binary.begin(), binary.end()), b = binary]() mutable {
                 for(auto i : sampled_positions)
                 {
                     bits.set_bit(i);
                     b[i] = true;

                     for(size_t k = 0; k < b.size(); ++k)
                         ASSERT_EQ(bits.read_bit(k), b[k]);
                 }
             });

        TEST("clear_bit(size_t)",
             [bits = Bits(binary.begin(), binary.end()), b = binary]() mutable {
                 for(auto i : sampled_positions)
                 {
                     bits.clear_bit(i);
                     b[i] = 0;

                     for(size_t k = 0; k < b.size(); ++k)
                         ASSERT_EQ(bits.read_bit(k), b[k]);
                 }
             });

        TEST("toggle_bit(size_t)",
             [bits = Bits(binary.begin(), binary.end()), b = binary]() mutable {
                 for(auto i : sampled_positions)
                 {
                     bits.toggle_bit(i);
                     b[i] = !(b[i]);

                     for(size_t k = 0; k < b.size(); ++k)
                         ASSERT_EQ(bits.read_bit(k), b[k]);
                 }
             });
    }

    TEST("read_int(size_t, size_t)",
         [bits = Bits(binary.begin(), binary.end())]() {
             for(size_t i = 0; i < W * 4; i += W)
                 ASSERT_EQ(bits.read_int(i, i + W), decimal);

             for(size_t i = 0; i < W * 4; i += 4)
                 ASSERT_EQ(bits.read_int(i, i + 4), decimal_parts[(i%W) / 4]);

             for(size_t i = 3; i < W * 4 - 3; i += 4)
                 ASSERT_EQ(bits.read_int(i, i + 4), sdecimal_parts[(i%W) / 4]);
         });

    if constexpr(std::is_same_v<Bits, plain_bit_vector<>>)
    {
        TEST("write_int(size_t, size_t)",
             [bits = Bits(binary.begin(), binary.end())]() mutable {
                 for(size_t i = 0; i < W * 4; i += W) {
                     bits.write_int(i, i + W, decimal);
                     ASSERT_EQ(bits.read_int(i, i + W), decimal);
                 }
                 for(size_t i = 0; i < W * 4; i += 4) {
                     bits.write_int(i, i + 4, decimal_parts[(i % W) / 4]);
                     ASSERT_EQ(bits.read_int(i, i + 4), decimal_parts[(i%W) / 4]);
                 }
                 for(size_t i = 3; i < W * 4 - 3; i += 4) {
                     bits.write_int(i, i + 4, sdecimal_parts[(i % W) / 4]);
                     ASSERT_EQ(bits.read_int(i, i + 4), sdecimal_parts[(i%W) / 4]);
                 }
             });
    }

    TEST("count()",
         [bits = Bits(binary.begin(), binary.end())]() mutable {
             ASSERT_EQ(bits.count(), bcount);
         });


    cout << "Testing " << type_name<Ext>() << "\n";

    TEST("rank1(size_t)",
         [bits = Bits(binary.begin(), binary.end())]() mutable {
            Ext ext(bits);

            for(size_t i = 0; i < bits.size(); ++i)
                ASSERT_EQ(ext.rank1(i), ranks1[i]);
         });

    TEST("rank0(size_t)",
         [bits = Bits(binary.begin(), binary.end())]() mutable {
            Ext ext(bits);

            for(size_t i = 0; i < bits.size(); ++i)
                ASSERT_EQ(ext.rank0(i), ranks0[i]);
         });

    TEST("select1(size_t)",
         [bits = Bits(binary.begin(), binary.end())]() mutable {
            Ext ext(bits);

            for(size_t i = 0; i < bits.size(); ++i)
                ASSERT_EQ(ext.select1(i), selects1[i]);
         });

    TEST("select0(size_t)",
         [bits = Bits(binary.begin(), binary.end())]() mutable {
            Ext ext(bits);

            for(size_t i = 0; i < bits.size(); ++i)
                ASSERT_EQ(ext.select0(i), selects0[i]);
         });


    cout << endl;
}

int main()
{
    run_tests<
        plain_bit_vector<>,
        typename plain_bit_vector<>::extension1>();
    run_tests<
        rrr_bit_vector<>,
        typename rrr_bit_vector<>::extension1>();
    run_tests<
        sparse_bit_vector<>,
        typename sparse_bit_vector<>::extension1>();
    /* run_tests< */
    /*     run_bit_vector<>, */
    /*     typename run_bit_vector<>::extension1>(); */
}
