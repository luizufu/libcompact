#include <cstdint>
#include <array>

#include <libculcompact/aux/word.hxx>
#include <tests/test.hxx>
#include <tests/bit/data.hxx>

using namespace std;
using namespace culcompact;

using decimal_t = uint32_t;
using word_t = word<uint32_t>;

void run_word_tests()
{
    std::cout << "Testing word<T>" << "\n";

    TEST("word<T>::read_bit(size_t)",
         [w = word_t(decimal)]() {
             for(size_t i = W; i > 0; --i)
                 ASSERT_EQ(w.read_bit(W - i), wbinary[i - 1]);
         });

    TEST("word<T>::set_bit(size_t)",
         [w = word_t(decimal), b = wbinary]() mutable {
             for(auto i : {0 , 2, 7, 10, 15, 26, 31}) {
                 w.set_bit(i);
                 b[32 - i - 1] = true;
             }

             for(size_t i = W; i > 0; --i)
                 ASSERT_EQ(w.read_bit(W - i), b[i - 1]);
         });

    TEST("word<T>::clear_bit(size_t)",
         [w = word_t(decimal), b = wbinary]() mutable {
             for(auto i : {0 , 2, 7, 10, 15, 26, 31}) {
                 w.clear_bit(i);
                 b[32 - i - 1] = 0;
             }

             for(size_t i = W; i > 0; --i)
                 ASSERT_EQ(w.read_bit(W - i), b[i - 1]);
         });

    TEST("word<T>::toggle_bit(size_t)",
         [w = word_t(decimal), b = wbinary]() mutable {
             for(auto i : {0 , 2, 7, 10, 15, 26, 31}) {
                 w.toggle_bit(i);
                 b[32 - i - 1] = !(b[32 - i - 1]);
             }

             for(size_t i = W; i > 0; --i)
                 ASSERT_EQ(w.read_bit(W - i), b[i - 1]);
         });

    TEST("word<T>::read_int(size_t, size_t)",
         [w = word_t(decimal)]() {
             ASSERT_EQ(w.read_int(0, W), decimal);
             for(size_t i = 0; i < W; i += 4)
                 ASSERT_EQ(w.read_int(i, i + 4), wdecimal_parts[i / 4]);
         });

    TEST("word<T>::write_int(size_t, size_t)",
         [w = word_t()]() mutable {
             w.write_int(0, W, decimal);
             ASSERT_EQ(w.read_int(0, W), decimal);
             for(size_t i = 0; i < W; i += 4) {
                 w.write_int(i, i + 4, wdecimal_parts[i / 4]);
                 ASSERT_EQ(w.read_int(i, i + 4), wdecimal_parts[i / 4]);
             }
         });

    TEST("word<T>::count(size_t, size_t)",
         [w = word_t(decimal)]() mutable {
             ASSERT_EQ(w.count(0, W), wbcount);
             for(size_t i = 0; i < W; i += 4) {
                 ASSERT_EQ(w.count(i, i + 4), wbcount_parts[i / 4]);
             }
         });

    std::cout << std::endl;
}
