#include <tests/test.hxx>
#include <tests/type-name.hxx>
#include <tests/int/data.hxx>

#include <libculcompact/int/fixed-int-array.hxx>
#include <libculcompact/int/sp-int-array.hxx>
#include <libculcompact/int/dp-int-array.hxx>
#include <libculcompact/int/da-int-array.hxx>
#include <libculcompact/int/ef-int-array.hxx>


using namespace std;
using namespace culcompact;

template<typename Ints, typename Ext, typename... Args>
void run_tests(Args... args)
{
    cout << "Testing " << type_name<Ints>() << "\n";

    TEST("read(size_t)",
         [ints = Ints(numbers.begin(), numbers.end(), args...)]() {
             for(size_t i = 0; i < numbers.size(); ++i)
                 ASSERT_EQ(ints.read(i), numbers[i]);
         });

    TEST("operator[](size_t)",
         [ints = Ints(numbers.begin(), numbers.end(), args...)]() mutable {
             for(size_t i = 0; i < numbers.size(); ++i)
                 ASSERT_EQ(static_cast<size_t>(ints[i]), numbers[i]);
         });

    TEST("iterator",
         [ints = Ints(numbers.begin(), numbers.end(), args...)]() {
            size_t i = 0;
            for(size_t number : ints)
                ASSERT_EQ(number, numbers[i++]);
         });

    if constexpr(std::is_same_v<Ints, fixed_int_array<>>)
    {
        TEST("write(size_t, T)",
             [ints = Ints(numbers.begin(), numbers.end(), args...), v = numbers]() mutable {
                 for(auto pos : sampled_positions)
                 {
                    for(auto number : sampled_numbers)
                    {
                         ints.write(pos, number);
                         v[pos] = number;
                         for(size_t k = 0; k < v.size(); ++k)
                             ASSERT_EQ(ints.read(k), v[k]);
                    }
                 }
             });

        TEST("operator=",
             [ints = Ints(numbers.begin(), numbers.end(), args...), v = numbers]() mutable {
                 for(auto pos : sampled_positions)
                 {
                    for(auto number : sampled_numbers)
                    {
                         ints[pos] = number;
                         v[pos] = number;
                         for(size_t k = 0; k < v.size(); ++k)
                             ASSERT_EQ(ints.read(k), v[k]);
                    }
                 }
             });
    }


    cout << "Testing " << type_name<Ext>() << "\n";

    TEST("sum(size_t)",
         [ints = Ints(numbers.begin(), numbers.end(), args...)]() mutable {
            Ext ext(ints);

            for(size_t i = 0; i < ints.size(); ++i)
                ASSERT_EQ(ext.sum(i), sums[i]);
         });

    TEST("search(size_t)",
         [ints = Ints(numbers.begin(), numbers.end(), args...)]() mutable {
            Ext ext(ints);

            for(size_t i = 0; i < ints.size(); ++i)
                ASSERT_EQ(ext.search(sums[i]), searchs[i]);
         });

    cout << endl;
}


int main()
{
    run_tests<fixed_int_array<>, typename fixed_int_array<>::extension1>(8);
    run_tests<sp_int_array<>, typename sp_int_array<>::extension1>();
    run_tests<dp_int_array<>, typename dp_int_array<>::extension1>();
    run_tests<da_int_array<>, typename da_int_array<>::extension1<>>();
    run_tests<ef_int_array<>, typename ef_int_array<>::extension1<>>();
}
