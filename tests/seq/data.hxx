#pragma once

#include <cstddef>
#include <cstdint>
#include <array>
#include <unordered_map>

const std::unordered_map<char, size_t> map = {
    { 'a', 0 },
    { 'b', 1 },
    { 'e', 2 },
    { 'h', 3 },
    { 'i', 4 },
    { 'n', 5 },
    { 'o', 6 },
    { 'q', 7 },
    { 'r', 8 },
    { 's', 9 },
    { 't', 10 },
    { 'u', 11 }
};

constexpr std::array<char, 12> unmap = {
    'a', 'b', 'e', 'h', 'i', 'n', 'o', 'q', 'r', 's', 't', 'u'
};

constexpr std::array<char, 30> text = {
    't', 'o', 'b', 'e', 'o', 'r', 'n', 'o', 't', 't', 'o', 'b', 'e', 't', 'h', 'a', 't', 'i', 's', 't', 'h', 'e', 'q', 'u', 'e', 's', 't', 'i', 'o', 'n'
};

constexpr std::array<size_t, 31> rankst = {
    0, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7
};

constexpr std::array<size_t, 30> selectst = {
    0, 1, 9, 10, 14, 17, 20, 27, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31
};
