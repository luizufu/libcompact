#include <tests/test.hxx>
#include <tests/type-name.hxx>
#include <tests/seq/data.hxx>

#include <libculcompact/seq/perm-sequence.hxx>
#include <libculcompact/seq/wt-sequence.hxx>
#include <libculcompact/seq/hwt-sequence.hxx>
#include <libculcompact/seq/wm-sequence.hxx>

#include <iostream>
#include <typeinfo>

using namespace std;
using namespace culcompact;


template <typename Seq, typename Ext>
void run_tests()
{

    cout << "Testing " << type_name<Seq>() << "\n";

    array<size_t, 30> numbers;

    for(size_t i = 0; i < text.size(); ++i)
        numbers[i] = map.at(text[i]);

    TEST("read(size_t)",
         [ints = Seq(numbers.begin(), numbers.end(), 12), &numbers]() {
             for(size_t i = 0; i < numbers.size(); ++i)
                 ASSERT_EQ(ints.read(i), numbers[i]);
         });

    TEST("operator[](size_t)",
         [ints = Seq(numbers.begin(), numbers.end(), 12), &numbers]() mutable {
             for(size_t i = 0; i < numbers.size(); ++i)
                 ASSERT_EQ(static_cast<size_t>(ints[i]), numbers[i]);
         });

    TEST("iterator",
         [ints = Seq(numbers.begin(), numbers.end(), 12), &numbers]() {
            size_t i = 0;
            for(size_t number : ints)
                ASSERT_EQ(number, numbers[i++]);
         });

    /* TEST("write(size_t)", */
    /*      [ints = Seq(numbers.begin(), numbers.end(), 12), v = numbers]() mutable { */
    /*          for(auto pos : sampled_positions) */
    /*          { */
    /*             for(auto number : sampled_numbers) */
    /*             { */
    /*                  ints.write(pos, number); */
    /*                  v[pos] = number; */
    /*                  for(size_t k = 0; k < v.size(); ++k) */
    /*                      ASSERT_EQ(ints.read(k), v[k]); */
    /*             } */
    /*          } */
    /*      }); */

    /* TEST("operator=", */
    /*      [ints = Seq(numbers.begin(), numbers.end(), 12), v = numbers]() mutable { */
    /*          for(auto pos : sampled_positions) */
    /*          { */
    /*             for(auto number : sampled_numbers) */
    /*             { */
    /*                  ints[pos] = number; */
    /*                  v[pos] = number; */
    /*                  for(size_t k = 0; k < v.size(); ++k) */
    /*                      ASSERT_EQ(ints.read(k), v[k]); */
    /*             } */
    /*          } */
         /* }); */


    cout << "Testing " << type_name<Ext>() << "\n";

    TEST("rank(size_t, 't')",
         [ints = Seq(numbers.begin(), numbers.end(), 12)]() mutable {
            Ext ext(ints);

            for(size_t i = 0; i < ints.size(); ++i)
                ASSERT_EQ(ext.rankc(i, 10), rankst[i]); // t = 10
         });

    TEST("select(size_t, 't')",
         [ints = Seq(numbers.begin(), numbers.end(), 12)]() mutable {
            Ext ext(ints);

            for(size_t i = 0; i < ints.size(); ++i)
                ASSERT_EQ(ext.selectc(i, 10), selectst[i]);
         });

    cout << endl;
    cout << endl;
}

int main()
{
    run_tests<perm_sequence<>, typename perm_sequence<>::extension1>();
    run_tests<wt_sequence<>, typename wt_sequence<>::extension1>();
    run_tests<cwt_sequence<>, typename cwt_sequence<>::extension1>();
    run_tests<hwt_sequence<>, typename hwt_sequence<>::extension1>();
    run_tests<chwt_sequence<>, typename chwt_sequence<>::extension1>();
    run_tests<wm_sequence<>, typename wm_sequence<>::extension1>();
    run_tests<cwm_sequence<>, typename cwm_sequence<>::extension1>();
}
