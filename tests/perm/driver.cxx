#include <tests/test.hxx>
#include <tests/type-name.hxx>
#include <tests/perm/data.hxx>

#include <libculcompact/perm/plain-permutation.hxx>
#include <libculcompact/perm/ec-permutation.hxx>
#include <libculcompact/perm/huffman-permutation.hxx>

using namespace std;
using namespace culcompact;

template<typename Perm, typename Ext>
void run_tests()
{
    cout << "Testing " << type_name<Perm>() << "\n";

    TEST("read(size_t)",
         [ints = Perm(numbers.begin(), numbers.end())]() {
             for(size_t i = 0; i < numbers.size(); ++i)
                 ASSERT_EQ(ints.read(i), numbers[i]);
         });

    TEST("operator[](size_t)",
         [ints = Perm(numbers.begin(), numbers.end())]() mutable {
             for(size_t i = 0; i < numbers.size(); ++i)
                 ASSERT_EQ(static_cast<size_t>(ints[i]), numbers[i]);
         });

    TEST("iterator",
         [ints = Perm(numbers.begin(), numbers.end())]() {
            size_t i = 0;
            for(size_t number : ints)
                ASSERT_EQ(number, numbers[i++]);
         });

    if constexpr(std::is_same_v<Perm, plain_permutation<>>)
    {
        TEST("swap",
             [ints = Perm(numbers.begin(), numbers.end())]() mutable {
                for(size_t i = 0; i < ints.size() - 2; ++i)
                {
                    auto a = ints[i];
                    auto b = ints[i + 2];

                    ints.swap(i, i + 2);

                    ASSERT_EQ(ints[i], b);
                    ASSERT_EQ(ints[i + 2], a);
                }
             });
    }


    cout << "Testing " << type_name<Ext>() << "\n";

    TEST("inverse(size_t)",
         [ints = Perm(numbers.begin(), numbers.end())]() mutable {
            Ext ext(ints);

            for(size_t i = 0; i < ints.size(); ++i)
                ASSERT_EQ(ext.inverse(i), invs[i]);
         });

    TEST("power(size_t, 2)",
         [ints = Perm(numbers.begin(), numbers.end())]() mutable {
            Ext ext(ints);

            for(size_t i = 0; i < ints.size(); ++i)
                ASSERT_EQ(ext.power(i, 2), k2p[i]);
         });

    TEST("power(size_t, -2)",
         [ints = Perm(numbers.begin(), numbers.end())]() mutable {
            Ext ext(ints);

            for(size_t i = 0; i < ints.size(); ++i)
                ASSERT_EQ(ext.power(i, -2), k2n[i]);
         });

    cout << endl;
}

int main()
{
    run_tests<plain_permutation<>, typename plain_permutation<>::extension1>();
    run_tests<ec_permutation<>, typename ec_permutation<>::extension1>();
    run_tests<huffman_permutation<>, typename huffman_permutation<>::extension1>();
}
