#pragma once

#include <cstddef>
#include <cstdint>
#include <array>

using natural_t = std::array<size_t, 12>;

constexpr natural_t numbers = {
    9, 6, 2, 4, 7, 0, 10, 11, 3, 5, 8, 1
};

constexpr natural_t invs = {
    5, 11, 2, 8, 3, 9, 1, 4, 10, 0, 6, 7
};

constexpr natural_t k2p = {
    0, 8, 2, 11, 1, 5, 3, 6, 7, 9, 4, 10
};

constexpr natural_t k2n = {
    9, 7, 2, 10, 8, 0, 11, 3, 6, 5, 1, 4
};

