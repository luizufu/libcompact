#pragma once

#include <stdexcept>
#include <iostream>

#define S1(x) #x
#define S2(x) S1(x)
#define FORMATED_MSG(exp, eval) \
    "\n  at " __FILE__ ":" S2(__LINE__) \
    "\n    expression: " exp \
    "\n    evaluated as: " + eval + "\n"

#define ASSERT_EQ(lhs, rhs) \
    if(lhs != rhs) \
        throw std::runtime_error( \
            FORMATED_MSG(\
                #lhs " != " #rhs, \
                to_string(lhs) + " != " + to_string(rhs)))

template <typename Func>
void TEST(const char* title, Func f)
{
    std::cout << "  " << title << "... ";
    try {
        f();
        std::cout << "PASSED\n";
    } catch (std::exception& ex) {
        std::cout << "NOT PASSED\n";
        std::cerr << ex.what() << std::endl;
        abort();
    }
}
