# libculcompact

C++ cool library containing compact data structures

# TODO

- coder
    - ~~unary~~
    - ~~gamma~~
    - ~~delta~~
    - ~~rice~~
    - ~~shannonfano~~
    - ~~huffman~~
    - ~~chuffman~~
    - ~~vbyte~~
    - simple9
    - etdc
- bit
    - ~~plain vector~~
    - ~~rrr vector~~
    - ~~sparse array~~
    - run vector
- int
    - ~~fixed array~~
    - ~~sampled pointers array~~
    - ~~dense pointers array~~
    - ~~direct access array~~
    - ~~eliasfano array~~
- perm (better implement these)
    - ~~plain~~
    - ~~cycle~~
    - ~~huffman~~
    - limited-length huffman
- seq
    - ~~perm~~ (seletc not passing)
    - ~~wavelet tre~~
    - ~~huffman wavelet tree~~
    - ~~wavelet matrix~~
    - alphabet partitioning
- general
    - change the compression stuff
        - should compressed bitvector be only constructable with an uncompressed bitvector?
        - should compressed wt be only constructable with an uncompressed wt?



