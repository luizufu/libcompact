#pragma once

#include <cstddef>

#ifndef CULCOMPACT_DEFAULT_INT_TYPE
// used for summable extension in int structures
#   define CULCOMPACT_DEFAULT_INT_TYPE uint32_t
#endif

#ifndef CULCOMPACT_DEFAULT_SMALL_INT_TYPE
// used for summable extension in int structures
#   define CULCOMPACT_DEFAULT_SMALL_INT_TYPE uint16_t
#endif

#ifndef CULCOMPACT_DEFAULT_K
// used for summable extension in int structures
#   define CULCOMPACT_DEFAULT_K 4
#endif

#ifndef CULCOMPACT_DEFAULT_B
// used for block partitioning of compressed bit structures
#   define CULCOMPACT_DEFAULT_B 4
#endif

// used for rankselectable extension in bit structures
#ifndef CULCOMPACT_DEFAULT_S
#   define CULCOMPACT_DEFAULT_S 16
#endif

// used for shortcuts in plain permutation structure
#ifndef CULCOMPACT_DEFAULT_Y
#   define CULCOMPACT_DEFAULT_Y 3
#endif
