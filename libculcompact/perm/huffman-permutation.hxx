#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/perm/index-iterator.hxx>
#include <libculcompact/int/fixed-int-array.hxx>
#include <libculcompact/bit/rrr-bit-vector.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/aux/huffman-tree.hxx>
#include <libculcompact/detail/math.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <type_traits>
#include <array>
#include <algorithm>
#include <random>
#include <numeric>
#include <stdexcept>

namespace culcompact
{

// stores a static sequence of bits in a plain format that concatenates words of type T to abstract the whole sequence
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class huffman_permutation
{
    // stores bitvector and extension together
    struct bitvector_ext_t;

    rrr_bit_vector<T> _run_starts;
    huffman_tree<bitvector_ext_t> _tree;
    fixed_int_array<T> _run_positions;
    typename rrr_bit_vector<T>::extension1 _run_starts_ext;
    size_t _n;


    template <detail::IntIterable It>
    size_t number_of_runs(It begin, It end) const
    {
        T last = *begin;
        return 1 + std::count_if(
            std::next(begin), end,
            [&last] (T s) {
                T tmp = s;
                std::swap(tmp, last);
                return s < tmp;
            });
    }

    template <detail::IntIterable It>
    void construct_tree(It begin, It end, size_t p, size_t k)
    {

        // TODO do i really need to make tmp (rrr cant set bit, set_bit is costly)
        plain_bit_vector<T> tmp(std::distance(begin, end));

        // allocate buffers
        std::vector<bitvector_ext_t> symbols;
        symbols.reserve(p);
        std::vector<float> frequencies(p);
        _run_positions = fixed_int_array<T>(
            p, std::floor(std::log2(_run_starts.size())) + 1);

        // compute all info
        tmp.set_bit(0);
        symbols.emplace_back(0, k);
        _run_positions[0] = 0;
        T last = *begin;
        size_t i = 1; size_t start = 0; size_t r = 0;
        for(It it = std::next(begin); it != end; ++it, ++i)
        {
            if(static_cast<T>(*it) < last)
            {
                // get frequency of traversed run
                frequencies[r] = i - start;
                ++r;
                // set other values for the new run
                symbols.emplace_back(r, k);
                tmp.set_bit(i);
                _run_positions[r] = i;
                start = i;
            }

            last = *it;
        }

        _run_starts = rrr_bit_vector<T>(tmp.begin(), tmp.end());

        // frequency of last run
        frequencies[r] = i - start;
        _run_starts_ext.update();

        _tree = huffman_tree<bitvector_ext_t>(symbols, frequencies);

        // merge tree nodes as in merge-sort and update bitvectors
        // discard ordered permutation
        _tree.traverse_r(
            [this, begin](auto* n, auto visit) {
                if(n->is_leaf())
                {
                    // trigger conversion
                    size_t j = n->symbol;

                    // n->weight has the length of run s
                    return std::vector<T>(
                        begin + _run_positions[j],
                        begin + (_run_positions[j] +
                            static_cast<size_t>(n->weight)));
                }

                // visit children and get their runs
                auto left_run = visit(n->left);
                auto right_run = visit(n->right);

                // merge runs and update bitvectors
                std::vector<T> merged_run(left_run.size() + right_run.size());
                n->symbol.bitvector.resize(merged_run.size());
                for(size_t j = 0, jl = 0, jr = 0; j < merged_run.size(); ++j)
                {
                    if(jl < left_run.size() &&
                       (jr >= right_run.size() || left_run[jl] < right_run[jr]))
                    {
                        merged_run[j] = left_run[jl];
                        /* n->symbol.bitvector.clear_bit(i); */ // already zero
                        ++jl;
                    }
                    else
                    {
                        merged_run[j] = right_run[jr];
                        n->symbol.bitvector.set_bit(j);
                        ++jr;
                    }
                }

                n->symbol.bitvector_ext.update();
                return merged_run;
            });
    }

public:

    class extension1;
    using iterator = index_iterator<huffman_permutation<T>>;
    using const_iterator = const_index_iterator<huffman_permutation<T>>;


    huffman_permutation() = default;

    huffman_permutation(size_t n, size_t k = CULCOMPACT_DEFAULT_K)
        : _n(n)
    {
        std::vector<T> tmp(_n);
        std::iota(tmp.begin(), tmp.end(), 0);

        std::shuffle(tmp.begin(), tmp.end(),
                std::mt19937 {std::random_device {}()});

        construct_tree(tmp.begin(), tmp.end(),
            number_of_runs(tmp.begin(), tmp.end()), k);
    }

    // constructs a sequence of integers from a pair of iterators, where elements are castable to integer
    template <detail::IntIterable It>
    huffman_permutation(It begin, It end, size_t k = CULCOMPACT_DEFAULT_K)
        : _run_starts(std::distance(begin, end))
        , _run_starts_ext(_run_starts)
        , _n(std::distance(begin, end))
    {
        construct_tree(begin, end, number_of_runs(begin, end), k);
    }

    // constructs a sequence of integers from an initializer_list with elements castable to integer
    template <detail::IntConvertible Int>
    huffman_permutation(std::initializer_list<Int> list)
        : huffman_permutation(list.begin(), list.end())
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    size_t operator[](size_t i)
    {
        size_t j = _run_starts_ext.rank1(i + 1);
        i = i - _run_positions[j - 1];

        _tree.traverse_from_v(bitvector_ext_t(j - 1, 1),
            [&i] (auto* n, auto visit) {
                if(!n->is_root())
                {
                    if(n == n->parent->left.get())
                        i = n->parent->symbol.bitvector_ext.select0(i + 1) - 1;
                    else
                        i = n->parent->symbol.bitvector_ext.select1(i + 1) - 1;

                    visit(n->parent);
                }
            });

        return i;
    }


    // reads int at position i, this is a throwing and non-modifiable version
    size_t read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        size_t j = _run_starts_ext.rank1(i + 1);
        i = i - _run_positions.read(j - 1);

        _tree.traverse_from_v(bitvector_ext_t(j - 1, 1),
            [&i] (auto* n, auto visit) {
                if(!n->is_root())
                {
                    if(n == n->parent->left.get())
                        i = n->parent->symbol.bitvector_ext.select0(i + 1) - 1;
                    else
                        i = n->parent->symbol.bitvector_ext.select1(i + 1) - 1;

                    visit(n->parent);
                }
            });

        return i;
    }

    /* // TODO not implemented */
    /* void swap(size_t i, size_t j) */
    /* { */
    /*     if(i >= size()) */
    /*         throw std::out_of_range(detail::make_out_of_range_msg(i, size())); */

    /*     if(j >= size()) */
    /*         throw std::out_of_range(detail::make_out_of_range_msg(j, size())); */

    /*     if(i == j) */
    /*         return; */

    /* } */

    // returns the number of elements in the permutation
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return 0;
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};

template <typename T>
struct huffman_permutation<T>::bitvector_ext_t
{
    plain_bit_vector<T> bitvector;
    typename plain_bit_vector<T>::extension1 bitvector_ext;

    bitvector_ext_t()
        : bitvector_ext(bitvector)
    {
    }

    bitvector_ext_t(T id, size_t k)
        : bitvector(id > 0  ? std::floor(std::log2(id)) + 1 : 1)
        , bitvector_ext(bitvector, k)
    {
        bitvector.write_int(0, bitvector.size(), id);
    }

    operator size_t() const
    {
        if(bitvector.size() == 0)
            return ~size_t{0};

        return bitvector.read_int(0, bitvector.size());
    }
};


// implements the sampled pointers strategy for sum and the binary search strategy for search
template <typename T>
class huffman_permutation<T>::extension1
{
    const huffman_permutation<T>& _permutation;

public:

    extension1(const huffman_permutation& permutation)
        : _permutation(permutation)
    {
        update();
    }

    void update()
    {
    }

    size_t inverse(size_t i) const
    {
        size_t run_id = _permutation._tree.traverse_r(
            [&i] (const auto* n, auto visit) {
                if(n->is_leaf())
                    return n->symbol;

                if(!n->symbol.bitvector.read_bit(i))
                {
                    i = n->symbol.bitvector_ext.rank0(i + 1) - 1;
                    return visit(n->left);
                }
                else
                {
                    i = n->symbol.bitvector_ext.rank1(i + 1) - 1;
                    return visit(n->right);
                }
            });

        return _permutation._run_positions.read(run_id) + i;
    }

    size_t power(size_t i, ssize_t k)
    {
        if(k > 0)
        {
            i = _permutation.read(i);
            while(k > 0)
            {
                i = _permutation.read(i);
                --k;
            }
        }
        else
        {
            while(k < 0)
            {
                i = inverse(i);
                ++k;
            }
        }

        return i;
    }
};

} /* culcompact */
