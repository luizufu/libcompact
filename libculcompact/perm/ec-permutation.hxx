#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/perm/index-iterator.hxx>
#include <libculcompact/perm/plain-permutation.hxx>
#include <libculcompact/int/fixed-int-array.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/detail/math.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <type_traits>
#include <array>
#include <algorithm>
#include <numeric>
#include <random>
#include <stdexcept>

namespace culcompact
{

// stores a static sequence of bits in a plain format that concatenates words of type T to abstract the whole sequence
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class ec_permutation
{
    plain_permutation<T> _cycles;
    plain_bit_vector<T> _endings;
    typename plain_permutation<T>::extension1 _cycles_ext;
    typename plain_bit_vector<T>::extension1 _endings_ext;
    size_t _n;


    void construct(const std::vector<T> numbers)
    {
        plain_bit_vector<T> tmp(_n);
        std::vector<T> cycles(_n);

        // update _shortcuts
        size_t k = 0;
        for(size_t i = 0; i < _n; ++i)
        {
            if(!tmp.read_bit(i))
            {
                tmp.set_bit(i);
                size_t j = numbers[i];

                // explore new cycle
                while(j != i)
                {
                    tmp.set_bit(j);
                    cycles[k++] = j;
                    j = numbers[j];
                }

                _endings.set_bit(k);
                cycles[k++] = j;
            }
        }

        _cycles = plain_permutation<T>(cycles.begin(), cycles.end());
    }

    // works with negative numbers: -1 % 3 = 2
    size_t mod(ssize_t a, ssize_t b) const
    {
        return (b + (a % b)) % b;
    }

    size_t power_impl(size_t i, ssize_t k) const
    {
        size_t j = _cycles_ext.inverse(i);

        size_t rank = _endings_ext.rank1(j);
        size_t start = _endings_ext.select1(rank);
        size_t end = _endings_ext.select1(rank + 1);

        /* size_t start = _endings_ext.select1(_endings_ext.rank1(j)); */
        /* size_t end = _endings_ext.select1(_endings_ext.rank1(j - 1) + 1); */

        return _cycles.read(start + mod(j + k - start, end - start));
    }

public:

    class extension1;
    using iterator = index_iterator<ec_permutation<T>>;
    using const_iterator = const_index_iterator<ec_permutation<T>>;


    ec_permutation() = default;

    ec_permutation(size_t n, size_t y = CULCOMPACT_DEFAULT_Y, size_t k = CULCOMPACT_DEFAULT_K)
        : _cycles(n)
        , _endings(n)
        , _cycles_ext(_cycles, y, k)
        , _endings_ext(_endings, k)
        , _n(n)
    {
        std::vector<T> tmp(_n);
        std::iota(tmp.begin(), tmp.end(), 0);

        std::shuffle(tmp.begin(), tmp.end(),
                std::mt19937 {std::random_device {}()});

        construct(tmp);
        _cycles_ext.update();
        _endings_ext.update();
    }

    // constructs a sequence of integers from a pair of iterators, where elements are castable to integer
    template <detail::IntIterable It>
    ec_permutation(It begin, It end, size_t y = CULCOMPACT_DEFAULT_Y, size_t k = CULCOMPACT_DEFAULT_K)
        : _cycles(std::distance(begin, end))
        , _endings(std::distance(begin, end))
        , _cycles_ext(_cycles, y, k)
        , _endings_ext(_endings, k)
        , _n(std::distance(begin, end))
    {
        std::vector<T> tmp(begin, end);
        construct(tmp);
        _cycles_ext.update();
        _endings_ext.update();
    }

    // constructs a sequence of integers from an initializer_list with elements castable to integer
    template <detail::IntConvertible Int>
    ec_permutation(std::initializer_list<Int> list)
        : ec_permutation(list.begin(), list.end())
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    size_t operator[](size_t i)
    {
        return power_impl(i, 1);
    }


    // reads int at position i, this is a throwing and non-modifiable version
    size_t read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        return power_impl(i, 1);
    }

    // returns the number of elements in the permutation
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _cycles.size_of() + _endings.size_of();
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


template <typename T>
class ec_permutation<T>::extension1
{
    const ec_permutation& _permutation;

public:

    extension1(const ec_permutation& permutation)
        : _permutation(permutation)
    {
        update();
    }

    void update()
    {
    }

    size_t inverse(size_t i) const
    {
        return _permutation.power_impl(i, -1);
    }

    size_t power(size_t i, ssize_t k)
    {
        if(k > 0)
            ++k;

        return _permutation.power_impl(i, k);
    }
};

} /* culcompact */
