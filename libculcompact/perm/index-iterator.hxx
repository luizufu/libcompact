#pragma once

#include <cstddef>
#include <iterator>

namespace culcompact
{

template <typename Perm>
class index_iterator
{
    Perm* _pointer;
    size_t _i;

public:

    using iterator_category = std::random_access_iterator_tag;
    using value_type = size_t;
    using reference = void;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    index_iterator()
        : _pointer(nullptr)
        , _i(0)
    {
    }

    index_iterator(Perm* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
    {
    }

    /* index_iterator(const index_iterator& rhs) */
    /*     : _pointer(rhs._pointer) */
    /*     , _i(rhs._i) */
    /* { */
    /* } */

    index_iterator& operator+=(difference_type rhs)
    {
        _i += rhs;
        return *this;
    }

    index_iterator& operator-=(difference_type rhs)
    {
        _i -= rhs;
        return *this;
    }

    value_type operator*() const
    {
        return _pointer->operator[](_i);
    }

    value_type operator[](difference_type rhs) const
    {
        return _pointer->operator[](rhs);
    }

    index_iterator& operator++()
    {
        ++_i;
        return *this;
    }

    index_iterator& operator--()
    {
        --_i;
        return *this;
    }

    index_iterator operator++(int)
    {
        index_iterator r(*this);
        this->operator++();
        return r;
    }

    index_iterator operator--(int)
    {
        index_iterator r(*this);
        this->operator--();
        return r;
    }

    difference_type operator-(const index_iterator& rhs) const
    {
        return _i - rhs._i;
    }

    index_iterator operator+(difference_type rhs) const
    {
        return index_iterator(_pointer, _i + rhs);
    }

    index_iterator operator-(difference_type rhs) const
    {
        return index_iterator(_pointer, _i - rhs);
    }

    friend index_iterator operator+(difference_type lhs, const index_iterator& rhs)
    {
        return index_iterator(rhs._pointer, lhs + rhs._i);
    }

    friend index_iterator operator-(difference_type lhs, const index_iterator& rhs)
    {
        return index_iterator(rhs._pointer, lhs - rhs._i);
    }

    bool operator==(const index_iterator& rhs) const
    {
        return _i == rhs._i;
    }

    bool operator!=(const index_iterator& rhs) const
    {
        return _i != rhs._i;
    }

    bool operator>(const index_iterator& rhs) const
    {
        return _i > rhs._i;
    }

    bool operator<(const index_iterator& rhs) const
    {
        return _i < rhs._i;
    }

    bool operator>=(const index_iterator& rhs) const
    {
        return _i >= rhs._i;
    }

    bool operator<=(const index_iterator& rhs) const
    {
        return _i <= rhs._i;
    }
};


template <typename Perm>
class const_index_iterator
{
    const Perm* _pointer;
    size_t _i;

public:

    using iterator_category = std::random_access_iterator_tag;
    using value_type = size_t;
    using reference = void;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    const_index_iterator()
        : _pointer(nullptr)
        , _i(0)
    {
    }

    const_index_iterator(const Perm* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
    {
    }

    /* const_index_iterator(const const_index_iterator& rhs) */
    /*     : _pointer(rhs._pointer) */
    /*     , _i(rhs._i) */
    /* { */
    /* } */

    const_index_iterator& operator+=(difference_type rhs)
    {
        _i += rhs;
        return *this;
    }

    const_index_iterator& operator-=(difference_type rhs)
    {
        _i -= rhs;
        return *this;
    }

    value_type operator*() const
    {
        return _pointer->read(_i);
    }

    value_type operator[](difference_type rhs) const
    {
        return _pointer->read(rhs);
    }

    const_index_iterator& operator++()
    {
        ++_i;
        return *this;
    }

    const_index_iterator& operator--()
    {
        --_i;
        return *this;
    }

    const_index_iterator operator++(int)
    {
        const_index_iterator r(*this);
        this->operator++();
        return r;
    }

    const_index_iterator operator--(int)
    {
        const_index_iterator r(*this);
        this->operator--();
        return r;
    }

    difference_type operator-(const const_index_iterator& rhs) const
    {
        return _i - rhs._i;
    }

    const_index_iterator operator+(difference_type rhs) const
    {
        return const_index_iterator(_pointer, _i + rhs);
    }

    const_index_iterator operator-(difference_type rhs) const
    {
        return const_index_iterator(_pointer, _i - rhs);
    }

    friend const_index_iterator operator+(difference_type lhs, const const_index_iterator& rhs)
    {
        return const_index_iterator(rhs._pointer, lhs + rhs._i);
    }

    friend const_index_iterator operator-(difference_type lhs, const const_index_iterator& rhs)
    {
        return const_index_iterator(rhs._pointer, lhs - rhs._i);
    }

    bool operator==(const const_index_iterator& rhs) const
    {
        return _i == rhs._i;
    }

    bool operator!=(const const_index_iterator& rhs) const
    {
        return _i != rhs._i;
    }

    bool operator>(const const_index_iterator& rhs) const
    {
        return _i > rhs._i;
    }

    bool operator<(const const_index_iterator& rhs) const
    {
        return _i < rhs._i;
    }

    bool operator>=(const const_index_iterator& rhs) const
    {
        return _i >= rhs._i;
    }

    bool operator<=(const const_index_iterator& rhs) const
    {
        return _i <= rhs._i;
    }
};
} /* culcompact */
