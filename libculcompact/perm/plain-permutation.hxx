#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/perm/index-iterator.hxx>
#include <libculcompact/int/fixed-int-array.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/detail/math.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <type_traits>
#include <array>
#include <algorithm>
#include <numeric>
#include <random>
#include <stdexcept>

/* #include <iostream> */

namespace culcompact
{

// stores a static sequence of bits in a plain format that concatenates words of type T to abstract the whole sequence
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class plain_permutation
{
    fixed_int_array<T> _numbers;

public:

    class extension1;

    using iterator = index_iterator<plain_permutation<T>>;
    using const_iterator = const_index_iterator<plain_permutation<T>>;


    plain_permutation() = default;

    plain_permutation(size_t n)
        : _numbers(n, std::floor(std::log2(n)) + 1)
    {
        std::vector<T> tmp(n);
        std::iota(tmp.begin(), tmp.end(), 0);

        std::shuffle(tmp.begin(), tmp.end(),
                std::mt19937 {std::random_device {}()});

        for(size_t i = 0; i < n; ++i)
            _numbers.write(i, tmp[i]);
    }

    // constructs a sequence of integers from a pair of iterators, where elements are castable to integer
    template <detail::IntIterable It>
    plain_permutation(It begin, It end)
        : _numbers(
            std::distance(begin, end),
            std::floor(std::log2(std::distance(begin, end))) + 1)
    {
        /* print(); */
        size_t i = 0;
        for(It it = begin; it != end; ++it, ++i)
            _numbers.write(i, *it);
    }

    // constructs a sequence of integers from an initializer_list with elements castable to integer
    template <detail::IntConvertible Int>
    plain_permutation(std::initializer_list<Int> list)
        : plain_permutation(list.begin(), list.end())
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    size_t operator[](size_t i)
    {
        return _numbers[i];
    }


    // reads int at position i, this is a throwing and non-modifiable version
    size_t read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        return _numbers.read(i);
    }

    void swap(size_t i, size_t j)
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        if(j >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(j, size()));

        if(i == j)
            return;

        T tmp = _numbers[i];
        _numbers[i] = (T) _numbers[j];
        _numbers[j] = tmp;
    }

    // returns the number of elements in the permutation
    size_t size() const
    {
        return _numbers.size();
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _numbers.size_of();
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


// implements the sampled pointers strategy for sum and the binary search strategy for search
template <typename T>
class plain_permutation<T>::extension1
{
    const plain_permutation& _permutation;
    plain_bit_vector<T> _shortcuts;
    fixed_int_array<T> _targets;
    typename plain_bit_vector<T>::extension1 _shortcuts_ext;
    size_t _y;
    size_t _k;


public:

    extension1(const plain_permutation& permutation, size_t y = CULCOMPACT_DEFAULT_Y, size_t k = CULCOMPACT_DEFAULT_K)
        : _permutation(permutation)
        , _shortcuts(permutation.size())
        , _targets(0, 0)
        , _shortcuts_ext(_shortcuts)
        , _y(y)
        , _k(k)

    {
        update();
    }

    void update()
    {
        plain_bit_vector<T> tmp(_permutation.size());
        _shortcuts.clear();

        // update _shortcuts
        for(size_t i = 0; i < _permutation.size(); ++i)
        {
            if(!tmp.read_bit(i))
            {
                tmp.set_bit(i);
                size_t j = _permutation.read(i);
                size_t k = 1;

                // explore new cycle
                while(j != i)
                {
                    if(k % _y == 0)
                        _shortcuts.set_bit(j);

                    tmp.set_bit(j);
                    j = _permutation.read(j);
                    ++k;
                }

                // close the last shortcut if it has lenght greater than T
                if(k > _y)
                    _shortcuts.set_bit(i);
            }
        }

        // update _shortcut structures and allocate _targets
        _shortcuts_ext.update();
        _targets = fixed_int_array<T>(
            _shortcuts_ext.rank1(_permutation.size() + 1),
            std::ceil(std::log2(static_cast<double>(_permutation.size()))));

        // update targets
        for(size_t i = 0; i < _permutation.size(); ++i)
        {
            if(tmp.read_bit(i))
            {
                tmp.clear_bit(i);
                size_t j = _permutation.read(i);

                // now just write the target values for each shortcut
                while(tmp.read_bit(j))
                {
                    if(_shortcuts.read_bit(j))
                    {
                        _targets.write(_shortcuts_ext.rank1(j + 1) - 1, i);
                        i = j;
                    }

                    tmp.clear_bit(j);
                    j = _permutation.read(j);
                }

                // there is a last shortcut?
                if(_shortcuts.read_bit(j))
                    _targets.write(_shortcuts_ext.rank1(j + 1) - 1, i);

                i = j;
            }
        }
    }

    size_t inverse(size_t i) const
    {
        size_t j = i;
        bool s = true;

        while(_permutation.read(j) != i)
        {
            if(s && _shortcuts.read_bit(j))
            {
                j = _targets.read(_shortcuts_ext.rank1(j + 1) - 1);
                s = false;
            }
            else
            {
                j = _permutation.read(j);
            }
        }

        return j;
    }

    size_t power(size_t i, ssize_t k)
    {
        if(k > 0)
        {
            i = _permutation.read(i);

            while(k > 0)
            {
                i = _permutation.read(i);
                --k;
            }
        }
        else
        {
            while(k < 0)
            {
                i = inverse(i);
                ++k;
            }
        }

        return i;
    }
};

} /* culcompact */
