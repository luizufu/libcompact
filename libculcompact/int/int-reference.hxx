#pragma once

#include <cstddef>
#include <cstdint>

namespace culcompact
{

template <typename Ints>
class int_reference
{
    Ints* _pointer;
    uint64_t _integer;
    size_t _i;

public:

    int_reference(Ints* pointer, uint64_t integer, size_t i)
        : _pointer(pointer)
        , _integer(integer)
        , _i(i)
    {
    }

    operator uint64_t() const
    {
        return _integer;
    }

    int_reference& operator=(int_reference other)
    {
        _integer = other._integer;
        _pointer->write(_i, _integer);

        return *this;
    }

    int_reference& operator=(uint64_t integer)
    {
        _integer = integer;
        _pointer->write(_i, _integer);

        return *this;
    }

    int_reference& operator++()
    {
        return operator=(_integer + 1);
    }

    int_reference& operator--()
    {
        return operator=(_integer - 1);
    }

    int_reference operator++(int)
    {
        int_reference tmp(*this);
        operator++();
        return tmp;
    }

    int_reference operator--(int)
    {
        int_reference tmp(*this);
        operator--();
        return tmp;
    }

    int_reference& operator+=(uint64_t rhs)
    {
        return operator=(_integer + rhs);
    }

    int_reference& operator-=(uint64_t rhs)
    {
        return operator=(_integer - rhs);
    }

    int_reference& operator*=(uint64_t rhs)
    {
        return operator=(_integer * rhs);
    }

    int_reference& operator/=(uint64_t rhs)
    {
        return operator=(_integer / rhs);
    }
};


// a proxy reference that enable only access to a bit
template <typename Ints>
class const_int_reference
{
    const Ints* _pointer;
    uint64_t _integer;
    size_t _i;

public:


    const_int_reference(const Ints* pointer, uint64_t integer, size_t i)
        : _pointer(pointer)
        , _integer(integer)
        , _i(i)
    {
    }

    operator uint64_t() const
    {
        return _integer;
    }

    const_int_reference& operator=(const_int_reference other) = delete;
};

} /* culcompact */
