#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/int/int-reference.hxx>
#include <libculcompact/int/int-iterator.hxx>
#include <libculcompact/aux/word.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <libculcompact/detail/binary-search.hxx>
#include <vector>
#include <cmath>
#include <type_traits>
#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace culcompact
{

// stores a static sequence of bits in a plain format that concatenates words of type T to abstract the whole sequence
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class fixed_int_array
{
    static constexpr size_t W = sizeof(T) * 8;
    std::vector<word<T>> _words;
    size_t _n = 0;
    size_t _l = 0;


    void write_int(size_t from, size_t to, uint64_t value)
    {
        // if interval is within the same word just write to corresponding bits
        if(from / W == (to - 1) / W)
        {
            _words[from / W].write_int(
                W - (((to - 1) % W) + 1), W - (from % W),
                value);
        }
        // if not, copy higher or lower bits of value to corresponding words
        else
        {
            _words[from / W].write_int(
                0, W - (from % W),
                value >> (((to - 1) % W) + 1));

            _words[(to - 1) / W].write_int(
                W - ((to - 1) % W + 1), W,
                value & ((1ull << (((to - 1) % W) + 1)) - 1));
        }
    }

    uint64_t read_int(size_t from, size_t to) const
    {
        // if interval is within the same word just read the corresponding bits
        if(from / W == (to - 1) / W)
        {
            return _words[from / W].read_int(
                W - (((to - 1) % W) + 1), W - (from % W));
        }
        // if not, sum higher and lower bits of corresponding words' bit
        else
        {
            return T{0}
                | (_words[from / W].read_int(0, W - (from % W))
                    << (((to - 1) % W) + 1))
                | _words[(to - 1) / W].read_int(W - ((to - 1) % W + 1), W);
        }
    }

public:

    class extension1;
    using iterator = int_iterator<fixed_int_array<T>>;
    using const_iterator = const_int_iterator<fixed_int_array<T>>;
    using reference = int_reference<fixed_int_array<T>>;
    using const_reference = const_int_reference<fixed_int_array<T>>;


    fixed_int_array() = default;

    fixed_int_array(size_t n, size_t l, T v = 0)
        : _words(std::ceil((n * l) / static_cast<double>(W)))
        , _n(n)
        , _l(l)
    {
        // TODO there is no better way to initialize this?
        if(v > 0)
            for(size_t i = 0; i < _words.size(); ++i)
                write(i, v);
    }

    // constructs a sequence of integers from a pair of iterators, where elements are castable to integer
    template <detail::IntIterable It>
    fixed_int_array(It begin, It end, size_t l)
        : fixed_int_array(std::distance(begin, end), l)
    {
        size_t i = 0;
        for(It it = begin; it != end; ++it, i += _l)
            write_int(i, i + _l, *it);
    }

    // constructs a sequence of integers from an initializer_list with elements castable to integer
    template <detail::IntConvertible Int>
    fixed_int_array(std::initializer_list<Int> list, size_t l)
        : fixed_int_array(list.begin(), list.end(), l)
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        return reference(this, read_int(i * _l, i * _l + _l), i);
    }


    // reads int at position i, this is a throwing and non-modifiable version
    const_reference read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        return const_reference(this, read_int(i * _l, i * _l + _l), i);
    }

    // writes int to position i, this is a throwing and non-modifiable version
    void write(size_t i, T integer)
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        write_int(i * _l, i * _l + _l, integer);
    }

    // inserts an integer with value v at the end
    void push_back(T v)
    {
        if(_n + 1 > _words.size() * W)
            _words.emplace_back();

        ++_n;
        write_int(_n * _l, _n * _l + _l, v);
    }

    // removes the last integer
    void pop_back()
    {
        if(_n <= (_words.size() - 1) * W)
            _words.pop_back();

        --_n;
    }

    // resizes the struture in order to accomodate n integers,
    // if n > size() then the value of v is used to fill new positions
    void resize(size_t n, T v = 0)
    {
        _words.resize(std::ceil((n * _l) / static_cast<double>(W)));

        if(v > 0)
        {
            for(size_t i = _n; i < n; ++i)
                write_int(i * _l, i * _l + _l, v);
        }

        _n = n;
    }


    // preallocates memory to accomodate n integers if necessary
    void reserve(size_t n)
    {
        _words.reserve(std::ceil((n * _l) / static_cast<double>(W)));
    }


    // returns the number of elements in the int_array
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _words.size() + sizeof(_words) + sizeof(_n) + sizeof(_l);
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


// implements the sampled pointers strategy for sum and the binary search strategy for search
template <typename T>
class fixed_int_array<T>::extension1
{
    const fixed_int_array& _int_array;
    std::vector<size_t> _sums;
    size_t _s;

    // returns the sum of the int_array inside (from, ..., to]
    size_t sum_slow(size_t from, size_t to) const
    {
        size_t sum = 0;
        for(size_t i = from; i < to; ++i)
            sum += _int_array.read(i);

        return sum;
    }

public:

    extension1(const fixed_int_array& int_array, size_t k = CULCOMPACT_DEFAULT_K)
        : _int_array(int_array)
        , _s(k)
    {
        update();
    }

    // reconstruct structures so that rank/select operations remain consistent
    void update()
    {
        _sums.resize(std::ceil(_int_array.size() / static_cast<double>(_s)));

        size_t i = 0, total_sum = 0;
        for(auto integer : _int_array)
        {
            if(i % _s == 0)
                _sums[i / _s] = total_sum;

            total_sum += integer;
            ++i;
        }
    }


    // returns the summation of numbers from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t i) const
    {
        if(i == 0)
            return 0;

        size_t p = (i - 1) / _s;

        // sum cumulative sum of previous integers and the nexts iterativelly
        return _sums[p] + sum_slow(p * _s, i);
    }

    // returns the summation of numbers from position from to 'to'
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t from, size_t to) const
    {
        return sum(to - 1) - sum(from - 1);
    }

    // select the position of that elements summation are >= j
    // uses a binary search on rank operation
    size_t search(size_t j) const
    {
        size_t n = _int_array.size();

        return detail::lower_bound(0, n + 1,
            [j, this](size_t i) {
                return sum(i) < j;
            });
    }
};

} /* culcompact */
