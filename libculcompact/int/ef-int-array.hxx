#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/int/int-reference.hxx>
#include <libculcompact/int/int-iterator.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <vector>
#include <cmath>
#include <type_traits>
#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace culcompact
{

// stores a static sequence of bits in a plain format that concatenates words of type T to abstract the whole sequence
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class ef_int_array
{
    T _min;
    plain_bit_vector<T> _bits;
    plain_bit_vector<T> _marks;
    typename plain_bit_vector<T>::extension1 _marks_ext;
    size_t _n = 0;


    template <detail::IntIterable It>
    size_t get_final_bits_size(It begin, It end)
    {
        size_t size = 0;
        for(auto it = begin; it != end; ++it)
            size += std::floor(std::log2(static_cast<double>(*it) - _min + 2)); // +1-1

        return size;
    }

    std::pair<size_t, size_t> find_starting_interval(size_t i) const
    {
        size_t begin = _marks_ext.select1(i + 1) - 1;
        size_t end = _marks_ext.select1(i + 2) - 1;

        return { begin, end };
    }


public:

    template <size_t K = CULCOMPACT_DEFAULT_K>
    class extension1;

    using iterator = int_iterator<ef_int_array<T>>;
    using const_iterator = const_int_iterator<ef_int_array<T>>;
    using reference = int_reference<ef_int_array<T>>;
    using const_reference = const_int_reference<ef_int_array<T>>;

    ef_int_array() = default;

    ef_int_array(size_t n, T v = 0)
        : _min(v)
        , _bits(n)
        , _marks(n)
        , _marks_ext(_marks)
        , _n(n)
    {
    }

    // constructs a sequence of integers from a pair of iterators, where elements are castable to integer
    template <detail::IntIterable It>
    ef_int_array(It begin, It end)
        : _min(*std::min_element(begin, end))
        , _bits(get_final_bits_size(begin, end))
        , _marks(_bits.size(), false)
        , _marks_ext(_marks)
        , _n(std::distance(begin, end))
    {
        size_t start_pos = 0;
        for(auto it = begin; it != end; ++it)
        {
            T shifted = *it - _min + 2;
            // remove highest bit from shifted
            size_t code_len = std::floor(std::log2(static_cast<double>(shifted)));
            T code_val = shifted & ((T{1} << code_len) - 1);

            _bits.write_int(start_pos, start_pos + code_len, code_val);
            _marks.set_bit(start_pos);
            start_pos += code_len;
        }

        _marks_ext.update();
    }


    // constructs a sequence of integers from an initializer_list with elements castable to integer
    template <detail::IntConvertible Int>
    ef_int_array(std::initializer_list<Int> list)
        : ef_int_array(list.begin(), list.end())
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        auto [begin, end] = find_starting_interval(i);

        if(begin == end)
            return reference(this, 0, i);

        T integer = _bits.read_int(begin, end) | (T{1} << (end - begin));
        return reference(this, integer + _min - 2, i);
    }


    // reads int at position i, this is a throwing and non-modifiable version
    const_reference read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        auto [begin, end] = find_starting_interval(i);

        if(begin == end)
            return const_reference(this, 0, i);

        T integer = _bits.read_int(begin, end) | (T{1} << (end - begin));
        return const_reference(this, integer + _min - 2, i);
    }

    // writes int to position i, this is a throwing and non-modifiable version
    void write(size_t i, T integer)
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        auto [begin, end] = find_starting_interval(i);
        size_t old_len = end - begin;

        T shifted = integer - _min + 2;
        // remove highest bit from shifted
        size_t code_len = std::floor(std::log2(static_cast<double>(shifted)));
        T code_val = shifted & ((T{1} << code_len) - 1);

        ssize_t diff =
            static_cast<ssize_t>(code_len) - static_cast<ssize_t>(old_len);

        if(diff > 0)
        {
            _bits.insert(begin, static_cast<size_t>(diff));
            _marks.insert(begin, static_cast<size_t>(diff));
        }
        else if(diff < 0)
        {
            _bits.erase(begin, static_cast<size_t>(begin - diff));
            _marks.erase(begin, static_cast<size_t>(begin - diff));
        }

        _bits.write_int(begin, begin + code_len, code_val);
        _marks.write_int(begin, begin + code_len, T{1} << (code_len - 1));
        _marks_ext.update();
    }


    // returns the number of elements in the int_array
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _bits.size_of() + _marks.size_of() + _marks_ext.size_of() + sizeof(_min) + sizeof(_n);
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


// implements the sampled pointers strategy for sum and the binary search strategy for search
template <typename T>
template <size_t K>
class ef_int_array<T>::extension1
{
    static constexpr size_t S = K;

    const ef_int_array& _int_array;
    std::vector<size_t> _sums;

    // returns the sum of the int_array inside (from, ..., to]
    size_t sum_slow(size_t from, size_t to) const
    {
        size_t sum = 0;
        for(size_t i = from; i < to; ++i)
            sum += _int_array.read(i);

        return sum;
    }

public:

    extension1(const ef_int_array& int_array)
        : _int_array(int_array)
    {
        update();
    }

    // reconstruct structures so that rank/select operations remain consistent
    void update()
    {
        _sums.resize(ceil(_int_array.size() / static_cast<double>(S)));
        size_t i = 0, total_sum = 0;
        for(auto integer : _int_array)
        {
            if(i % S == 0)
                _sums[i / S] = total_sum;

            total_sum += integer;
            ++i;
        }
    }


    // returns the summation of numbers from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t i) const
    {
        if(i == 0)
            return 0;

        size_t p = (i - 1) / S;

        // sum cumulative sum of previous integers and the nexts iterativelly
        return _sums[p] + sum_slow(p * S, i);
    }

    // returns the summation of numbers from position from to 'to'
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t from, size_t to) const
    {
        return sum(to - 1) - sum(from - 1);
    }

    // select the position of that elements summation are >= j
    // uses a binary search on rank operation
    size_t search(size_t j) const
    {
        size_t n = _int_array.size();

        return detail::lower_bound(0, n + 1,
            [j, this](size_t i) {
                return sum(i) < j;
            });
    }
};

} /* culcompact */
