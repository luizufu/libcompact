#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/int/int-reference.hxx>
#include <libculcompact/int/int-iterator.hxx>
#include <libculcompact/int/fixed-int-array.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/code/gamma-coder.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <array>
#include <cmath>
#include <type_traits>
#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace culcompact
{

// stores a static sequence of bits in a plain format that concatenates words of type T to abstract the whole sequence
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE,
      typename Coder = gamma_coder<CULCOMPACT_DEFAULT_INT_TYPE>>
class sp_int_array
{
    plain_bit_vector<T> _bits;
    size_t _n = 0;
    size_t _k = 0;
    fixed_int_array<T> _pointers;
    Coder _coder;


    template <detail::IntIterable It>
    size_t final_bits_size(It begin, It end) const
    {
        size_t size = 0;
        for(auto it = begin; it != end; ++it)
            size += _coder.encode(*it + 1).length;

        return size;
    }

    size_t find_starting_position(size_t i) const
    {
        size_t j = _pointers.read(i / _k);
        size_t current = _k * (i / _k);

        for(; current < i; ++current)
            _coder.skip_next(_bits, j);

        return j;
    }


public:

    class extension1;
    using iterator = int_iterator<sp_int_array<T, Coder>>;
    using const_iterator = const_int_iterator<sp_int_array<T, Coder>>;
    using reference = int_reference<sp_int_array<T, Coder>>;
    using const_reference = const_int_reference<sp_int_array<T, Coder>>;


    sp_int_array() = default;

    sp_int_array(size_t n, T v = 0, size_t k = CULCOMPACT_DEFAULT_K, const Coder& coder = gamma_coder<T>())
        : _bits(n * coder.encode(v + 1).length)
        , _n(n)
        , _k(k)
        , _pointers(
            std::ceil(_n / static_cast<double>(_k)), // N
            std::floor(std::log2(static_cast<double>(_bits.size()))) + 1) // L,
        , _coder(coder)
    {
        size_t pi = 0, next_pos = 0;
        auto code = _coder.encode(v + 1);
        for(size_t i = 0; i < n; ++i)
        {
            if((i % _k) == 0)
                _pointers[pi++] = next_pos;

            _bits.write_int(next_pos, next_pos + code.length, code.value);
            next_pos += code.length;
        }
    }

    // constructs a sequence of integers from a pair of iterators, where elements are castable to integer
    template <detail::IntIterable It>
    sp_int_array(It begin, It end, size_t k = CULCOMPACT_DEFAULT_K, const Coder& coder = gamma_coder<T>())
        : _bits(final_bits_size(begin, end))
        , _n(std::distance(begin, end))
        , _k(k)
        , _pointers(
            std::ceil(_n / static_cast<double>(_k)), // N
            std::floor(std::log2(static_cast<double>(_bits.size()))) + 1) // L,
        , _coder(coder)
    {
        size_t i = 0, pi = 0, next_pos = 0;
        for(auto it = begin; it != end; ++it, ++i)
        {
            if((i % _k) == 0)
                _pointers[pi++] = next_pos;

            auto code = _coder.encode(*it + 1);
            _bits.write_int(next_pos, next_pos + code.length, code.value);
            next_pos += code.length;
        }
    }


    // constructs a sequence of integers from an initializer_list with elements castable to integer
    template <detail::IntConvertible Int>
    sp_int_array(std::initializer_list<Int> list, size_t k = CULCOMPACT_DEFAULT_K, const Coder& coder = gamma_coder<T>())
        : sp_int_array(list.begin(), list.end(), k, coder)
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        size_t j = find_starting_position(i);
        return reference(this, _coder.decode_next(_bits, j) - 1, i);
    }


    // reads int at position i, this is a throwing and non-modifiable version
    const_reference read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        size_t j = find_starting_position(i);
        return const_reference(this, _coder.decode_next(_bits, j) - 1, i);
    }

    // returns the number of elements in the int_array
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _bits.size_of() + _pointers.size_of() + sizeof(_n) + sizeof(_k);
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


// implements the sampled pointers strategy for sum and the binary search strategy for search
template <typename T, typename Coder>
class sp_int_array<T, Coder>::extension1
{
    const sp_int_array& _int_array;
    std::vector<size_t> _sums;
    size_t _s;

    // returns the sum of the int_array inside (from, ..., to]
    size_t sum_slow(size_t from, size_t to) const
    {
        size_t sum = 0;
        for(size_t i = from; i < to; ++i)
            sum += _int_array.read(i);

        return sum;
    }

public:

    extension1(const sp_int_array& int_array, size_t k = CULCOMPACT_DEFAULT_K)
        : _int_array(int_array)
        , _s(k)
    {
        update();
    }

    // reconstruct structures so that rank/select operations remain consistent
    void update()
    {
        _sums.resize(std::ceil(_int_array.size() / static_cast<double>(_s)));
        size_t i = 0, total_sum = 0;
        for(auto integer : _int_array)
        {
            if(i % _s == 0)
                _sums[i / _s] = total_sum;

            total_sum += integer;
            ++i;
        }
    }


    // returns the summation of numbers from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t i) const
    {
        if(i == 0)
            return 0;

        size_t p = (i - 1) / _s;

        // sum cumulative sum of previous integers and the nexts iterativelly
        return _sums[p] + sum_slow(p * _s, i);
    }

    // returns the summation of numbers from position from to 'to'
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t from, size_t to) const
    {
        return sum(to - 1) - sum(from - 1);
    }

    // select the position of that elements summation are >= j
    // uses a binary search on rank operation
    size_t search(size_t j) const
    {
        size_t n = _int_array.size();

        return detail::lower_bound(0, n + 1,
            [j, this](size_t i) {
                return sum(i) < j;
            });
    }
};

} /* culcompact */
