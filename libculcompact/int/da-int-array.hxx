#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/int/int-reference.hxx>
#include <libculcompact/int/int-iterator.hxx>
#include <libculcompact/int/fixed-int-array.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <array>
#include <vector>
#include <deque>
#include <cmath>
#include <type_traits>
#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace culcompact
{

// stores a static sequence of bits in a plain format that concatenates words of type T to abstract the whole sequence
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class da_int_array
{
    // TODO deque only in marks, is it actually working?
    std::deque<plain_bit_vector<T>> _marks;// deque doesnt invalidate pointers
    std::vector<fixed_int_array<T>> _parts;
    std::vector<typename plain_bit_vector<T>::extension1> _marks_ext;
    size_t _n = 0;
    size_t _b = 0;


    size_t number_of_blocks(T integer) const
    {
        if(integer == 0)
            return 0;

        size_t len = (std::floor(std::log2(static_cast<double>(integer))) + 1);
        return std::ceil(len / static_cast<double>(_b));
    }

    size_t next(size_t i, size_t r) const
    {
        return _marks_ext[r].rank1(i);
    }

    size_t start_for(size_t i, size_t to)
    {
        for(size_t r = 0; r < to; ++r)
            i = next(i, r);

        return i;
    }


public:

    template <size_t K = CULCOMPACT_DEFAULT_K>
    class extension1;

    using iterator = int_iterator<da_int_array<T>>;
    using const_iterator = const_int_iterator<da_int_array<T>>;
    using reference = int_reference<da_int_array<T>>;
    using const_reference = const_int_reference<da_int_array<T>>;


    da_int_array() = default;

    da_int_array(size_t n, T v = 0, size_t b = CULCOMPACT_DEFAULT_B)
        : _n(n)
        , _b(b)
    {
        T mask = (T{1} << _b) - 1;
        std::vector<T> tmp(n, v);
        while(!tmp.empty())
        {
            _marks.emplace_back(tmp.size());
            _parts.emplace_back(tmp.size(), _b);
            for(size_t i = 0; i < tmp.size(); ++i)
            {
                _parts.back()[i] = tmp[i] & mask;
                tmp[i] = tmp[i] >> _b;

                if(tmp[i] > 0)
                    _marks.back().set_bit(i);
            }

            _marks_ext.emplace_back(_marks.back());
            tmp.erase(std::remove(tmp.begin(), tmp.end(), 0), tmp.end());
        }
    }

    // constructs a sequence of integers from a pair of iterators, where elements are castable to integer
    template <detail::IntIterable It>
    da_int_array(It begin, It end, size_t b = CULCOMPACT_DEFAULT_B)
        : _n(std::distance(begin, end))
        , _b(b)
    {
        T mask = (T{1} << _b) - 1;
        std::vector<T> tmp(begin, end);
        while(!tmp.empty())
        {
            _marks.emplace_back(tmp.size());
            _parts.emplace_back(tmp.size(), _b);
            for(size_t i = 0; i < tmp.size(); ++i)
            {
                _parts.back()[i] = tmp[i] & mask;
                tmp[i] = tmp[i] >> _b;

                if(tmp[i] > 0)
                    _marks.back().set_bit(i);
            }

            _marks_ext.emplace_back(_marks.back());
            tmp.erase(std::remove(tmp.begin(), tmp.end(), 0), tmp.end());
        }
    }


    // constructs a sequence of integers from an initializer_list with elements castable to integer
    template <detail::IntConvertible Int>
    da_int_array(std::initializer_list<Int> list, size_t b = CULCOMPACT_DEFAULT_B)
        : da_int_array(list.begin(), list.end(), b)
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        size_t i_tmp = i;
        T integer = _parts[0][i];
        size_t r = 0;
        while(_marks[r].read_bit(i))
        {
            i = next(i, r);
            ++r;
            integer |= _parts[r][i] << (r * _b);
        }

        return reference(this, integer, i_tmp);
    }


    // reads int at position i, this is a throwing and non-modifiable version
    const_reference read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        size_t i_tmp = i;
        T integer = _parts[0].read(i);
        size_t r = 0;
        while(_marks[r].read_bit(i))
        {
            i = next(i, r);
            ++r;
            integer |= _parts[r].read(i) << (r * _b);
        }

        return const_reference(this, integer, i_tmp);
    }


    // returns the number of elements in the int_array
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _marks.size_of() + _parts.size_of() + _marks_ext.size_of() + sizeof(_n) + sizeof(_b);
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


// implements the sampled pointers strategy for sum and the binary search strategy for search
template <typename T>
template <size_t K>
class da_int_array<T>::extension1
{
    static constexpr size_t S = K;

    const da_int_array& _int_array;
    std::vector<size_t> _sums;

    // returns the sum of the int_array inside (from, ..., to]
    size_t sum_slow(size_t from, size_t to) const
    {
        size_t sum = 0;
        for(size_t i = from; i < to; ++i)
            sum += _int_array.read(i);

        return sum;
    }

public:

    extension1(const da_int_array& int_array)
        : _int_array(int_array)
    {
        update();
    }

    // reconstruct structures so that rank/select operations remain consistent
    void update()
    {
        _sums.resize(ceil(_int_array.size() / static_cast<double>(S)));
        size_t i = 0, total_sum = 0;
        for(auto integer : _int_array)
        {
            if(i % S == 0)
                _sums[i / S] = total_sum;

            total_sum += integer;
            ++i;
        }
    }


    // returns the summation of numbers from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t i) const
    {
        if(i == 0)
            return 0;

        size_t p = (i - 1) / S;

        // sum cumulative sum of previous integers and the nexts iterativelly
        return _sums[p] + sum_slow(p * S, i);
    }

    // returns the summation of numbers from position from to 'to'
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t from, size_t to) const
    {
        return sum(to - 1) - sum(from - 1);
    }

    // select the position of that elements summation are >= j
    // uses a binary search on rank operation
    size_t search(size_t j) const
    {
        size_t n = _int_array.size();

        return detail::lower_bound(0, n + 1,
            [j, this](size_t i) {
                return sum(i) < j;
            });
    }
};

} /* culcompact */
