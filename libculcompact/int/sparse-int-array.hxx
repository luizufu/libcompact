#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/int/int-reference.hxx>
#include <libculcompact/int/int-iterator.hxx>
#include <libculcompact/bit/sparse-bit-vector.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <type_traits>
#include <vector>
#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace culcompact
{


// stores a static sequence of bits in a plain format that concatenates words of type T to abstract the whole sequence
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class sparse_int_array
{
    plain_bit_vector<T> _zeros;
    sparse_bit_vector<T> _bits;
    typename plain_bit_vector<T>::extension1 _zeros_ext;
    typename sparse_bit_vector<T>::extension1 _bits_ext;
    size_t _n = 0;
    size_t _r = 0;

public:

    class extension1;
    using iterator = int_iterator<sparse_int_array<T>>;
    using const_iterator = const_int_iterator<sparse_int_array<T>>;
    using reference = int_reference<sparse_int_array<T>>;
    using const_reference = const_int_reference<sparse_int_array<T>>;


    sparse_int_array() = default;

    sparse_int_array(size_t n, T v = 0, size_t r = CULCOMPACT_DEFAULT_R)
        : _zeros(n, v > 0)
        , _bits(v > 0 ? n * v : 0)
        , _zeros_ext(_zeros)
        , _bits_ext(_bits)
        , _n(n)
        , _r(r)
    {
        if(v > 0)
        {
            size_t sum = 0;
            for(size_t i = 0; i < n; ++i)
            {
                sum += n;
                _bits.set_bit(sum - 1);
            }
        }
    }

    // constructs a sequence of integers from a pair of iterators, where elements are castable to integer
    template <detail::IntIterable It>
    sparse_int_array(It begin, It end, size_t r = CULCOMPACT_DEFAULT_R)
        : _zeros(std::distance(begin, end), false)
        , _bits(std::accumulate(begin, end, 0), r)
        , _zeros_ext(_zeros)
        , _bits_ext(_bits)
        , _n(_zeros.size())
        , _r(r)
    {
        size_t i = 0;
        T sum = 0;
        for(auto it = begin; it != end; ++it, ++i)
        {
            if(*it > 0)
            {
                sum += *it;
                _zeros.set_bit(i);
                _bits.set_bit(sum - 1);
            }
        }

        _zeros_ext.update();
        _bits_ext.update();
    }

    // constructs a sequence of integers from an initializer_list with elements castable to integer
    template <detail::IntConvertible Int>
    sparse_int_array(std::initializer_list<Int> list)
        : sparse_int_array(list.begin(), list.end())
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        if(!_zeros.read_bit(i))
            return reference(this, 0, i);

        size_t ii = _zeros_ext.rank1(i);
        T integer = _bits_ext.select1(ii) - (ii - 1);
        if(ii > 1)
            integer -= _bits_ext.select1(ii - 1) - (ii - 2);

        return reference(this, integer + 1, i);
    }


    // reads int at position i, this is a throwing and non-modifiable version
    const_reference read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        if(!_zeros.read_bit(i))
            return const_reference(this, 0, i);

        size_t ii = _zeros_ext.rank1(i);
        T integer = _bits_ext.select1(ii) - (ii - 1);
        if(ii > 1)
            integer -= _bits_ext.select1(ii - 1) - (ii - 2);

        return const_reference(this, integer + 1, i);
    }

    // writes int to position i, this is a throwing and non-modifiable version
    //
    // TODO needs sparse_bit_vector::insert/erase
    void write(size_t i, [[maybe_unused]] T integer)
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        /* bool was_zero = !_zeros.read_bit(i); */

        /* if(was_zero && integer == 0) */
        /* { */
        /*     return; */
        /* } */
        /* else if(was_zero && integer > 0) */
        /* { */
        /*     size_t ii = _zeros_ext.rank1(i); */
        /*     size_t start = ii > 0 ? _bits_ext.select1(ii) + 1 : 0; */

        /*     _bits.insert(start, integer, false); */
        /*     _bits.set_bit(start + integer - 1); */
        /*     _zeros.set_bit(i); */

        /*     _zeros_ext.update(); */
        /*     _bits_ext.update(); */
        /* } */
        /* else if(!was_zero && integer > 0) */
        /* { */
        /*     size_t ii = _zeros_ext.rank1(i) - 1; */
        /*     size_t start = ii > 0 ? _bits_ext.select1(ii) + 1 : 0; */

        /*     ssize_t diff = integer - read(i); */
        /*     if(diff > 0) */
        /*         _bits.insert(start, (size_t) diff); */
        /*     else if(diff < 0) */
        /*         _bits.erase(start, (size_t) (start - diff)); */

        /*     _bits_ext.update(); */
        /* } */
        /* else // if (!was_zero && integer == 0) */
        /* { */
        /*     size_t ii = _zeros_ext.rank1(i); */
        /*     size_t start = ii > 1 ? _bits_ext.select1(ii - 1) + 1 : 0; */
        /*     size_t end = _bits_ext.select1(ii) + 1; */
        /*     _bits.erase(start, end); */
        /*     _zeros.clear_bit(i); */

        /*     _zeros_ext.update(); */
        /*     _bits_ext.update(); */
        /* } */
    }


    // returns the number of elements in the int_array
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _bits.size_of();
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


// implements the sampled pointers strategy for sum and the binary search strategy for search
template <typename T>
class sparse_int_array<T>::extension1
{
    const sparse_int_array& _int_array;
    std::vector<size_t> _sums;
    size_t _s;

    // returns the sum of the int_array inside (from, ..., to]
    size_t sum_slow(size_t from, size_t to) const
    {
        size_t sum = 0;
        for(size_t i = from; i < to; ++i)
            sum += _int_array.read(i);

        return sum;
    }

public:

    extension1(const sparse_int_array& int_array, size_t k = CULCOMPACT_DEFAULT_K)
        : _int_array(int_array)
        , _s(k)
    {
        update();
    }

    // reconstruct structures so that rank/select operations remain consistent
    void update()
    {
        _sums.resize(std::ceil(_int_array.size() / static_cast<double>(_s)));
        size_t i = 0, total_sum = 0;
        for(auto integer : _int_array)
        {
            if(i % _s == 0)
                _sums[i / _s] = total_sum;

            total_sum += integer;
            ++i;
        }
    }


    // returns the summation of numbers from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t i) const
    {
        size_t ii = _int_array._zeros_ext.rank1(i);
        return ii > 0 ? _int_array._bits_ext.select1(ii) + 1 : 0;
    }

    // returns the summation of numbers from position from to 'to'
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t from, size_t to) const
    {
        return sum(to - 1) - sum(from - 1);
    }

    // select the position of that elements summation are >= j
    // uses a binary search on rank operation
    size_t search(size_t j) const
    {
        if(j == 0)
        {
            return _int_array.read(0) > 0 ?  _int_array.size() : 0;
        }
        else if(j > _int_array._bits.size())
        {
            return _int_array.size();
        }
        else // 0 < j <= _bits.size()
        {
            size_t jj =  _int_array._bits_ext.rank1(j - 1);
            return _int_array._zeros_ext.select1(jj);
        }
    }
};

} /* culcompact */
