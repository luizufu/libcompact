#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/int/int-reference.hxx>
#include <libculcompact/int/int-iterator.hxx>
#include <libculcompact/int/fixed-int-array.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <vector>
#include <cmath>
#include <type_traits>
#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace culcompact
{

// stores a static sequence of bits in a plain format that concatenates words of type T to abstract the whole sequence
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class dp_int_array
{
    T _min;
    plain_bit_vector<T> _bits;
    size_t _n = 0;
    size_t _k = 0;
    fixed_int_array<T> _dense_pointers;
    fixed_int_array<T> _sample_pointers;


    template <detail::IntIterable It>
    size_t get_final_bits_size(It begin, It end)
    {
        size_t size = 0;
        for(auto it = begin; it != end; ++it)
        {
            if(*it == 0)
                continue;

            size += std::floor(std::log2(static_cast<double>(*it) - _min + 1)); // +1-1
        }

        return size;
    }

    std::pair<size_t, size_t> find_starting_interval(size_t i) const
    {
    size_t begin = _sample_pointers.read(i / _k) + _dense_pointers.read(i);
    size_t end = i == _n - 1 ?
        _bits.size() :
        (i + 1) % _k == 0 ?
            _sample_pointers.read((i + 1) / _k) + _dense_pointers.read(i + 1) :
            _sample_pointers.read(i / _k) + _dense_pointers.read(i + 1);


        return { begin, end };
    }


public:

    class extension1;
    using iterator = int_iterator<dp_int_array<T>>;
    using const_iterator = const_int_iterator<dp_int_array<T>>;
    using reference = int_reference<dp_int_array<T>>;
    using const_reference = const_int_reference<dp_int_array<T>>;


    dp_int_array() = default;

    dp_int_array(size_t n, T v = 0, size_t k = CULCOMPACT_DEFAULT_K)
        : _min(v)
        , _bits(0)
        , _n(n)
        , _k(k)
        , _dense_pointers(_n, 1)
        , _sample_pointers(
            std::ceil(_n / static_cast<double>(_k)), // N
            std::floor(std::log2(static_cast<double>(_bits.size()))) + 1) // L,
    {

        for(auto& p : _dense_pointers)
            p = 0;

        for(auto& p : _sample_pointers)
            p = 0;
    }

    // constructs a sequence of integers from a pair of iterators, where elements are castable to integer
    template <detail::IntIterable It>
    dp_int_array(It begin, It end, size_t k = CULCOMPACT_DEFAULT_K)
        : _min(*std::min_element(begin, end))
        , _bits(get_final_bits_size(begin, end))
        , _n(std::distance(begin, end))
        , _k(k)
        , _dense_pointers(
            _n,
            std::floor(std::log2(static_cast<double>(*std::max_element(begin, end)) - _min + 1)))
        , _sample_pointers(
            std::ceil(_n / static_cast<double>(_k)), // N
            std::floor(std::log2(static_cast<double>(_bits.size()))) + 1) // L,
    {
        size_t i = 0, pi = 0, sample_pos = 0, dense_pos = 0;
        for(auto it = begin; it != end; ++it, ++i)
        {
            if((i % _k) == 0)
            {
                dense_pos = 0;
                _sample_pointers[pi++] = sample_pos;
            }

            _dense_pointers[i] = dense_pos;

            if(*it == 0)
                continue;

            T shifted = *it - _min + 1;
            // remove highest bit from shifted
            size_t code_len = std::floor(std::log2(static_cast<double>(shifted)));
            T code_val = shifted & ((T{1} << code_len) - 1);

            _bits.write_int(sample_pos, sample_pos + code_len, code_val);
            sample_pos += code_len;
            dense_pos += code_len;
        }
    }


    // constructs a sequence of integers from an initializer_list with elements castable to integer
    template <detail::IntConvertible Int>
    dp_int_array(std::initializer_list<Int> list)
        : dp_int_array(list.begin(), list.end())
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        auto [begin, end] = find_starting_interval(i);

        if(begin == end)
            return reference(this, 0, i);

        T integer = _bits.read_int(begin, end) | (T{1} << (end - begin));
        return reference(this, integer + _min - 1, i);
    }


    // reads int at position i, this is a throwing and non-modifiable version
    const_reference read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        auto [begin, end] = find_starting_interval(i);

        if(begin == end)
            return const_reference(this, 0, i);

        T integer = _bits.read_int(begin, end) | (T{1} << (end - begin));
        return const_reference(this, integer + _min - 1, i);
    }


    // returns the number of elements in the int_array
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _bits.size_of() + _dense_pointers.size_of() + _sample_pointers.size_of() + sizeof(_min);
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


// implements the sampled pointers strategy for sum and the binary search strategy for search
template <typename T>
class dp_int_array<T>::extension1
{
    const dp_int_array& _int_array;
    std::vector<size_t> _sums;
    size_t _s;

    // returns the sum of the int_array inside (from, ..., to]
    size_t sum_slow(size_t from, size_t to) const
    {
        size_t sum = 0;
        for(size_t i = from; i < to; ++i)
            sum += _int_array.read(i);

        return sum;
    }

public:

    extension1(const dp_int_array& int_array, size_t k = CULCOMPACT_DEFAULT_K)
        : _int_array(int_array)
        , _s(k)
    {
        update();
    }

    // reconstruct structures so that rank/select operations remain consistent
    void update()
    {
        _sums.resize(std::ceil(_int_array.size() / static_cast<double>(_s)));
        size_t i = 0, total_sum = 0;
        for(auto integer : _int_array)
        {
            if(i % _s == 0)
                _sums[i / _s] = total_sum;

            total_sum += integer;
            ++i;
        }
    }


    // returns the summation of numbers from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t i) const
    {
        if(i == 0)
            return 0;

        size_t p = (i - 1) / _s;

        // sum cumulative sum of previous integers and the nexts iterativelly
        return _sums[p] + sum_slow(p * _s, i);
    }

    // returns the summation of numbers from position from to 'to'
    // uses a single layer strategy of pointers to speed up operation
    size_t sum(size_t from, size_t to) const
    {
        return sum(to - 1) - sum(from - 1);
    }

    // select the position of that elements summation are >= j
    // uses a binary search on rank operation
    size_t search(size_t j) const
    {
        size_t n = _int_array.size();

        return detail::lower_bound(0, n + 1,
            [j, this](size_t i) {
                return sum(i) < j;
            });
    }
};

} /* culcompact */
