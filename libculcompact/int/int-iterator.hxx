#pragma once

#include <cstddef>
#include <iterator>

namespace culcompact
{

template <typename Ints>
class int_iterator
{
    Ints* _pointer;
    size_t _i;

public:

    using iterator_category = std::random_access_iterator_tag;
    using value_type = uint64_t;
    using reference = typename Ints::reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    int_iterator()
        : _pointer(nullptr)
        , _i(0)
    {
    }

    int_iterator(Ints* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
    {
    }

    /* int_iterator(const int_iterator& rhs) */
    /*     : _pointer(rhs._pointer) */
    /*     , _i(rhs._i) */
    /* { */
    /* } */

    int_iterator& operator+=(difference_type rhs)
    {
        _i += rhs;
        return *this;
    }

    int_iterator& operator-=(difference_type rhs)
    {
        _i -= rhs;
        return *this;
    }

    reference operator*() const
    {
        return reference(_pointer, _pointer->operator[](_i), _i);
    }

    reference operator[](difference_type rhs) const
    {
        return reference(_pointer, _pointer->operator[](rhs), _i);
    }

    int_iterator& operator++()
    {
        ++_i;
        return *this;
    }

    int_iterator& operator--()
    {
        --_i;
        return *this;
    }

    int_iterator operator++(int)
    {
        int_iterator r(*this);
        this->operator++();
        return r;
    }

    int_iterator operator--(int)
    {
        int_iterator r(*this);
        this->operator--();
        return r;
    }

    difference_type operator-(const int_iterator& rhs) const
    {
        return _i - rhs._i;
    }

    int_iterator operator+(difference_type rhs) const
    {
        return int_iterator(_pointer, _i + rhs);
    }

    int_iterator operator-(difference_type rhs) const
    {
        return int_iterator(_pointer, _i - rhs);
    }

    friend int_iterator operator+(difference_type lhs, const int_iterator& rhs)
    {
        return int_iterator(rhs._pointer, lhs + rhs._i);
    }

    friend int_iterator operator-(difference_type lhs, const int_iterator& rhs)
    {
        return int_iterator(rhs._pointer, lhs - rhs._i);
    }

    bool operator==(const int_iterator& rhs) const
    {
        return _i == rhs._i;
    }

    bool operator!=(const int_iterator& rhs) const
    {
        return _i != rhs._i;
    }

    bool operator>(const int_iterator& rhs) const
    {
        return _i > rhs._i;
    }

    bool operator<(const int_iterator& rhs) const
    {
        return _i < rhs._i;
    }

    bool operator>=(const int_iterator& rhs) const
    {
        return _i >= rhs._i;
    }

    bool operator<=(const int_iterator& rhs) const
    {
        return _i <= rhs._i;
    }
};


template <typename Ints>
class const_int_iterator
{
    const Ints* _pointer;
    size_t _i;

public:

    using iterator_category = std::random_access_iterator_tag;
    using value_type = uint64_t;
    using reference = typename Ints::const_reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    const_int_iterator()
        : _pointer(nullptr)
        , _i(0)
    {
    }

    const_int_iterator(const Ints* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
    {
    }

    /* const_int_iterator(const const_int_iterator& rhs) */
    /*     : _pointer(rhs._pointer) */
    /*     , _i(rhs._i) */
    /* { */
    /* } */

    const_int_iterator& operator+=(difference_type rhs)
    {
        _i += rhs;
        return *this;
    }

    const_int_iterator& operator-=(difference_type rhs)
    {
        _i -= rhs;
        return *this;
    }

    reference operator*() const
    {
        return reference(_pointer, _pointer->read(_i), _i);
    }

    reference operator[](difference_type rhs) const
    {
        return reference(_pointer, _pointer->read(rhs), _i);
    }

    const_int_iterator& operator++()
    {
        ++_i;
        return *this;
    }

    const_int_iterator& operator--()
    {
        --_i;
        return *this;
    }

    const_int_iterator operator++(int)
    {
        const_int_iterator r(*this);
        this->operator++();
        return r;
    }

    const_int_iterator operator--(int)
    {
        const_int_iterator r(*this);
        this->operator--();
        return r;
    }

    difference_type operator-(const const_int_iterator& rhs) const
    {
        return _i - rhs._i;
    }

    const_int_iterator operator+(difference_type rhs) const
    {
        return const_int_iterator(_pointer, _i + rhs);
    }

    const_int_iterator operator-(difference_type rhs) const
    {
        return const_int_iterator(_pointer, _i - rhs);
    }

    friend const_int_iterator operator+(difference_type lhs, const const_int_iterator& rhs)
    {
        return const_int_iterator(rhs._pointer, lhs + rhs._i);
    }

    friend const_int_iterator operator-(difference_type lhs, const const_int_iterator& rhs)
    {
        return const_int_iterator(rhs._pointer, lhs - rhs._i);
    }

    bool operator==(const const_int_iterator& rhs) const
    {
        return _i == rhs._i;
    }

    bool operator!=(const const_int_iterator& rhs) const
    {
        return _i != rhs._i;
    }

    bool operator>(const const_int_iterator& rhs) const
    {
        return _i > rhs._i;
    }

    bool operator<(const const_int_iterator& rhs) const
    {
        return _i < rhs._i;
    }

    bool operator>=(const const_int_iterator& rhs) const
    {
        return _i >= rhs._i;
    }

    bool operator<=(const const_int_iterator& rhs) const
    {
        return _i <= rhs._i;
    }
};

} /* culcompact */
