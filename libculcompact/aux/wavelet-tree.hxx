#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/bit/rrr-bit-vector.hxx>
#include <libculcompact/aux/node-visitor.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <memory>
#include <deque>
#include <vector>
#include <type_traits>
#include <queue>
#include <algorithm>
#include <cassert>

namespace culcompact
{

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE,
          template <typename> class BitVector = plain_bit_vector>
class wavelet_tree
{
public:

    struct node;


private:

    std::unique_ptr<node> _tree;


    template <detail::IntIterable It>
    std::unique_ptr<node> build(It begin, It end, size_t a, size_t b, node* parent, size_t k)
    {
        if(a == b)
            return nullptr;


        if constexpr (std::is_same_v<BitVector<T>, rrr_bit_vector<T>>)
        {
            plain_bit_vector<T> tmp(std::distance(begin, end), false);

            size_t i = 0;
            size_t m = (a + b) / 2;
            std::vector<T> s0, s1;
            for(auto it = begin; it != end; ++it, ++i)
            {
                if(*it <= m)
                {
                    s0.push_back(*it);
                }
                else
                {
                    tmp.set_bit(i);
                    s1.push_back(*it);
                }
            }


            auto n = std::make_unique<node>(tmp.begin(), tmp.end(), k);
            n->parent = parent;
            n->left = build(s0.begin(), s0.end(), a, m, n.get(), k);
            n->right = build(s1.begin(), s1.end(), m + 1, b, n.get(), k);
            n->bits_ext.update();

            return n;
        }
        else
        {
            auto n = std::make_unique<node>(std::distance(begin, end), k);

            size_t i = 0;
            size_t m = (a + b) / 2;
            std::vector<T> s0, s1;
            for(auto it = begin; it != end; ++it, ++i)
            {
                if(*it <= m)
                {
                    s0.push_back(*it);
                }
                else
                {
                    n->bits.set_bit(i);
                    s1.push_back(*it);
                }
            }

            n->parent = parent;
            n->left = build(s0.begin(), s0.end(), a, m, n.get(), k);
            n->right = build(s1.begin(), s1.end(), m + 1, b, n.get(), k);
            n->bits_ext.update();

            return n;
        }
    }

public:

    wavelet_tree() = default;

    template <detail::IntIterable It>
    wavelet_tree(It begin, It end, size_t o, size_t k = CULCOMPACT_DEFAULT_K)
    {
        _tree = build(begin, end, 0, o - 1, nullptr, k);
    }


    template <typename F, typename... Args>
    auto traverse_r(F f, Args&&... args)
    {
        return f(_tree.get(), node_visitor_r<F>(f), std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    auto traverse_r(F f, Args&&... args) const
    {
        return f(_tree.get(), node_visitor_r<F>(f), std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    void traverse_v(F f, Args&&... args)
    {
        f(_tree.get(), node_visitor_v<F>(f), std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    void traverse_v(F f, Args&&... args) const
    {
        f(_tree.get(), node_visitor_v<F>(f), std::forward<Args>(args)...);
    }


    size_t size() const
    {
        return traverse_r(
            [](const node* n, auto visit) {
                if(n->is_leaf())
                    return 1;

                return visit(n->left) + visit(n->right) + 1;
            });
    }

    size_t height() const
    {
        return traverse_r(
            [](const node* n, auto visit) {
                if(n->is_leaf())
                    return 0;

                return std::max(visit(n->left), visit(n->right)) + 1;
            });
    }

    size_t size_of() const
    {
        return traverse_r(
            [](const node* n, auto visit) {
                if(n->is_leaf())
                    return n->size_of();

                return visit(n->left) + visit(n->right) + n->size_of();
            });
    }
};


template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
using cwavelet_tree = wavelet_tree<T, rrr_bit_vector>;



template <typename T,
          template <typename> class BitVector>
struct wavelet_tree<T, BitVector>::node
{
    BitVector<T> bits;
    typename BitVector<T>::extension1 bits_ext;

    node* parent = nullptr;
    std::unique_ptr<node> left;
    std::unique_ptr<node> right;

    node() = default;

    node(size_t k = CULCOMPACT_DEFAULT_K)
        : bits(0)
        , bits_ext(bits, k)
    {
    }

    node(size_t n, size_t k = CULCOMPACT_DEFAULT_K)
        : bits(n)
        , bits_ext(bits, k)
    {
    }

    template <detail::IntIterable It>
    node(It begin, It end, size_t k = CULCOMPACT_DEFAULT_K)
        : bits(begin, end)
        , bits_ext(bits, k)
    {
    }

    bool is_leaf() const
    {
        return !right && !left;
    }

    bool is_root() const
    {
        return !parent;
    }

    size_t size_of() const
    {
        return bits.size_of() + bits_ext.size_of();
    }

};

};
