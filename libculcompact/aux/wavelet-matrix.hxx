#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/bit/rrr-bit-vector.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <deque>
#include <vector>
#include <queue>
#include <algorithm>
#include <cassert>
#include <tuple>
#include <functional>

namespace culcompact
{

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE,
          template <typename> class BitVector = plain_bit_vector>
class wavelet_matrix
{

    std::deque<BitVector<T>> _rows;
    std::deque<typename BitVector<T>::extension1> _rows_ext;
    std::vector<size_t> _zeros;


public:


    wavelet_matrix() = default;

    template <detail::IntIterable It>
    wavelet_matrix(It begin, It end, size_t o, size_t k = CULCOMPACT_DEFAULT_K)
    {
        std::vector<T> seq1(begin, end);
        std::vector<T> seq2(seq1.size());


        size_t h =  std::ceil(std::log2(static_cast<double>(o)));
        size_t n = seq1.size();
        --o;
        for(size_t l = 0; l < h; ++l)
        {
            size_t z = 0;

            if constexpr (std::is_same_v<BitVector<T>, rrr_bit_vector<T>>)
            {
                plain_bit_vector<T> tmp(n, false);

                for(size_t i = 0; i < n; ++i)
                {
                    size_t m = o / 2;
                    if(seq1[i] <= m)
                    {
                        ++z;
                    }
                    else
                    {
                        tmp.set_bit(i);
                        seq1[i] -= m + 1;
                    }
                }

                _rows.emplace_back(tmp.begin(), tmp.end());
                _rows_ext.emplace_back(_rows.back(), k);
                _zeros.push_back(z);
                _rows_ext.back().update();
            }
            else
            {
                _rows.emplace_back(n, false);

                for(size_t i = 0; i < n; ++i)
                {
                    size_t m = o / 2;
                    if(seq1[i] <= m)
                    {
                        ++z;
                    }
                    else
                    {
                        _rows.back().set_bit(i);
                        seq1[i] -= m + 1;
                    }
                }

                _rows_ext.emplace_back(_rows.back(), k);
                _zeros.push_back(z);
                _rows_ext.back().update();
            }


            if(l < h - 1)
            {
                size_t pl = 0;
                size_t pr = z;
                size_t p;

                for(size_t i = 0; i < n; ++i)
                {
                    bool b = _rows.back().read_bit(i);
                    if(!b)
                    {
                        ++pl;
                        p = pl;
                    }
                    else
                    {
                        ++pr;
                        p = pr;
                    }

                    seq2[p - 1] = seq1[i];

                    /* bool is_even = o % 2 == b ? b : m1.read_bit(i); */

                    /* if(!is_even) */
                    /*     m2.clear_bit(p - 1); */
                    /* else */
                    /*     m2.set_bit(p - 1); */

                    /* if(o / 2 == 1 && m2.read_bit(p - 1)) */
                    /*     --n; */
                }

                std::swap(seq1, seq2);
                /* std::swap(m1, m2); */
                if(o / 2 == 1)
                    n -= (n - z);

                o /= 2;
            }
            /* o /= 2; */
        }
    }

    std::tuple<
        std::reference_wrapper<BitVector<T>>,
        std::reference_wrapper<typename BitVector<T>::extension1>,
        size_t>
        row(size_t i)
    {
        if(i >= height())
            throw std::out_of_range(detail::make_out_of_range_msg(i, height()));

        return std::make_tuple(std::ref(_rows[i]), std::ref(_rows_ext[i]), _zeros[i]);
    }

    std::tuple<
        std::reference_wrapper<const BitVector<T>>,
        std::reference_wrapper<const typename BitVector<T>::extension1>,
        size_t>
        row(size_t i) const
    {
        if(i >= height())
            throw std::out_of_range(detail::make_out_of_range_msg(i, height()));

        return std::make_tuple(std::cref(_rows[i]), std::cref(_rows_ext[i]), _zeros[i]);
    }

    size_t width() const
    {
        if(_rows.empty())
            return 0;

        return _rows[0].size();
    }

    size_t height() const
    {
        return _rows.size();
    }

    size_t size_of() const
    {
        size_t s = 0;
        for(size_t i = 0; i < _rows.size(); ++i)
        {
            s += _rows[i].size_of();
            s += _rows_ext[i].size_of();
        }

        return s;
    }

};


template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
using cwavelet_matrix = wavelet_matrix<T, rrr_bit_vector>;


};
