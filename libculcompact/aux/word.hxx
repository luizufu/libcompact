#pragma once

#include <libculcompact/detail/pop-count.hxx>
#include <cstddef>

namespace culcompact
{

template <typename T>
class word
{
    static constexpr size_t W = sizeof(T) * 8;
    T _integer = 0;

public:

    word() = default;
    word(T integer) : _integer(integer) {}

    void set_bit(size_t i)
    {
        _integer |= T{1} << i;
    }

    void clear_bit(size_t i)
    {
        _integer &= ~(T{1} << i);
    }

    void toggle_bit(size_t i)
    {
        _integer ^= T{1} << i;
    }

    bool read_bit(size_t i) const
    {
        return (_integer >> i) & T{1};
    }

    void write_int(size_t from = 0, size_t to = W, T value = ~(T{0}))
    {
        if(to - from == W)
        {
            _integer = value;
            return;
        }

        _integer &= ~(((T{1} << (to - from)) - T{1}) << from);
        _integer |= (value & ((T{1} << (to - from)) - T{1})) << from;
    }

    T read_int(size_t from = 0, size_t to = W) const
    {
        if(to == from)
            return 0;
        else if(to - from == W)
            return _integer;
        else
            return (_integer >> from) & ((T{1} << (to - from)) - T{1});
    }

    size_t count(size_t from = 0, size_t to = W) const
    {
        if(to == from)
            return 0;
        else if(to - from == W)
            return detail::pop_count(_integer);
        else
            return detail::pop_count(static_cast<T>(
                        (_integer >> from) & ((T{1} << (to - from)) - 1)));
    }

    // TODO leading_zeros clz
    // TODO trailing_zeros ctz
};

} /* culcompact */
