#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/bit/rrr-bit-vector.hxx>
#include <libculcompact/code/code.hxx>
#include <libculcompact/aux/huffman-tree.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <libculcompact/detail/compressible-concept.hxx>
#include <type_traits>
#include <array>

namespace culcompact
{

// stores a static sequence of bits in a plain format that concatenates words of type T to abstract the whole sequence
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE,
          template <typename> class BitVector = plain_bit_vector>
class hwavelet_tree
{
public:
    // stores bitvector and extension together
    struct bitvector_ext_t;


private:

    huffman_tree<bitvector_ext_t> _tree;


public:

    hwavelet_tree() = default;

    // constructs a sequence of integers from a pair of iterators, where elements are castable to integer
    template <detail::IntIterable It>
    hwavelet_tree(It begin, It end, size_t o, size_t k = CULCOMPACT_DEFAULT_K)
    {
        std::vector<bitvector_ext_t> symbols;
        symbols.reserve(o);
        for(size_t i = 0; i < o; ++i)
            symbols.emplace_back(i, k);

        std::vector<float> frequency(o, 0);
        for(auto it = begin; it != end; ++it)
            ++frequency[*it];

        // construct huffman tree of symbols as bitvectors
        _tree = huffman_tree<bitvector_ext_t>(symbols, frequency);

        // get the code for every symbol traversing the whole tree
        size_t i = 0;
        std::vector<code<T>> codes(o);
        _tree.traverse_v(
            [&i, &codes](auto* n, auto visit, code<T> mcode) {
                if(n->is_leaf())
                {
                    codes[n->symbol] = mcode;
                    return;
                }

                visit(n->left, code<T>{(mcode.value << 1) | 0, mcode.length + 1});
                visit(n->right,code<T>{(mcode.value << 1) | 1, mcode.length + 1});
            }, code<T>{ 0, 0 });


        // update bitvectors of internal nodes, using the sequence and the codes
        _tree.traverse_v(
            [&](auto* n, auto visit, auto jbegin, auto jend, size_t depth) {
                if(n->is_leaf())
                    return;

                plain_bit_vector<T> tmp(std::distance(jbegin, jend), false);

                size_t j = 0;
                std::vector<T> s0, s1;
                for(auto it = jbegin; it != jend; ++it, ++j)
                {
                    if(!((codes[*it].value >> (codes[*it].length - depth)) & 1))
                    {
                        s0.push_back(*it);
                    }
                    else
                    {
                        tmp.set_bit(j);
                        s1.push_back(*it);
                    }
                }

                n->symbol.bitvector = BitVector<T>(tmp.begin(), tmp.end());
                n->symbol.bitvector_ext.update();
                visit(n->left, s0.begin(), s0.end(), depth + 1);
                visit(n->right, s1.begin(), s1.end(), depth + 1);

            }, begin, end, 1);
    }

    // constructs a sequence of integers from an initializer_list with elements castable to integer
    template <typename Int>
    hwavelet_tree(std::initializer_list<Int> list)
        : hwavelet_tree(list.begin(), list.end())
    {
    }

    template <typename F, typename... Args>
    auto traverse_r(F f, Args&&... args)
    {
        return _tree.traverse_r(f, std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    auto traverse_r(F f, Args&&... args) const
    {
        return _tree.traverse_r(f, std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    void traverse_v(F f, Args&&... args)
    {
        _tree.traverse_v(f, std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    void traverse_v(F f, Args&&... args) const
    {
        _tree.traverse_v(f, std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    auto traverse_from_r(T symbol, F f, Args&&... args)
    {
        return _tree.traverse_from_r(symbol, f, std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    auto traverse_from_r(T symbol, F f, Args&&... args) const
    {
        return _tree.traverse_from_r(symbol, f, std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    void traverse_from_v(T symbol, F f, Args&&... args)
    {
        _tree.traverse_from_v(symbol, f, std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    void traverse_from_v(T symbol, F f, Args&&... args) const
    {
        _tree.traverse_from_v(symbol, f, std::forward<Args>(args)...);
    }


    size_t size() const
    {
        return _tree.size();
    }

    size_t height() const
    {
        return _tree.height();
    }

    size_t size_of() const
    {
        return _tree.size_of();
    }
};


template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
using chwavelet_tree = hwavelet_tree<T, rrr_bit_vector>;

template <typename T,
          template <typename> class BitVector>
struct hwavelet_tree<T, BitVector>::bitvector_ext_t
{
    BitVector<T> bitvector;
    typename BitVector<T>::extension1 bitvector_ext;

    bitvector_ext_t()
        requires detail::NotCompressible<BitVector>
        : bitvector()
        , bitvector_ext(bitvector)

    {
    }

    bitvector_ext_t(T id, size_t k = CULCOMPACT_DEFAULT_K)
        requires detail::NotCompressible<BitVector>
        : bitvector(id > 0  ? std::floor(std::log2(id)) + 1 : 1)
        , bitvector_ext(bitvector, k)
    {
        bitvector.write_int(0, bitvector.size(), id);
    }

    bitvector_ext_t()
        requires detail::Compressible<BitVector>
        : bitvector(0)
        , bitvector_ext(bitvector)
    {
    }

    bitvector_ext_t(T id, size_t k = CULCOMPACT_DEFAULT_K)
        requires detail::Compressible<BitVector>
        : bitvector(0)
        , bitvector_ext(bitvector, k)
    {
        plain_bit_vector<T> tmp(id > 0  ? std::floor(std::log2(id)) + 1 : 1);
        tmp.write_int(0, tmp.size(), id);

        bitvector = BitVector<T>(tmp.begin(), tmp.end(), CULCOMPACT_DEFAULT_B, k);
        bitvector_ext.update();
    }

    operator size_t() const
    {
        if(bitvector.size() == 0)
            return ~size_t{0};

        return bitvector.read_int(0, bitvector.size());
    }
};

} /* culcompact */
