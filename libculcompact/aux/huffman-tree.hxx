#pragma once

#include <libculcompact/code/code.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/aux/node-visitor.hxx>
#include <memory>
#include <deque>
#include <vector>
#include <type_traits>
#include <queue>
#include <algorithm>
#include <cassert>
#include <iostream>

namespace culcompact
{

template <typename T>
class huffman_tree
{
public:
    struct node;

private:

    std::unique_ptr<node> _tree;
    std::deque<node*> _leaves; // deque doesn't invalidate pointers

public:

    huffman_tree() = default;

    huffman_tree(const std::vector<T>& symbols, const std::vector<float>& weights)
        : _leaves(symbols.size())
    {

        // will hold a priority queue
        std::vector<std::unique_ptr<node>> queue;

        auto comp = [](const auto& lhs, const auto& rhs) {
            return lhs->weight > rhs->weight;
        };
        auto q_push = [&](std::unique_ptr<node> p) {
            queue.push_back(std::move(p));
            std::push_heap(queue.begin(), queue.end(), comp);
        };
        auto q_pop = [&]() {
            std::pop_heap(queue.begin(), queue.end(), comp);
            auto result = std::move(queue.back());
            queue.pop_back();
            return result;
        };


        for(size_t i = 0; i < symbols.size(); ++i)
            q_push(std::make_unique<node>(symbols[i], weights[i]));

        // remove two nodes with the lowest weights, join and push it back
        while(queue.size() > 1)
        {
            auto n = std::make_unique<node>();
            n->left = q_pop();
            n->right = q_pop();
            n->weight = n->right->weight + n->left->weight;

            n->left->parent = n.get();
            n->right->parent = n.get();

            q_push(std::move(n));
        }

        // set tree so we can call traverse member function
        _tree = q_pop();

        // set link to leaf nodes by traversing the whole tree
        traverse_v(
            [this](node* n, auto visit) mutable {
                if(n->is_leaf())
                {
                    _leaves[n->symbol] = n;
                    return;
                }

                visit(n->left);
                visit(n->right);
            });
    }


    template <typename F, typename... Args>
    auto traverse_r(F f, Args&&... args)
    {
        return f(_tree.get(), node_visitor_r<F>(f), std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    auto traverse_r(F f, Args&&... args) const
    {
        return f(_tree.get(), node_visitor_r<F>(f), std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    void traverse_v(F f, Args&&... args)
    {
        f(_tree.get(), node_visitor_v<F>(f), std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    void traverse_v(F f, Args&&... args) const
    {
        f(_tree.get(), node_visitor_v<F>(f), std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    auto traverse_from_r(T symbol, F f, Args&&... args)
    {
        return f(_leaves[symbol], node_visitor_v<F>(f), std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    auto traverse_from_r(T symbol, F f, Args&&... args) const
    {
        return f(_leaves[symbol], node_visitor_v<F>(f), std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    void traverse_from_v(T symbol, F f, Args&&... args)
    {
        f(_leaves[symbol], node_visitor_v<F>(f), std::forward<Args>(args)...);
    }

    template <typename F, typename... Args>
    void traverse_from_v(T symbol, F f, Args&&... args) const
    {
        f(_leaves[symbol], node_visitor_v<F>(f), std::forward<Args>(args)...);
    }


    template <typename F>
    void traverse_leaves(F f)
    {
        for(const node* leaf : _leaves)
            f(leaf);
    }

    template <typename F>
    void traverse_leaves(F f) const
    {
        for(const node* leaf : _leaves)
            f(leaf);
    }


    size_t size() const
    {
        return traverse_r(
            [](const node* n, auto visit) {
                if(n->is_leaf())
                    return 1;

                return visit(n->left) + visit(n->right) + 1;
            });
    }

    size_t height() const
    {
        return traverse_r(
            [](const node* n, auto visit) {
                if(n->is_leaf())
                    return 0;

                return std::max(visit(n->left), visit(n->right)) + 1;
            });
    }

    size_t size_of() const
    {
        return traverse_v(
            [](const node* n, auto visit) {
                if(n->is_leaf())
                    return n->size_of();

                return visit(n->left) + visit(n->right) + n->size_of();
            });
    }
};


template <typename T>
struct huffman_tree<T>::node
{
    T symbol;
    float weight = 0;

    node* parent = nullptr;
    std::unique_ptr<node> left;
    std::unique_ptr<node> right;


    node() = default;

    node(T s, float p)
        : symbol(s)
        , weight(p)
    {
    }


    bool is_leaf() const
    {
        return !right && !left;
    }

    bool is_root() const
    {
        return !parent;
    }

    size_t size_of() const
    {
        return sizeof(symbol) + sizeof(weight)
            + sizeof(parent) + sizeof(left) + sizeof(right);
    }

};

};
