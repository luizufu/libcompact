#pragma once

#include <utility>
#include <memory>

namespace culcompact
{

template <typename F>
class node_visitor_v
{
    F _f;

public:

    node_visitor_v(F f)
        : _f(f)
    {
    }

    template <typename Node, typename... Args>
    void operator () (const std::unique_ptr<Node>& n, Args&&... args) const
    {
        if(n)
            _f(n.get(), *this, std::forward<Args>(args)...);
    }

    template <typename Node, typename... Args>
    void operator () (std::unique_ptr<Node>& n, Args&&... args)
    {
        if(n)
            _f(n.get(), *this, std::forward<Args>(args)...);
    }

    template <typename Node, typename... Args>
    void operator () (const Node* n, Args&&... args) const
    {
        if(n)
            _f(n, *this, std::forward<Args>(args)...);
    }

    template <typename Node, typename... Args>
    void operator () (Node* n, Args&&... args)
    {
        if(n)
            _f(n, *this, std::forward<Args>(args)...);
    }

};

template <typename F>
class node_visitor_r
{
    F _f;

public:

    node_visitor_r(F f)
        : _f(f)
    {
    }

    template <typename Node, typename... Args>
    auto operator () (const std::unique_ptr<Node>& n, Args&&... args) const
        -> decltype(_f(n.get(), *this, std::forward<Args>(args)...))
    {
        if(n)
            return _f(n.get(), *this, std::forward<Args>(args)...);
        else
            return {};
    }

    template <typename Node, typename... Args>
    auto operator () (std::unique_ptr<Node>& n, Args&&... args)
        -> decltype(_f(n.get(), *this, std::forward<Args>(args)...))
    {
        if(n)
            return _f(n.get(), *this, std::forward<Args>(args)...);
        else
            return {};
    }

    template <typename Node, typename... Args>
    auto operator () (const Node* n, Args&&... args) const
        -> decltype(_f(n, *this, std::forward<Args>(args)...))
    {
        if(n)
            return _f(n, *this, std::forward<Args>(args)...);
        else
            return {};
    }

    template <typename Node, typename... Args>
    auto operator () (Node* n, Args&&... args)
        -> decltype(_f(n, *this, std::forward<Args>(args)...))
    {
        if(n)
            return _f(n, *this, std::forward<Args>(args)...);
        else
            return {};
    }

};

}
