#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/code/code.hxx>
#include <libculcompact/aux/huffman-tree.hxx>

#include <memory>
#include <vector>
#include <functional>

namespace culcompact
{


template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class huffman_coder
{
    huffman_tree<T> _tree;

public:
    huffman_coder(const std::vector<T>& symbols, const std::vector<float>& probs)
        : _tree(symbols, probs)
    {
    }

    code<T> encode(T s) const
    {
        code<T> c = { 0, 0 };
        _tree.traverse_from_v(s,
            [&c](const auto* n, auto visit) {
                if(!n->is_root())
                {
                    if(n == n->parent->right.get())
                        c.value |= (T{1} << c.length);
                    ++c.length;

                    visit(n->parent);
                }
            });

        return c;
    }

    T decode(code<T> c) const
    {
        T symbol = 0;
        _tree.traverse_v(
            [&](const auto* n, auto visit) {
                if(n->is_leaf())
                {
                    symbol = n->symbol;
                    return;
                }

                auto [value, length] = c;
                c.value &= (T{1} << (c.length - 1)) - 1;
                --c.length;

                if(!(value >> (length - 1)))
                    visit(n->left);
                else
                    visit(n->right);
            });

        return symbol;
    }

    template <typename BitContainer>
    T decode_next(const BitContainer& bits, size_t& i) const
    {
        T symbol = 0;
        _tree.traverse_v(
            [&](const auto* node, auto visit) {
                if(node->is_leaf())
                {
                    symbol = node->symbol;
                    return;
                }

                if(!bits.read_bit(i++))
                    visit(node->left);
                else
                    visit(node->right);
            });

        return symbol;
    }

    template <typename BitContainer>
    void skip_next(const BitContainer& bits, size_t& i) const
    {
        _tree.traverse_v(
            [&](const auto* node, auto visit) {
                if(node->is_leaf())
                    return;

                if(!bits.read_bit(i++))
                    visit(node->left);
                else
                    visit(node->right);
            });
    }

    const huffman_tree<T>& tree() const
    {
        return _tree;
    }
};

} /* culcompact */
