#pragma once

#include <libculcompact/code/code.hxx>
#include <libculcompact/config.hxx>
#include <cstddef>
#include <cmath>

namespace culcompact
{

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class gamma_coder
{
public:

    code<T> encode(T x) const
    {
        return { x, 2 * static_cast<size_t>(std::log2(x)) + 1 };
    }

    T decode(code<T> c) const
    {
        return c.value;
    }

    template <typename BitContainer>
    T decode_next(const BitContainer& bits, size_t& i) const
    {
        size_t zeros = 0;
        while(!bits.read_bit(i++))
            ++zeros;

        size_t start = i - 1;
        i += zeros;

        return bits.read_int(start, i);
    }

    template <typename BitContainer>
    void skip_next(const BitContainer& bits, size_t& i) const
    {
        size_t zeros = 0;
        while(!bits.read_bit(i++))
            ++zeros;

        i += zeros;
    }
};

} /* culcompact */
