#pragma once

#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/code/unary-coder.hxx>
#include <libculcompact/code/code.hxx>
#include <libculcompact/config.hxx>
#include <cstddef>


namespace culcompact
{

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class rice_coder
{
    size_t _l;
    unary_coder<T> _unary;

public:

    rice_coder(size_t l)
        : _l(l)
    {
    }

    code<T> encode(T x) const
    {
        T y = x >> _l;

        code<T> uc = _unary.encode(y + 1);
        T lbit = uc.value << _l;

        return  { lbit |  (x & (lbit - T{1})), _l + uc.length};
    }

    T decode(code<T> c) const
    {
        plain_bit_vector<T> bits(c.length, c.value);

        size_t i = 0;
        T y = _unary.decode_next(bits, i) - 1;

        return (y << _l) | bits.read_int(i, i + _l);
    }

    template <typename BitContainer>
    T decode_next(const BitContainer& bits, size_t& i) const
    {
        T y = _unary.decode_next(bits, i) - 1;

        T rvalue = (y << _l) | bits.read_int(i, i + _l);
        i += _l;

        return rvalue;
    }

    template <typename BitContainer>
    void skip_next(const BitContainer& bits, size_t& i) const
    {
        _unary.skip_next(bits, i);
        i += _l;
    }
};

} /* culcompact */
