#pragma once

#include <libculcompact/code/code.hxx>
#include <libculcompact/config.hxx>
#include <cmath>
#include <cstddef>

namespace culcompact
{

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class unary_coder
{
public:

    code<T> encode(T x) const
    {
        return { T{1}, static_cast<size_t>(x) };
    }

    T decode(code<T> c) const
    {
        return c.length;
    }

    template <typename BitContainer>
    T decode_next(const BitContainer& bits, size_t& i) const
    {
        size_t length = 1;

        while(!bits.read_bit(i++))
            ++length;

        return length;
    }

    template <typename BitContainer>
    void skip_next(const BitContainer& bits, size_t& i) const
    {
        while(!bits.read_bit(i++));
    }
};

} /* culcompact */
