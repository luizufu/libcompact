#pragma once

#include <libculcompact/config.hxx>
#include <cstddef>
#include <string>

namespace culcompact
{

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
struct code
{
    T value;
    size_t length;

    template <typename OtherT>
    bool operator==(const code<OtherT>& other) const
    {
        return value == other.value && length == other.length;
    }

    template <typename OtherT>
    bool operator!=(const code<OtherT>& other) const
    {
        return !operator==(other);
    }

    template <typename OtherT>
    bool operator<(const code<OtherT>& other) const
    {
        return std::pair(value, length) < std::pair(other.value, other.length);
    }

    template <typename OtherT>
    bool operator>(const code<OtherT>& other) const
    {
        return std::pair(value, length) > std::pair(other.value, other.length);
    }
};

// for testing
template<typename T>
std::string to_string(const code<T>& code) {
    return "{"
        + std::to_string(code.value) + ", "
        + std::to_string(code.length) + "}";
}

} /* culcompact */
