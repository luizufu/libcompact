#pragma once

#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/code/gamma-coder.hxx>
#include <libculcompact/code/code.hxx>
#include <libculcompact/config.hxx>
#include <cmath>
#include <cstddef>

namespace culcompact
{

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class delta_coder
{
    gamma_coder<T> _gamma;

public:

    code<T> encode(T x) const
    {
        size_t mag = static_cast<size_t>(std::log2(x)) + 1;
        code<T> gcode = _gamma.encode(mag);

        return { x +  ((gcode.value - 1) << (mag-1)), gcode.length + mag - 1 };
    }

    T decode(code<T> c) const
    {
        plain_bit_vector<T> bits(c.length, c.value);

        size_t i = 0; // need to pass lvalue
        T mag = _gamma.decode_next(bits, i);
        T mask = T{1} << (mag - 1);

        return (c.value & (mask - 1)) | mask;
    }

    template <typename BitContainer>
    T decode_next(const BitContainer& bits, size_t& i) const
    {
        T mag = _gamma.decode_next(bits, i);
        T mask = T{1} << (mag - 1);

        T dvalue =  bits.read_int(i, i + mag - 1) | mask;
        i += mag - 1;

        return dvalue;
    }

    template <typename BitContainer>
    void skip_next(const BitContainer& bits, size_t& i) const
    {
        T gvalue = _gamma.decode_next(bits, i);
        i += gvalue - 1;
    }
};

} /* culcompact */
