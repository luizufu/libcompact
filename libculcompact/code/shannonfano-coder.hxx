#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/code/code.hxx>
#include <libculcompact/int/fixed-int-array.hxx>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <cassert>

namespace culcompact
{

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class shannonfano_coder
{
    fixed_int_array<T> _symbols;
    fixed_int_array<T> _codes;
    fixed_int_array<T> _lengths;

public:
    shannonfano_coder(const std::vector<T>& symbols, const std::vector<size_t>& lengths)
    {
        // sort symbols by increasing length using indexes vector
        std::vector<size_t> indexes(symbols.size());
        std::iota(indexes.begin(), indexes.end(), 0);
        std::sort(
            indexes.begin(), indexes.end(),
            [&lengths] (auto lhs, auto rhs) {
                return lengths[lhs] < lengths[rhs];
            });

        // compute some info and initialize structures
        T max_symbol = *std::max_element(symbols.begin(), symbols.end());
        size_t h = lengths[indexes.back()];

        _symbols = fixed_int_array<T>(
            symbols.size(), std::floor(std::log2(max_symbol)) + 1);
        _lengths = fixed_int_array<T>(
            symbols.size(), std::floor(std::log2(h)) + 1);
        _codes = fixed_int_array<T>(symbols.size(), h);

        // set structures and compute codes
        _symbols[0] = symbols[indexes[0]];
        _lengths[0] = lengths[indexes[0]];
        for(size_t i = 1; i < indexes.size(); ++i)
        {
            _symbols[i] = symbols[indexes[i]];
            _lengths[i] = lengths[indexes[i]];
            _codes[i] = (_codes[i - 1] + 1) << (_lengths[i] - _lengths[i - 1]);
        }
    }

    code<T> encode(T& x) const
    {
        auto it = std::find(_symbols.begin(), _symbols.end(), x);
        assert(it != _symbols.end() && "symbols x is not known");

        size_t i = std::distance(_symbols.begin(), it);
        return {
            static_cast<T>(_codes.read(i)),
            static_cast<size_t>(_lengths.read(i)) };
    }

    T decode(code<T> c) const
    {
        auto it = std::lower_bound(_codes.begin(), _codes.end(), c.value);
        assert(it != _codes.end() && "code c is not known");

        size_t i = std::distance(_symbols.begin(), it);
        return _symbols.read(i);
    }

    template <typename BitContainer>
    T decode_next(const BitContainer& bits, size_t& i) const
    {
        size_t j = i;
        size_t k = 0;
        T current_code = 0;

        do
        {
            current_code = (current_code << 1) | bits.read_bit(j++);
            while(k < _symbols.size()
                && (j - i) <= _lengths.read(k)
                && current_code > _codes.read(k))
            {
                ++k;
            }
        }
        while(k < _symbols.size() && current_code != _codes.read(k));

        assert(k < _symbols.size() && "invalid bit stream");

        i = j;
        return _symbols.read(k);
    }

    template <typename BitContainer>
    void skip_next(const BitContainer& bits, size_t& i) const
    {
        decode_next(bits, i);
    }

    const fixed_int_array<T>& symbols() const
    {
        return _symbols;
    }

    const fixed_int_array<T>& lengths() const
    {
        return _lengths;
    }

    const fixed_int_array<T>& codes() const
    {
        return _codes;
    }
};

} /* culcompact */
