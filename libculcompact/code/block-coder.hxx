#pragma once

#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/detail/math.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <libculcompact/config.hxx>
#include <cstddef>

namespace culcompact
{

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
struct block_code
{
    size_t c;
    T o;

    bool operator==(const block_code& other) const
    {
        return c == other.c && o == other.o;
    }

    bool operator!=(const block_code& other) const
    {
        return !operator==(other);
    }
};

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class block_coder
{
    size_t _b;

public:


    block_coder(size_t b)
        : _b(b)
    {
    }

    template <typename BitContainer>
    block_code<T> encode(const BitContainer& block) const
    {
        size_t i = 0;
        return encode_next(block, i);
    }


    plain_bit_vector<T> decode(block_code<T> c) const
    {
        plain_bit_vector<T> block(_b);
        size_t j = 0;

        while(c.c > 0)
        {
            if(c.o >= detail::combination(_b - j - 1, c.c))
            {
                block.set_bit(j);
                c.o -= detail::combination(_b - j - 1, c.c);
                --c.c;
            }
            ++j;
        }

        return block;
    }


    template <typename BitContainer>
    block_code<T> encode_next(const BitContainer& bits, size_t& i) const
    {
        size_t c = 0;
        for(size_t ii = i, end = i + _b; ii < end; ++ii)
            if(bits.read_bit(ii))
                ++c;

        size_t c2 = c;
        T o = 0;
        size_t j = 0;

        while(c2 > 0 && c2 < (_b - j))
        {
            if(bits.read_bit(i + j))
            {
                o += detail::combination(_b - j - 1, c2);
                --c2;
            }
            ++j;
        }

        i += _b;

        return { c,  o };
    }

    template <typename BitContainer>
    void skip_next(const BitContainer&, size_t& i) const
    {
        i += _b;
    }
};

} /* culcompact */
