#pragma once

#include <libculcompact/code/code.hxx>
#include <libculcompact/config.hxx>
#include <cstddef>

namespace culcompact
{

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class vbyte_coder
{
public:

    code<T> encode(T x) const
    {
        T c = 0;
        size_t len = 0;

        T mask = (T{1} << 7) - T{1};

        while(x >= 128)
        {
            c |= (x & mask) << len;
            x = x >> 7;
            len += 8;
        }

        c |= (x & mask) << len;
        len += 8;

        return { c + 128, len };
    }

    T decode(code<T> c) const
    {
        T x = 0;
        T next_val = 0;
        T mask = (T{1} << 8) - T{1};

        while((next_val = ((c.value >> (c.length - 8)) & mask)) < 128)
        {
            x = (x + next_val) << 7;
            c.length -= 8;
        }

        x += next_val;

        return x - 128;
    }

    template <typename BitContainer>
    T decode_next(const BitContainer& bits, size_t& i) const
    {
        T x = 0;
        T next_val = 0;

        while((next_val = bits.read_int(i, i + 8)) < 128)
        {
            x = (x + next_val) << 7;
            i += 8;
        }

        x += next_val;
        i += 8;

        return x - 128;
    }

    template <typename BitContainer>
    void skip_next(const BitContainer& bits, size_t& i) const
    {
        T next_val = 0;

        while((next_val = bits.read_int(i, i + 8)) < 128)
            i += 8;

        i += 8;
    }
};

} /* culcompact */
