#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/code/code.hxx>
#include <libculcompact/code/huffman-coder.hxx>
#include <libculcompact/code/shannonfano-coder.hxx>
#include <libculcompact/int/fixed-int-array.hxx>

#include <vector>
#include <algorithm>
#include <numeric>
#include <cassert>

namespace culcompact
{

template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class chuffman_coder
{
    fixed_int_array<T> _symbols;
    fixed_int_array<T> _first_of_length;
    fixed_int_array<T> _code_of_first;

public:
    chuffman_coder(const std::vector<T>& symbols, const std::vector<float>& probs)
    {
        // construct huffman tree
        huffman_coder<T> hcoder(symbols, probs);

        // retrieve lengths using huffman tree and construct shannonfano coder
        std::vector<size_t> lengths(symbols.size());
        for(size_t i = 0; i < symbols.size(); ++i)
            lengths[i] = hcoder.encode(symbols[i]).length;

        shannonfano_coder<T> scoder(symbols, lengths);

        // retrieve information of shannonfano strutures
        const auto& ssymbols = scoder.symbols();
        const auto& slengths = scoder.lengths();
        const auto& scodes = scoder.codes();
        size_t h = slengths.read(slengths.size() - 1);
        T max_symbol = *std::max_element(symbols.begin(), symbols.end());

        // initialize structures
        _symbols = fixed_int_array<T>(
            symbols.size(), std::floor(std::log2(max_symbol)) + 1);
        _first_of_length = fixed_int_array<T>(
            h, std::floor(std::log2(static_cast<double>(symbols.size()))) + 1, ~T{0});
        _code_of_first = fixed_int_array<T>(h, h, ~T{0});

        // copy symbols in order of lenght
        for(size_t i = 0; i < ssymbols.size(); ++i)
            _symbols[i] = ssymbols.read(i);

        // set values needed for encoding/decoding in descending order
        for(size_t i = ssymbols.size(); i > 0; --i)
        {
            _first_of_length[slengths.read(i - 1) - 1] = i - 1;
            _code_of_first[slengths.read(i - 1) - 1] = scodes.read(i - 1);
        }

        for(size_t l = h; l > 1; --l)
        {
            if(_first_of_length[l - 2] == ~T{0})
            {
                _first_of_length[l - 2] = _first_of_length[l - 1];
                _code_of_first[l - 2] = _code_of_first[l - 1] >> 1;
            }
        }
    }

    code<T> encode(T s) const
    {
        auto it = std::find(_symbols.begin(), _symbols.end(), s);
        assert(it != _symbols.end() && "symbol not recognized");
        size_t i = std::distance(_symbols.begin(), it);

        auto it2 = std::upper_bound(
            _first_of_length.begin(), _first_of_length.end(), i);
        size_t j = std::distance(_first_of_length.begin(), it2) - 1;

        return {
            static_cast<T>(_code_of_first.read(j) + i -
                    _first_of_length.read(j)),
            static_cast<size_t>(_first_of_length.read(j) + 1) };
    }

    T decode(code<T> c) const
    {
        size_t l = c.length;
        size_t n = c.value;

        return _symbols.read(_first_of_length.read(l-1) + n - _code_of_first.read(l-1));
    }

    template <typename BitContainer>
    T decode_next(const BitContainer& bits, size_t& i) const
    {
        size_t h = _first_of_length.size();
        size_t end = std::min(i + h, bits.size());
        T n = bits.read_int(i, end) << (h - (end - i));

        std::vector<size_t> indexes(h);
        std::iota(indexes.begin(), indexes.end(), 1);

        auto it = std::upper_bound(
            indexes.begin(), indexes.end(), n,
            [this, h](T n_in, size_t l) {
                return n_in < (_code_of_first.read(l - 1) << (h - l));
            });

        size_t l = std::distance(indexes.begin(), it); // - 1
        n = n >> (h - l);
        i += l;

        return _symbols.read(_first_of_length.read(l-1) + n - _code_of_first.read(l-1));
    }

    template <typename BitContainer>
    void skip_next(const BitContainer& bits, size_t& i) const
    {
        size_t h = _first_of_length.size();
        size_t end = std::min(i + h, bits.size());
        T n = bits.read_int(i, end) << (h - (end - i));

        std::vector<size_t> indexes(h);
        std::iota(indexes.begin(), indexes.end(), 1);

        auto it = std::upper_bound(
            indexes.begin(), indexes.end(), n,
            [this, h](T n_in, size_t l) {
                return n_in < (_code_of_first.read(l - 1) << (h - l));
            });

        i += std::distance(indexes.begin(), it);
    }

};

} /* culcompact */
