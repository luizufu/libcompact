#pragma once

#include <cstddef>
#include <stdint.h>

#if defined(_MSC_VER)
#   include <intrin.h>
#endif

namespace culcompact::detail
{

inline size_t count(uint64_t x)
{
    size_t v = 0;
    while(x != 0) {
        x &= x - 1;
        v++;
    }
    return v;
}

inline size_t pop_count(unsigned short x)
{
#if defined(__clang__) || defined(__GNUC__) || defined(__GNUG__)
    return __builtin_popcount(x);
#elif defined(__ICC) || defined(__INTEL_COMPILER)
    return _mm_popcnt_u32(x);
#elif defined(_MSC_VER)
    return __popcnt16(x);
#else
    return count(x);
#endif
}

inline size_t pop_count(unsigned int x)
{
#if defined(__clang__) || defined(__GNUC__) || defined(__GNUG__)
    return __builtin_popcount(x);
#elif defined(__ICC) || defined(__INTEL_COMPILER)
    return _mm_popcnt_u32(x);
#elif defined(_MSC_VER)
    return __popcnt(x);
#else
    return count(x);
#endif
}

inline size_t pop_count(unsigned long x)
{
#if defined(__clang__) || defined(__GNUC__) || defined(__GNUG__)
    return __builtin_popcountl(x);
#elif defined(__ICC) || defined(__INTEL_COMPILER)
    return _mm_popcnt_u64(x);
#elif defined(_MSC_VER)
    return __popcnt64(x);
#else
    return count(x);
#endif
}

inline size_t pop_count(unsigned long long x)
{
#if defined(__clang__) || defined(__GNUC__) || defined(__GNUG__)
    return __builtin_popcountll(x);
#elif defined(__ICC) || defined(__INTEL_COMPILER)
    return _mm_popcnt_u64(x);
#elif defined(_MSC_VER)
    return __popcnt64(x);
#else
    return count(x);
#endif
}

inline size_t pop_count(unsigned char x)
{
    return pop_count(static_cast<unsigned int>(x));
}


} /* culcompact */

