#pragma once

#include <cstddef>
#include <string>
#include <type_traits>
#include <iterator>

namespace culcompact::detail
{
    inline std::string make_out_of_range_msg(size_t i, size_t n)
    {
        return
            "accessing position " + std::to_string(i)
            + " from container of size " + std::to_string(n);
    }

    inline std::string make_range_error_msg(size_t from, size_t to)
    {
        return
            "range (" + std::to_string(from) + ", " + std::to_string(to) + "]"
            + " is malformed";
    }

    inline std::string make_length_error_msg(size_t l, size_t max)
    {
        return
            "length " + std::to_string(l)
            + " cannot be higher than " + std::to_string(max);
    }

    inline std::string make_too_high_parameter_msg(size_t v, size_t max)
    {
        return
            "parameter " + std::to_string(v)
            + " cannot be higher than " + std::to_string(max);
    }


    namespace detail
    {

    template<class>
    struct sfinae_true : std::true_type{};

    template<typename It>
    [[maybe_unused]] static auto test_iterator_category(int)
        -> sfinae_true<typename std::iterator_traits<It>::iterator_category>;

    template<typename>
    [[maybe_unused]] static auto test_iterator_category(long)
        -> std::false_type;

    template <typename It>
    struct has_iterator_category : decltype(detail::test_iterator_category<It>(0)){};

    } /* detail */

    template <typename It>
    constexpr bool is_iterator = true
        && detail::has_iterator_category<It>::value;

} /* culcompact */
