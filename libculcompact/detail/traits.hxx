#pragma once

namespace culcompact::detail
{

template<typename Container>
struct extract_T;

template<template<typename> class Container, typename T>
struct extract_T<Container<T>>
{
    using type = T;
};

template<template<typename> class Container, size_t N, typename T>
struct extract_T<Container<N, T>>
{
    using type = T;
};

template <typename Container>
using extract_T_t = typename extract_T<Container>::type;

} /* traits */
