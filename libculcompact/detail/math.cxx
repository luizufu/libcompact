#include <libculcompact/detail/math.hxx>

#include <vector>
#include <limits>

#ifndef COMB_TABLE_SIZE
#   define COMB_TABLE_SIZE 100
#endif


namespace culcompact::detail
{

static std::vector<std::vector<size_t>> initialize_table()
{
    return std::vector<std::vector<size_t>>(
        COMB_TABLE_SIZE,
        std::vector<size_t>(COMB_TABLE_SIZE, std::numeric_limits<size_t>::max()));
}

size_t combination(size_t n, size_t k)
{
    // TODO change to half matrix with no diagonals
    static auto table = initialize_table();

    if (n < k)
    {
        return 0;
    }
    else if(n == k || k == 0)
    {
        return 1;
    }
    else if(table[n][k] != std::numeric_limits<size_t>::max())
    {
        return table[n][k];
    }
    else if(table[n][n - k] != std::numeric_limits<size_t>::max())
    {
        return table[n][n - k];
    }

    table[n][k] = table[n][n - k] = combination(n - 1, k - 1) + combination(n - 1, k);
    return table[n][k];
}



} /* culcompact */
