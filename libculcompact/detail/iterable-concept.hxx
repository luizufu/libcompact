#pragma once

#include <type_traits>
#include <libculcompact/detail/util.hxx>

namespace culcompact::detail
{
template <typename T>
concept IntConvertible = std::is_integral_v<T>;

template <typename T>
concept BoolConvertible = std::is_convertible_v<T, bool>;

template <typename It>
concept IntIterable = is_iterator<It> && IntConvertible<typename std::iterator_traits<It>::value_type>;

template <typename It>
concept BoolIterable = true;
/* concept BoolIterable = is_iterator<It> && BoolConvertible<typename std::iterator_traits<It>::value_type>; */

}
