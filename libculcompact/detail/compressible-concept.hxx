#pragma once

#include <type_traits>
#include <libculcompact/detail/util.hxx>
#include <libculcompact/bit/rrr-bit-vector.hxx>

namespace culcompact::detail
{

template <template <typename> class BitVector>
concept Compressible = std::is_same<BitVector<int>, rrr_bit_vector<int>>::value;

template <template <typename> class BitVector>
concept NotCompressible = !std::is_same<BitVector<int>, rrr_bit_vector<int>>::value;

}


