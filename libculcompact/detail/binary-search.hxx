#pragma once

#include <cstddef>

namespace culcompact::detail
{

    template <typename Comp>
    size_t lower_bound(size_t l, size_t r, Comp comp)
    {
        size_t len = r - l;

        while(len > 0)
        {
            size_t half = len >> 1;
            size_t m = l + half;

            if(comp(m))
            {
                l = m + 1;
                len = len - half - 1;
            }
            else
            {
                len = half;
            }
        }

        return l;
    }

} /* culcompact::detail */
