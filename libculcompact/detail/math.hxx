#pragma once

#include <gcem.hpp>

#include <cmath>
#include <limits>
#include <cstddef>

namespace culcompact::detail
{

constexpr bool approximately_equal(double a, double b,
        double epsilon = std::numeric_limits<double>::epsilon())
{
    return gcem::abs(a - b) <=
        ( (gcem::abs(a) < gcem::abs(b) ?
           gcem::abs(b) :
           gcem::abs(a)) * epsilon);
}

constexpr size_t ceil(double x)
{
    size_t f = static_cast<size_t>(x);
    if(approximately_equal(x, static_cast<double>(f)))
        return f;

    return f + (x > 0 ? 1 : 0);
}

constexpr size_t floor(double x)
{
    return static_cast<size_t>(x);
}

constexpr double log2(size_t x)
{
    return gcem::log(static_cast<double>(x)) / gcem::log(2.);
}

template <typename T>
constexpr T max(T lhs, T rhs)
{
    return gcem::max(lhs, rhs);
}


constexpr size_t scomb(size_t n, size_t k)
{
    if(n < k)
    {
        return 0;
    }
    else if(n == k || k == 0)
    {
        return 1;
    }
    else
    {
        return scomb(n - 1, k - 1) + scomb(n - 1, k);
    }
}



size_t combination(size_t n, size_t k);

} /* culcompact */
