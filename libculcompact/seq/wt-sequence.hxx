#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/seq/symbol-reference.hxx>
#include <libculcompact/seq/symbol-iterator.hxx>
#include <libculcompact/aux/wavelet-tree.hxx>
#include <libculcompact/detail/iterable-concept.hxx>


namespace culcompact
{

// plain in the sense it stores every position and it is not compressed
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE,
          template <typename> class BitVector = plain_bit_vector> //use plain_bit_vector?
class wt_sequence
{
    // deque doesnt invalidate pointers, maybe I should be using another way
    wavelet_tree<T, BitVector> _tree;

    size_t _n = 0;
    size_t _o = 0;


public:

    class extension1;
    using iterator = symbol_iterator<wt_sequence<T, BitVector>>;
    using const_iterator = const_symbol_iterator<wt_sequence<T, BitVector>>;
    using reference = symbol_reference<wt_sequence<T, BitVector>>;
    using const_reference = const_symbol_reference<wt_sequence<T, BitVector>>;


    wt_sequence() = default;

    template <detail::IntIterable It>
    wt_sequence(It begin, It end, size_t o)
        : _tree(begin, end, o)
        , _n(std::distance(begin, end))
        , _o(o)
    {
    }

    template <detail::IntConvertible Int>
    wt_sequence(std::initializer_list<Int> list, size_t o)
        : wt_sequence(list.begin(), list.end(), o)
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        size_t a = 0;
        size_t b = _o - 1;
        size_t ibefore = i;


        _tree.traverse_v(
            [&](auto* n, auto visit) {
                if(a == b)
                    return;

                if(!n->bits.read_bit(i))
                {
                    i = n->bits_ext.rank0(i + 1) - 1;
                    b = (a + b) / 2;
                    visit(n->left);
                }
                else
                {
                    i = n->bits_ext.rank1(i + 1) - 1;
                    a = (a + b) / 2 + 1;
                    visit(n->right);
                }
            });

        return reference(this, a, ibefore);
    }


    // reads int at position i, this is a throwing and non-modifiable version
    const_reference read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        size_t a = 0;
        size_t b = _o - 1;
        size_t ibefore = i;

        _tree.traverse_v(
            [&](const auto* n, auto visit) {
                if(a == b)
                    return;

                if(!n->bits.read_bit(i))
                {
                    i = n->bits_ext.rank0(i + 1) - 1;
                    b = (a + b) / 2;
                    visit(n->left);
                }
                else
                {
                    i = n->bits_ext.rank1(i + 1) - 1;
                    a = (a + b) / 2 + 1;
                    visit(n->right);
                }
            });

        return const_reference(this, a, ibefore);
    }


    // returns the number of elements in the int_array
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _tree.size_of();
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }

};


template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
using cwt_sequence = wt_sequence<T, rrr_bit_vector>;


template <typename T,
          template <typename> class BitVector>
class wt_sequence<T, BitVector>::extension1
{
    const wt_sequence& _sequence;

public:

    extension1(const wt_sequence& sequence)
        : _sequence(sequence)
    {
        update();
    }

    void update()
    {
    }


    size_t rankc(size_t i, T c) const
    {
        if(i == 0)
            return 0;

        size_t a = 0;
        size_t b = _sequence._o - 1;

        _sequence._tree.traverse_v(
            [&](const auto* n, auto visit) {
                if(a == b)
                    return;

                size_t m = (a + b) / 2;
                if(c <= m)
                {
                    i = n->bits_ext.rank0(i);
                    if(i == 0)
                        return;

                    b = m;
                    visit(n->left);
                }
                else
                {
                    i = n->bits_ext.rank1(i);
                    if(i == 0)
                        return;

                    a = m + 1;
                    visit(n->right);
                }
            });

        return i;
    }

    size_t selectc(size_t j, T c) const
    {
        if(j == 0)
            return 0;

        size_t a = 0;
        size_t b = _sequence._o - 1;

        _sequence._tree.traverse_v(
            [&](const auto* n, auto visit) {
                if(a == b)
                    return;

                size_t m = (a + b) / 2;
                if(c <= m)
                {
                    b = m;
                    visit(n->left);
                    j = n->bits_ext.select0(j);
                }
                else
                {
                    a = m + 1;
                    visit(n->right);
                    j = n->bits_ext.select1(j);
                }
            });

        return j;
    }

    size_t size_of() const
    {
        return 0;
    }

};

} /* culcompact */
