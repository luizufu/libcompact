#pragma once

#include <cstddef>
#include <iterator>

namespace culcompact
{

template <typename Symbols>
class symbol_iterator
{
    Symbols* _pointer;
    size_t _i;

public:

    using iterator_category = std::random_access_iterator_tag;
    using value_type = uint64_t;
    using reference = typename Symbols::reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    symbol_iterator()
        : _pointer(nullptr)
        , _i(0)
    {
    }

    symbol_iterator(Symbols* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
    {
    }

    /* symbol_iterator(const symbol_iterator& rhs) */
    /*     : _pointer(rhs._pointer) */
    /*     , _i(rhs._i) */
    /* { */
    /* } */

    symbol_iterator& operator+=(difference_type rhs)
    {
        _i += rhs;
        return *this;
    }

    symbol_iterator& operator-=(difference_type rhs)
    {
        _i -= rhs;
        return *this;
    }

    reference operator*() const
    {
        return reference(_pointer, _pointer->operator[](_i), _i);
    }

    reference operator[](difference_type rhs) const
    {
        return reference(_pointer, _pointer->operator[](rhs), _i);
    }

    symbol_iterator& operator++()
    {
        ++_i;
        return *this;
    }

    symbol_iterator& operator--()
    {
        --_i;
        return *this;
    }

    symbol_iterator operator++(int)
    {
        symbol_iterator r(*this);
        this->operator++();
        return r;
    }

    symbol_iterator operator--(int)
    {
        symbol_iterator r(*this);
        this->operator--();
        return r;
    }

    difference_type operator-(const symbol_iterator& rhs) const
    {
        return _i - rhs._i;
    }

    symbol_iterator operator+(difference_type rhs) const
    {
        return symbol_iterator(_pointer, _i + rhs);
    }

    symbol_iterator operator-(difference_type rhs) const
    {
        return symbol_iterator(_pointer, _i - rhs);
    }

    friend symbol_iterator operator+(difference_type lhs, const symbol_iterator& rhs)
    {
        return symbol_iterator(rhs._pointer, lhs + rhs._i);
    }

    friend symbol_iterator operator-(difference_type lhs, const symbol_iterator& rhs)
    {
        return symbol_iterator(rhs._pointer, lhs - rhs._i);
    }

    bool operator==(const symbol_iterator& rhs) const
    {
        return _i == rhs._i;
    }

    bool operator!=(const symbol_iterator& rhs) const
    {
        return _i != rhs._i;
    }

    bool operator>(const symbol_iterator& rhs) const
    {
        return _i > rhs._i;
    }

    bool operator<(const symbol_iterator& rhs) const
    {
        return _i < rhs._i;
    }

    bool operator>=(const symbol_iterator& rhs) const
    {
        return _i >= rhs._i;
    }

    bool operator<=(const symbol_iterator& rhs) const
    {
        return _i <= rhs._i;
    }
};


template <typename Symbols>
class const_symbol_iterator
{
    const Symbols* _pointer;
    size_t _i;

public:

    using iterator_category = std::random_access_iterator_tag;
    using value_type = uint64_t;
    using reference = typename Symbols::const_reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    const_symbol_iterator()
        : _pointer(nullptr)
        , _i(0)
    {
    }

    const_symbol_iterator(const Symbols* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
    {
    }

    /* const_symbol_iterator(const const_symbol_iterator& rhs) */
    /*     : _pointer(rhs._pointer) */
    /*     , _i(rhs._i) */
    /* { */
    /* } */

    const_symbol_iterator& operator+=(difference_type rhs)
    {
        _i += rhs;
        return *this;
    }

    const_symbol_iterator& operator-=(difference_type rhs)
    {
        _i -= rhs;
        return *this;
    }

    reference operator*() const
    {
        return reference(_pointer, _pointer->read(_i), _i);
    }

    reference operator[](difference_type rhs) const
    {
        return reference(_pointer, _pointer->read(rhs), _i);
    }

    const_symbol_iterator& operator++()
    {
        ++_i;
        return *this;
    }

    const_symbol_iterator& operator--()
    {
        --_i;
        return *this;
    }

    const_symbol_iterator operator++(int)
    {
        const_symbol_iterator r(*this);
        this->operator++();
        return r;
    }

    const_symbol_iterator operator--(int)
    {
        const_symbol_iterator r(*this);
        this->operator--();
        return r;
    }

    difference_type operator-(const const_symbol_iterator& rhs) const
    {
        return _i - rhs._i;
    }

    const_symbol_iterator operator+(difference_type rhs) const
    {
        return const_symbol_iterator(_pointer, _i + rhs);
    }

    const_symbol_iterator operator-(difference_type rhs) const
    {
        return const_symbol_iterator(_pointer, _i - rhs);
    }

    friend const_symbol_iterator operator+(difference_type lhs, const const_symbol_iterator& rhs)
    {
        return const_symbol_iterator(rhs._pointer, lhs + rhs._i);
    }

    friend const_symbol_iterator operator-(difference_type lhs, const const_symbol_iterator& rhs)
    {
        return const_symbol_iterator(rhs._pointer, lhs - rhs._i);
    }

    bool operator==(const const_symbol_iterator& rhs) const
    {
        return _i == rhs._i;
    }

    bool operator!=(const const_symbol_iterator& rhs) const
    {
        return _i != rhs._i;
    }

    bool operator>(const const_symbol_iterator& rhs) const
    {
        return _i > rhs._i;
    }

    bool operator<(const const_symbol_iterator& rhs) const
    {
        return _i < rhs._i;
    }

    bool operator>=(const const_symbol_iterator& rhs) const
    {
        return _i >= rhs._i;
    }

    bool operator<=(const const_symbol_iterator& rhs) const
    {
        return _i <= rhs._i;
    }
};

} /* culcompact */
