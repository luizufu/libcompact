#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/seq/symbol-reference.hxx>
#include <libculcompact/seq/symbol-iterator.hxx>
#include <libculcompact/perm/plain-permutation.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/detail/math.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <array>
#include <vector>
#include <deque>


namespace culcompact
{

// plain in the sense it stores every position and it is not compressed
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class perm_sequence
{

    // within chunk
    // deque doesnt invalidate pointers, maybe I should be using another way
    std::deque<plain_bit_vector<T>> _marks;
    std::deque<plain_permutation<T>> _positions;
    std::deque<typename plain_bit_vector<T>::extension1> _marks_ext;
    std::deque<typename plain_permutation<T>::extension1> _positions_ext;

    size_t _n = 0;
    size_t _o = 0;
    size_t _y = 0;
    size_t _k = 0;


    template <detail::IntIterable It>
    std::vector<T> histogram(It begin, It end) const
    {
        std::vector<T> hist(_o, 0);
        for(auto it = begin; it != end; ++it)
            ++hist[*it];

        return hist;
    }

    std::vector <T> cumsum_starting_with_zero(const std::vector<T>& v)const
    {
        if(v.empty())
            return {};

        std::vector<T> sums(_o + 1, 0);
        for(size_t i = 0; i < v.size(); ++i)
            sums[i + 1] = sums[i] + v[i];

        return sums;
    }


public:

    class extension1;
    using iterator = symbol_iterator<perm_sequence<T>>;
    using const_iterator = const_symbol_iterator<perm_sequence<T>>;
    using reference = symbol_reference<perm_sequence<T>>;
    using const_reference = const_symbol_reference<perm_sequence<T>>;


    perm_sequence(size_t n, size_t o, size_t y = CULCOMPACT_DEFAULT_Y, size_t k = CULCOMPACT_DEFAULT_K)
        : _n(n)
        , _o(o)
        , _y(y)
        , _k(k)
    {
    }

    template <detail::IntIterable It>
    perm_sequence(It begin, It end, size_t o, size_t y = CULCOMPACT_DEFAULT_Y, size_t k = CULCOMPACT_DEFAULT_K)
        : perm_sequence(std::distance(begin, end), o, y, k)
    {
        // for every partition or _o symbols
        for(size_t k = 0; k < std::ceil(_n / (double)_o); ++k)
        {
            auto kbegin = begin + (k * _o);
            auto kend = (k + 1) * _o < _n ? begin + (k + 1) * _o : end;
            auto khist = histogram(kbegin, kend);

            auto csum = cumsum_starting_with_zero(khist);
            size_t l = std::distance(kbegin, kend);

            _marks.emplace_back(l + _o, false);
            for(size_t o = 0; o < _o; ++o)
                _marks.back().set_bit(csum[o + 1] + o);
            _marks_ext.emplace_back(_marks.back(), _k);

            std::vector<size_t> perm(l);
            size_t i = 0;
            for(auto it = kbegin; it != kend; ++it, ++i)
            {
                perm[csum[*it]] = i;
                ++csum[*it];
            }

            _positions.emplace_back(perm.begin(), perm.end());
            _positions_ext.emplace_back(_positions.back(), _y, _k);
        }
    }

    template <detail::IntConvertible Int>
    perm_sequence(std::initializer_list<Int> list, size_t o, size_t y = CULCOMPACT_DEFAULT_Y, size_t k = CULCOMPACT_DEFAULT_K)
        : perm_sequence(list.begin(), list.end(), o, y, k)
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        size_t j = _positions_ext[i / _o].inverse(i % _o);
        return reference(this, _marks_ext[i / _o].select0(j + 1) - j - 1, i);
    }


    // reads int at position i, this is a throwing and non-modifiable version
    const_reference read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        size_t j = _positions_ext[i / _o].inverse(i % _o);
        return const_reference(this, _marks_ext[i / _o].select0(j + 1) - j - 1, i);
    }

    // writes int to position i, this is a throwing and non-modifiable version
    // T_oD_o implement
    void write(size_t i, [[maybe_unused]] T integer)
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

    }


    // returns the number of elements in the int_array
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        size_t s = 0;

        for(size_t i = 0; i < std::ceil(_n / (double)_o); ++i)
        {
            s += _marks[i].size_of() + _marks_ext[i].size_of();
            s += _positions[i].size_of() + _positions_ext[i].size_of();
        }

        return s;
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }

};


template <typename T>
class perm_sequence<T>::extension1
{
    const perm_sequence& _sequence;

    // chunk level
    plain_bit_vector<T> _symbol_count;
    typename plain_bit_vector<T> ::extension1 _symbol_count_ext;

public:

    extension1(const perm_sequence& sequence)
        : _sequence(sequence)
        , _symbol_count(_sequence._n +
                (_sequence._o *
                    std::ceil(_sequence._n / (double)_sequence._o)), true)
        , _symbol_count_ext(_symbol_count, _sequence._k)
    {
        update();
    }

    void update()
    {
        auto hist = _sequence.histogram(_sequence.begin(), _sequence.end());

        size_t n = _sequence._n;
        size_t o = _sequence._o;

        std::vector<size_t> ksums(o, 0);

        size_t s = std::ceil(n / (double)o);

        // for every partition or _o symbols
        for(size_t k = 0; k < s; ++k)
        {
            auto kbegin = _sequence.begin() + (k * o);
            auto kend = (k + 1) * o < n
                ? _sequence.begin() + (k + 1) * o
                : _sequence.end();

            auto khist = _sequence.histogram(kbegin, kend);

            // compute _symbol_count by setting 0 at boundaries
            for(size_t l = 0, sums = 0; l < o; ++l)
            {
                ksums[l] += khist[l];
                size_t start = sums + (l * s);
                size_t next_zero_pos = start + ksums[l];
                _symbol_count.clear_bit(next_zero_pos);

                ++ksums[l]; // count zero
                sums += hist[l];
            }
        }

        _symbol_count_ext.update();
    }


    size_t rankc(size_t i, T c) const
    {
        if(i == 0)
            return 0;

        size_t n = _sequence._n;
        size_t o = _sequence._o;

        size_t k = (i - 1) / o;
        size_t s = std::ceil(n / (double)o);

        size_t gbegin = _symbol_count_ext.select0(c * s);
        size_t gend = _symbol_count_ext.select0((c * s) + k);
        size_t rank1 = (gend - gbegin) - k;

        size_t lbegin = _sequence._marks_ext[k].select1(c) - c;
        size_t lend = _sequence._marks_ext[k].select1(c + 1) - (c + 1);

        size_t j = lbegin;
        // T_oD_o is sequential scan faster than binary search?
        while(j < lend && _sequence._positions[k].read(j) <= (i - 1) % o)
            j++;

        size_t rank2 = j - lbegin;

        return rank1 + rank2;
    }

    size_t selectc(size_t j, T c) const
    {
        if(j == 0)
            return 0;

        size_t n = _sequence._n;
        size_t o = _sequence._o;

        size_t s = std::ceil(n / (double)o);

        size_t gbegin = _symbol_count_ext.select0(c * s);
        size_t gend = _symbol_count_ext.select0((c + 1) * s);

        size_t ones_before = gbegin - (c * s);
        size_t jpos = _symbol_count_ext.select1(ones_before + j);
        if(jpos >= gend)
            return _sequence.size() + 1;

        size_t k = jpos - gbegin - j;
        size_t select1 = k * o;

        j = jpos - _symbol_count_ext.select0(_symbol_count_ext.rank0(jpos));

        size_t lbegin = _sequence._marks_ext[k].select1(c) - c - 1;
        size_t select2 = _sequence._positions[k].read(lbegin + j) + 1;

        return select1 + select2;
    }

    size_t size_of() const
    {
        return _symbol_count.size_of() + _symbol_count_ext.size_of();
    }

};

} /* culcompact */
