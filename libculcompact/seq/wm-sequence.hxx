#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/seq/symbol-reference.hxx>
#include <libculcompact/seq/symbol-iterator.hxx>
#include <libculcompact/aux/wavelet-matrix.hxx>
#include <libculcompact/detail/iterable-concept.hxx>


namespace culcompact
{

// plain in the sense it stores every position and it is not compressed
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE,
          template <typename> class BitVector = plain_bit_vector>
class wm_sequence
{
    // deque doesnt invalidate pointers, maybe I should be using another way
    wavelet_matrix<T, BitVector> _matrix;

    size_t _n = 0;
    size_t _o = 0;


public:

    class extension1;
    using iterator = symbol_iterator<wm_sequence<T, BitVector>>;
    using const_iterator = const_symbol_iterator<wm_sequence<T, BitVector>>;
    using reference = symbol_reference<wm_sequence<T, BitVector>>;
    using const_reference = const_symbol_reference<wm_sequence<T, BitVector>>;


    wm_sequence(size_t n, size_t o)
        : _n(n)
        , _o(o)
    {
    }

    template <detail::IntIterable It>
    wm_sequence(It begin, It end, size_t o)
        : _matrix(begin, end, o)
        , _n(std::distance(begin, end))
        , _o(o)
    {
    }

    template <detail::IntConvertible Int>
    wm_sequence(std::initializer_list<Int> list, size_t o)
        : wm_sequence(list.begin(), list.end(), o)
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        size_t l = 0;
        size_t a = 0;
        size_t b = _o - 1;
        size_t ibefore = i;

        while(a != b)
        {
            // bits and bits_ext are reference_wrappers
            auto [bits, bits_ext, z] = _matrix.row(l);

            if(!bits.get()[i])
            {
                i = bits_ext.get().rank0(i + 1) - 1;
                b = (a + b) / 2;
            }
            else
            {
                i = z + bits_ext.get().rank1(i + 1) - 1;
                a = (a + b) / 2 + 1;
            }

            ++l;
        }

        return reference(this, a, ibefore);
    }


    // reads int at position i, this is a throwing and non-modifiable version
    // T_oD_o why b is not used
    const_reference read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        size_t l = 0;
        size_t a = 0;
        size_t b = _o - 1;
        size_t ibefore = i;

        while(a != b)
        {
            // bits and bits_ext are reference_wrappers
            auto [bits, bits_ext, z] = _matrix.row(l);

            if(!bits.get().read_bit(i))
            {
                i = bits_ext.get().rank0(i + 1) - 1;
                b = (a + b) / 2;
            }
            else
            {
                i = z + bits_ext.get().rank1(i + 1) - 1;
                a = (a + b) / 2 + 1;
            }

            ++l;
        }

        return const_reference(this, a, ibefore);
    }

    // writes int to position i, this is a throwing and non-modifiable version
    // T_oD_o implement
    void write(size_t i, [[maybe_unused]] T integer)
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

    }


    // returns the number of elements in the int_array
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _matrix.size_of();
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }

};


template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
using cwm_sequence = wm_sequence<T, rrr_bit_vector>;



template <typename T,
          template <typename> class BitVector>
class wm_sequence<T, BitVector>::extension1
{
    const wm_sequence& _sequence;

public:

    extension1(const wm_sequence& sequence)
        : _sequence(sequence)
    {
        update();
    }

    void update()
    {
    }


    size_t rankc(size_t i, T c) const
    {
        if(i == 0)
            return 0;

        size_t l = 0;
        size_t p = 0;
        size_t a = 0;
        size_t b = _sequence._o - 1;

        while(a != b)
        {
            // bits and bits_ext are reference_wrapper
            auto [bits, bits_ext, z] = _sequence._matrix.row(l);
            size_t m = (a + b) / 2;
            if(c <= m)
            {
                i = bits_ext.get().rank0(i);
                // T_oD_o read_bit is really necessary?
                p = bits_ext.get().rank0(p + 1) - !bits.get().read_bit(p);

                if(i == 0)
                    break;

                b = (a + b) / 2;
            }
            else
            {
                i = z + bits_ext.get().rank1(i);
                p = z + bits_ext.get().rank1(p + 1) - bits.get().read_bit(p);

                if(i == 0)
                    break;

                a = (a + b) / 2 + 1;
            }

            ++l;
        }

        return i - p;
    }

    // T_oD_o now working
    size_t selectc_impl(size_t l, size_t p, size_t a, size_t b, size_t j, size_t c) const
    {
        if(a == b)
            return p + j;

        auto [bits, bits_ext, z] = _sequence._matrix.row(l);
        size_t m = (a + b) / 2;
        if(c <= m)
        {
            p = bits_ext.get().rank0(p + 1) - !bits.get().read_bit(p);
            j = selectc_impl(l + 1, p, a, m, j, c);
            return bits_ext.get().select0(j);
        }
        else
        {
            p = z + bits_ext.get().rank1(p + 1) - bits.get().read_bit(p);
            j = selectc_impl(l + 1, p, m + 1, b, j, c);
            return bits_ext.get().select1(j - z);
        }
    }

    size_t selectc(size_t j, T c) const
    {
        if(j == 0)
            return 0;

        return selectc_impl(0, 0, 0, _sequence._o - 1, j, c);
    }

    size_t size_of() const
    {
        return 0;
    }

};

} /* culcompact */
