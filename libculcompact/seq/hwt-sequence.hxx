#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/seq/symbol-reference.hxx>
#include <libculcompact/seq/symbol-iterator.hxx>
#include <libculcompact/aux/hwavelet-tree.hxx>
#include <libculcompact/detail/iterable-concept.hxx>


namespace culcompact
{

// plain in the sense it stores every position and it is not compressed
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE,
          template <typename> class BitVector = plain_bit_vector> //use plain_bit_vector?
class hwt_sequence
{
    // deque doesnt invalidate pointers, maybe I should be using another way
    hwavelet_tree<T, BitVector> _tree;

    size_t _n = 0;
    size_t _o = 0;


public:

    class extension1;
    using iterator = symbol_iterator<hwt_sequence<T, BitVector>>;
    using const_iterator = const_symbol_iterator<hwt_sequence<T, BitVector>>;
    using reference = symbol_reference<hwt_sequence<T, BitVector>>;
    using const_reference = const_symbol_reference<hwt_sequence<T, BitVector>>;


    hwt_sequence(size_t n, size_t o)
        : _n(n)
        , _o(o)
    {
    }

    template <detail::IntIterable It>
    hwt_sequence(It begin, It end, size_t o)
        : _tree(begin, end, _o)
        , _n(std::distance(begin, end))
        , _o(o)
    {
    }

    template <detail::IntConvertible Int>
    hwt_sequence(std::initializer_list<Int> list, size_t o)
        : hwt_sequence(list.begin(), list.end())
    {
    }

    // reads int at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        size_t ibefore = i;

        size_t v = _tree.traverse_r(
            [&](auto* n, auto visit) {
                if(n->is_leaf())
                    return static_cast<size_t>(n->symbol);

                if(!n->symbol.bitvector.read_bit(i))
                {
                    i = n->symbol.bitvector_ext.rank0(i + 1) - 1;
                    return visit(n->left);
                }
                else
                {
                    i = n->symbol.bitvector_ext.rank1(i + 1) - 1;
                    return visit(n->right);
                }
            });

        return reference(this, v, ibefore);
    }


    // reads int at position i, this is a throwing and non-modifiable version
    const_reference read(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        size_t ibefore = i;

        size_t v = _tree.traverse_r(
            [&](const auto* n, auto visit) {
                if(n->is_leaf())
                    return static_cast<size_t>(n->symbol);

                if(!n->symbol.bitvector.read_bit(i))
                {
                    i = n->symbol.bitvector_ext.rank0(i + 1) - 1;
                    return visit(n->left);
                }
                else
                {
                    i = n->symbol.bitvector_ext.rank1(i + 1) - 1;
                    return visit(n->right);
                }
            });

        return const_reference(this, v, ibefore);
    }

    // writes int to position i, this is a throwing and non-modifiable version
    // T_oD_o implement
    void write(size_t i, [[maybe_unused]] T integer)
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

    }


    // returns the number of elements in the int_array
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _tree.size_of();
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }

};


template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
using chwt_sequence = hwt_sequence<T, rrr_bit_vector>;



template <typename T,
          template <typename> class BitVector>
class hwt_sequence<T, BitVector>::extension1
{
    const hwt_sequence& _sequence;

public:

    extension1(const hwt_sequence& sequence)
        : _sequence(sequence)
    {
        update();
    }

    void update()
    {
    }


    size_t rankc(size_t i, T c) const
    {
        if(i == 0)
            return 0;

        _sequence._tree.traverse_from_v(
            typename hwavelet_tree<T>::bitvector_ext_t(c),
            [&](const auto* n, auto visit) {
                if(n->is_root())
                    return;

                visit(n->parent);

                if(i == 0)
                    return;

                if(n == n->parent->left.get())
                    i = n->parent->symbol.bitvector_ext.rank0(i);
                else
                    i = n->parent->symbol.bitvector_ext.rank1(i);
            });

        return i;
    }

    size_t selectc(size_t j, T c) const
    {
        if(j == 0)
            return 0;

        _sequence._tree.traverse_from_v(
            typename hwavelet_tree<T>::bitvector_ext_t(c),
            [&](const auto* n, auto visit) {
                if(n->is_root())
                    return;

                if(n == n->parent->left.get())
                    j = n->parent->symbol.bitvector_ext.select0(j);
                else
                    j = n->parent->symbol.bitvector_ext.select1(j);

                visit(n->parent);
            });

        return j;
    }

    size_t size_of() const
    {
        return 0;
    }

};

} /* culcompact */
