#pragma once

#include <cstddef>
#include <cstdint>

namespace culcompact
{

template <typename Symbols>
class symbol_reference
{
    Symbols* _pointer;
    uint64_t _symbol;
    size_t _i;

public:

    symbol_reference(Symbols* pointer, uint64_t symbol, size_t i)
        : _pointer(pointer)
        , _symbol(symbol)
        , _i(i)
    {
    }

    operator uint64_t() const
    {
        return _symbol;
    }

    symbol_reference& operator=(uint64_t symbol)
    {
        _symbol = symbol;
        _pointer->write(_i, _symbol);

        return *this;
    }

    symbol_reference& operator++()
    {
        return operator=(_symbol + 1);
    }

    symbol_reference& operator--()
    {
        return operator=(_symbol - 1);
    }

    symbol_reference operator++(int)
    {
        symbol_reference tmp(*this);
        operator++();
        return tmp;
    }

    symbol_reference operator--(int)
    {
        symbol_reference tmp(*this);
        operator--();
        return tmp;
    }

    symbol_reference& operator+=(uint64_t rhs)
    {
        return operator=(_symbol + rhs);
    }

    symbol_reference& operator-=(uint64_t rhs)
    {
        return operator=(_symbol - rhs);
    }

    symbol_reference& operator*=(uint64_t rhs)
    {
        return operator=(_symbol * rhs);
    }

    symbol_reference& operator/=(uint64_t rhs)
    {
        return operator=(_symbol / rhs);
    }
};


// a proxy reference that enable only access to a bit
template <typename Symbols>
class const_symbol_reference
{
    const Symbols* _pointer;
    uint64_t _symbol;
    size_t _i;

public:


    const_symbol_reference(const Symbols* pointer, uint64_t symbol, size_t i)
        : _pointer(pointer)
        , _symbol(symbol)
        , _i(i)
    {
    }

    operator uint64_t() const
    {
        return _symbol;
    }

    const_symbol_reference& operator=(const_symbol_reference other) = delete;
};

} /* culcompact */
