#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/aux/word.hxx>
#include <libculcompact/bit/sparse-bit-vector.hxx>
#include <libculcompact/bit/bit-reference.hxx>
#include <libculcompact/bit/bit-iterator.hxx>
#include <libculcompact/detail/math.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <array>
#include <vector>
#include <cmath>
#include <type_traits>
#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace culcompact
{

// stores a static sequence of bits in run format, zeros and ones are split in two bit vectors coded in unary to find run sizes
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class run_bit_vector
{
    template <typename>
    friend class bit_iterator;
    template <typename>
    friend class const_bit_iterator;

    sparse_bit_vector<T> _zeros;
    sparse_bit_vector<T> _ones;
    typename sparse_bit_vector<T>::extension1 _zeros_ext;
    typename sparse_bit_vector<T>::extension1 _ones_ext;

    bool _start_with_zero;

    // returns the number of bits set from position 0 to i,
    // it uses a binary search strategy
    size_t prank1(size_t i) const
    {
        size_t j = std::max(
            _ones_ext.rank1(_ones.size()), _zeros_ext.rank1(_zeros.size()));

        std::vector<size_t> indexes(j);
        std::iota(indexes.begin(), indexes.end(), 0);

        auto it = std::lower_bound(
            indexes.begin(), indexes.end(), i,
            [this](size_t k, size_t i_in) {
                return
                    _zeros_ext.select1(k + 1) + _ones_ext.select1(k + 1)
                    <= i_in;
            });

        size_t k =  std::distance(indexes.begin(), it);

        if(_start_with_zero)
        {
            size_t select1ok = _ones_ext.select1(k);
            if(i < _zeros_ext.select1(k + 1) + select1ok)
                return select1ok;
            else
                return i - _zeros_ext.select1(k + 1) + 1;
        }
        else
        {
            size_t select1ok = _ones_ext.select1(k + 1);
            if(i < _zeros_ext.select1(k) + select1ok)
                return i - _zeros_ext.select1(k) + 1;
            else
                return select1ok;
        }
    }


    // select the position of the j-th one, the fist zero has j = 1
    // uses a binary search on rank operation
    size_t pselect1(size_t j) const
    {
        if(j == 0 || j > count())
            return size();

        if(_start_with_zero)
            return j + _zeros_ext.select1(_ones_ext.rank1(j - 1) + 1) - 1;
        else
            return j + _zeros_ext.select1(_ones_ext.rank1(j - 1)) - 1;
    }

    // uses a binary search to retrieve number j, such that:
    // select1(Z, j) + select1(O, j) <= j
    size_t lower_run(size_t i) const
    {
        size_t j = std::max(
            _ones_ext.rank1(_ones.size()), _zeros_ext.rank1(_zeros.size()));

        std::vector<size_t> indexes(j);
        std::iota(indexes.begin(), indexes.end(), 0);

        auto it = std::lower_bound(
            indexes.begin(), indexes.end(), i,
            [this](size_t k, size_t i_in) {
                return
                    _zeros_ext.select1(k + 1) + _ones_ext.select1(k + 1)
                    <= i_in;
            });

        return std::distance(indexes.begin(), it);
    }


public:

    class extension1;
    using iterator = bit_iterator<run_bit_vector<T>>;
    using const_iterator = const_bit_iterator<run_bit_vector<T>>;
    using reference = bit_reference<run_bit_vector<T>>;
    using const_reference = const_bit_reference<run_bit_vector<T>>;

    run_bit_vector(size_t K = CULCOMPACT_DEFAULT_K)
        : _zeros(0, K)
        , _ones(0, K)
        , _zeros_ext(_zeros)
        , _ones_ext(_ones)
        , _start_with_zero(true)
    {
    }

    // constructs a sequence of bits from a pair of iterators, where elements are castable to bool
    template <detail::BoolIterable It>
    run_bit_vector(It begin, It end, size_t K = CULCOMPACT_DEFAULT_K)
        : run_bit_vector(K)
    {
        _start_with_zero = !static_cast<bool>(*begin);

        size_t zero_run_size = 0;
        size_t one_run_size = 0;
        size_t i = 0;
        for(It it = begin; it != end; ++it, ++i)
        {
            if(static_cast<bool>(*it))
            {
                if(zero_run_size > 0)
                {
                    _zeros.push_back(true);
                    _zeros.resize(_zeros.size() + zero_run_size - 1);
                    zero_run_size = 0;
                }

                ++one_run_size;
            }
            else
            {
                if(one_run_size > 0)
                {
                    _ones.push_back(true);
                    _ones.resize(_ones.size() + one_run_size - 1);
                    one_run_size = 0;
                }

                ++zero_run_size;
            }
        }

        if(zero_run_size > 0)
        {
            _zeros.push_back(true);
            _zeros.resize(_zeros.size() + zero_run_size - 1);
        }
        else if(one_run_size > 0)
        {
            _ones.push_back(true);
            _ones.resize(_ones.size() + one_run_size - 1);
        }

        _zeros_ext.update();
        _ones_ext.update();
    }

    // constructs a sequence of bits from an initializer_list with elements castable to bool
    template <detail::BoolConvertible Bool>
    run_bit_vector(std::initializer_list<Bool> list)
        : run_bit_vector(list.begin(), list.end())
    {
    }

    // reads bit at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        size_t k =  lower_run(i);

        if(_start_with_zero)
            return reference(this,
                !(i < _zeros_ext.select1(k + 1) + _ones_ext.select1(k)), i);
        else
            return reference(this,
                i < _zeros_ext.select1(k) + _ones_ext.select1(k + 1), i);
    }


    // reads bit at position i, this is a throwing and non-modifiable version
    const_reference read_bit(size_t i) const
    {
        size_t k =  lower_run(i);

        if(_start_with_zero)
            return const_reference(this,
                !(i < _zeros_ext.select1(k + 1) + _ones_ext.select1(k)), i);
        else
            return const_reference(this,
                i < _zeros_ext.select1(k) + _ones_ext.select1(k + 1), i);
    }


    // reads as an integer the bits in range (from, to]
    //
    // TODO implementation...
    T read_int(size_t from, size_t to) const
    {
        if(from == to)
            return 0;

        return 0;
    }

    // returns the number of elements in the bit_vector
    size_t size() const
    {
        return _zeros.size() + _ones.size();
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return 0
            + _zeros.size_of()
            + _ones.size_of()
            + _zeros_ext.size_of()
            + _ones.size_of()
            + sizeof(_start_with_zero);
    }

    // returns the popcount
    size_t count() const
    {
        return _ones.size();
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


// implements the binary search strategy for rank and the constant strategy for select
template <typename T>
class run_bit_vector<T>::extension1
{
    run_bit_vector& _bit_array;

public:

    extension1(run_bit_vector& bit_array)
        : _bit_array(bit_array)
    {
        update();
    }

    // reconstruct structures so that rank/select operations remain consistent
    void update()
    {
        /* _bit_array._zeros_ext.update(); */
        /* _bit_array._ones_ext.update(); */
    }

    // returns the number of bits cleared from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t rank0(size_t i) const
    {
        return i - rank1(i) + 1;
    }

    // returns the number of bits set from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t rank1(size_t i) const
    {
        size_t k = _bit_array.lower_run(i);

        if(_bit_array._start_with_zero)
        {
            size_t select1ok = _bit_array._ones_ext.select1(k);
            if(i < _bit_array._zeros_ext.select1(k + 1) + select1ok)
                return select1ok;
            else
                return i - _bit_array._zeros_ext.select1(k + 1) + 1;
        }
        else
        {
            size_t select1ok = _bit_array._ones_ext.select1(k + 1);
            if(i < _bit_array._zeros_ext.select1(k) + select1ok)
                return i - _bit_array._zeros_ext.select1(k) + 1;
            else
                return select1ok;
        }
    }

    // select the position of the j-th zero, the fist zero has j = 1
    // uses a binary search on rank operation
    size_t select0(size_t j) const
    {
        if(j == 0 || j > (_bit_array.size() - _bit_array.count()))
            return _bit_array.size();

        if(_bit_array._start_with_zero)
            return j + _bit_array._ones_ext.select1(_bit_array._zeros_ext.rank1(j - 1)) - 1;
        else
            return j + _bit_array._ones_ext.select1(_bit_array._zeros_ext.rank1(j - 1) + 1) - 1;
    }

    // select the position of the j-th one, the fist zero has j = 1
    // uses a binary search on rank operation
    size_t select1(size_t j) const
    {
        return _bit_array.pselect1(j);
    }
};


// iterates through run_bit_vector maintaining the current run of zeros or ones, allows bit modification while iterating
template <typename T>
class bit_iterator<run_bit_vector<T>>
{
    run_bit_vector<T>* _pointer;
    size_t _i;

    T _value;
    size_t _run_size;
    size_t _zero_run;
    size_t _one_run;
    size_t _zero_count;
    size_t _one_count;

public:

    using iterator_category = std::forward_iterator_tag;
    using value_type = bool;
    using reference = typename run_bit_vector<T>::reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;

    bit_iterator()
        : _pointer(nullptr)
        , _i(0)
        , _value(false)
        , _run_size(0)
        , _zero_run(0)
        , _one_run(0)
        , _zero_count(0)
        , _one_count(0)
    {
    }

    bit_iterator(run_bit_vector<T>* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
        , _value(!(pointer->_start_with_zero))
        , _run_size((_value) ?
                    // use lower instead of select
            pointer->_ones_ext.select1(2) : // shold be based on i
            pointer->_zeros_ext.select1(2))
        , _zero_run((_value) ? 1 : 2)
        , _one_run((_value) ? 2 : 1)
        , _zero_count(0)
        , _one_count(0)
    {
    }

    reference operator*() const
    {
        return reference(_pointer, _value, _i);
    }

    bit_iterator& operator++()
    {
        ++_i;

        if(_value)
            ++_one_count;
        else
            ++_zero_count;

        if(--_run_size == 0)
        {
            _value = !_value;
            _run_size = (_value) ?
                _pointer->_ones_ext.select1(++_one_run) - _one_count :
                _pointer->_zeros_ext.select1(++_zero_run) - _zero_count;
        }

        return *this;
    }

    bit_iterator operator++(int)
    {
        bit_iterator r(*this);
        this->operator++();
        return r;
    }

    difference_type operator-(const bit_iterator& r) const
    {
        return _i - r._i;
    }

    bool operator!=(const bit_iterator& r) const
    {
        return _i != r._i;
    }

    bool operator==(const bit_iterator& r) const
    {
        return _i == r._i;
    }
};


// iterates through run_bit_vector maintaining the current run of zeros or ones, do not allow bit modification while iterating
template <typename T>
class const_bit_iterator<run_bit_vector<T>>
{
    const run_bit_vector<T>* _pointer;
    size_t _i;

    T _value;
    size_t _run_size;
    size_t _zero_run;
    size_t _one_run;
    size_t _zero_count;
    size_t _one_count;

public:

    using iterator_category = std::forward_iterator_tag;
    using value_type = bool;
    using reference = typename run_bit_vector<T>::const_reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;

    const_bit_iterator()
        : _pointer(nullptr)
        , _i(0)
        , _value(false)
        , _run_size(0)
        , _zero_run(0)
        , _one_run(0)
        , _zero_count(0)
        , _one_count(0)
    {
    }

    const_bit_iterator(const run_bit_vector<T>* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
        , _value(!(pointer->_start_with_zero))
        , _run_size((_value) ?
                    // use lower instead of select
            pointer->_ones_ext.select1(2) : // shold be based on i
            pointer->_zeros_ext.select1(2))
        , _zero_run((_value) ? 1 : 2)
        , _one_run((_value) ? 2 : 1)
        , _zero_count(0)
        , _one_count(0)
    {
    }

    reference operator*() const
    {
        return reference(_pointer, _value, _i);
    }

    const_bit_iterator& operator++()
    {
        ++_i;

        if(_value)
            ++_one_count;
        else
            ++_zero_count;

        if(--_run_size == 0)
        {
            _value = !_value;
            _run_size = (_value) ?
                _pointer->_ones_ext.select1(++_one_run) - _one_count :
                _pointer->_zeros_ext.select1(++_zero_run) - _zero_count;
        }

        return *this;
    }

    const_bit_iterator operator++(int)
    {
        const_bit_iterator r(*this);
        this->operator++();
        return r;
    }

    difference_type operator-(const const_bit_iterator& r) const
    {
        return _i - r._i;
    }

    bool operator!=(const const_bit_iterator& r) const
    {
        return _i != r._i;
    }

    bool operator==(const const_bit_iterator& r) const
    {
        return _i == r._i;
    }
};

} /* culcompact */
