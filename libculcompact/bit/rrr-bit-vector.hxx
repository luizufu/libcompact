#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/bit/bit-reference.hxx>
#include <libculcompact/bit/bit-iterator.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/code/block-coder.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <vector>
#include <type_traits>
#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace culcompact
{

// stores a modifying sequence of bits in a block format, each block is represented implicitly by an class C, and an id of length L[C], pointers are used to quickly locate the starting id position of a block
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class rrr_bit_vector
{
    template <typename>
    friend class bit_iterator;
    template <typename>
    friend class const_bit_iterator;

    std::vector<size_t> _classes;
    plain_bit_vector<T> _ids;
    std::vector<size_t> _id_lengths;
    std::vector<size_t> _pointers;

    block_coder<T> _coder;

    size_t _n = 0;
    size_t _b = 0;
    size_t _k = 0;

    // retrieves the i-th block as a plain_bit_vector of size _b
    plain_bit_vector<T> retrieve_block(size_t i) const
    {
        // find the starting id position of block i
        size_t start = _pointers[i / _k];
        size_t current = _k * (i / _k);
        for(; current < i; ++current)
            start += _id_lengths[_classes[current]];

        // extract class c and id o to decode block
        size_t c = _classes[current];
        T o = _id_lengths[c] == 0
            ?  0
            : _ids.read_int(start, start + _id_lengths[c]);

        block_code<T> code = { c, o };
        return _coder.decode(code);
    }

    // replaces the last block making necessary changes
    void reconstruct_last(const plain_bit_vector<T>& block)
    {
        auto code = _coder.encode(block);

        size_t start = _ids.size() - _id_lengths[_classes.back()];
        _ids.resize(start + _id_lengths[code.c]);

        _classes.back() = code.c;
        if(_id_lengths[code.c] > 0)
            _ids.write_int(start, _ids.size(), code.o);
    }


public:

    class extension1;
    using iterator = bit_iterator<rrr_bit_vector<T>>;
    using const_iterator = const_bit_iterator<rrr_bit_vector<T>>;
    using reference = bit_reference<rrr_bit_vector<T>>;
    using const_reference = const_bit_reference<rrr_bit_vector<T>>;



    // constructs a sequence with n bits of value b by using blocks os size b and creating a pointer to every k blocks
    rrr_bit_vector(size_t n, bool bit = false, size_t b = CULCOMPACT_DEFAULT_B, size_t k = CULCOMPACT_DEFAULT_K)
        : _classes(std::ceil(n / static_cast<double>(b)), bit ? b : 0)
        , _ids(0)
        , _id_lengths(b + 1)
        , _pointers(std::ceil(n / static_cast<double>(b * k)))
        , _coder(b)
        , _n(n)
        , _b(b)
        , _k(k)
    {
        // precompute id lengths
        for(size_t c = 0; c < _b + 1; ++c)
            _id_lengths[c] = std::ceil(
                    std::log2(static_cast<double>(detail::combination(_b, c))));
    }

    // constructs a sequence of bits from a pair of iterators, where elements are castable to bool, by using blocks os size b and creating a pointer to every k blocks
    template <detail::BoolIterable It>
    rrr_bit_vector(It begin, It end, size_t b = CULCOMPACT_DEFAULT_B, size_t k = CULCOMPACT_DEFAULT_K)
        : rrr_bit_vector(std::distance(begin, end), false, b, k)
    {
        plain_bit_vector<T> block(begin, end);
        if(_n % b != 0)
            block.resize(block.size() + b - (_n % b));
        size_t i = 0, bi = 0, pi = 0;

        while(i < block.size())
        {
            if((i / _b) % _k == 0)
                _pointers[pi++] = _ids.size();

            auto code = _coder.encode_next(block, i);

            _classes[bi++] = code.c;

            if(_id_lengths[code.c] > 0)
            {
                size_t next_pos = _ids.size();
                _ids.resize(_ids.size() + _id_lengths[code.c]);
                _ids.write_int(
                    next_pos, next_pos + _id_lengths[code.c], code.o);
            }
        }
    }

    // constructs a sequence of bits from an initializer_list, where elements ore castable to bool, by using blocks os size b and creating a pointer to every k blocks
    template <detail::BoolConvertible Bool>
    rrr_bit_vector(std::initializer_list<Bool> list, size_t b = CULCOMPACT_DEFAULT_B, size_t k = CULCOMPACT_DEFAULT_K)
        : rrr_bit_vector(list.begin(), list.end(), b, k)
    {
    }

    // reads bit at position i, this is a non-throwing and modifiable version
    reference operator[](size_t i)
    {
        return reference(this, retrieve_block(i / _b).read_bit(i % _b), i);
    }


    // reads bit at position i, this is a throwing and non-modifiable version
    const_reference read_bit(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        return const_reference(this, retrieve_block(i/_b).read_bit(i % _b), i);
    }


    // reads as an integer the bits in range (from, to]
    uint64_t read_int(size_t from, size_t to) const
    {
        if(from >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(from, size()));

        if(to > size())
            throw std::out_of_range(detail::make_out_of_range_msg(to, size()));

        if(from > to)
            throw std::range_error(detail::make_range_error_msg(from, to));

        if(from == to)
            return 0;

        // if interval is within the same block just read the corresponding bits
        if(from / _b == (to - 1) / _b)
        {
            return retrieve_block(from / _b).read_int(
                from % _b,
                ((to - 1) % _b) + 1);
        }
        // if not, extract bits iteratively adding to x
        else
        {
            uint64_t shift = (to - from) - (_b - (from % _b));

            // read the ending positions of fist block
            uint64_t x = retrieve_block(from / _b).read_int(from % _b, _b) << shift;

            // if there are blocks in the middle then read them
            for(size_t b = from / _b + 1; b < (to - 1) / _b; ++b)
            {
                shift -= _b;
                x |= retrieve_block(b).read_int(0, _b) << shift;
            }

            // read the starting positions of last block
            x |= retrieve_block((to - 1) / _b).read_int(0, ((to - 1) % _b) + 1);

            return x;
        }
    }

    // inserts a bit with value b at the end
    void push_back(bool b)
    {
        // if the last element was the _b-th bit of the block,
        // then append another block and make necessary changes
        if(_n % _b == 0)
        {
            if((_n / _b) % _k == 0)
                _pointers.push_back(_ids.size());

            _classes.push_back(0);

            if(b)
            {
                plain_bit_vector<T> block(_b);
                block.set_bit(0);
                reconstruct_last(block);
            }
        }
        // if not, just update the last block
        else
        {
            auto block = retrieve_block(_n / _b);

            if(b)
                block.set_bit(_n % _b);
            else
                block.clear_bit(_n % _b);

            reconstruct_last(block);
        }

        ++_n;
    }

    // removes the last bit
    void pop_back()
    {
        --_n;

        // if the last element was the 0-th bit of the last block,
        // then remove the last block and make necessary changes
        if(_n % _b == 0)
        {
            _ids.resize(_ids.size() - _id_lengths[_classes.back()]);
            _classes.pop_back();

            if((_n / _b) % _k == 0)
                _pointers.pop_back();
        }
        // if not, just update the last block
        else
        {
            auto block = retrieve_block(_n / _b);
            block.clear_bit(_n % _b);
            reconstruct_last(block);
        }
    }

    // resizes the struture in order to accomodate n bits,
    // if n > size() then the value of b is used to fill new positions
    void resize(size_t n, bool b = false)
    {
        if(n == 0)
        {
            _ids.resize(0);
            _classes.clear();
            _pointers.clear();
        }
        else if(n > _n)
        {

            // fill the last block if it has more space
            if(_n % _b != 0)
            {
                uint64_t value = b ? (1ull << (_b - (_n % _b))) - 1 : 0;

                auto block = retrieve_block(_n / _b);
                block.write_int(_n % _b, _b, value);
                reconstruct_last(block);
            }

            // if needs to create more blocks, update accordingly
            // _ids do not need be updated since L[0] = L[_b] = 0
            if(n / _b >= _classes.size())
            {
                _classes.resize(
                        std::ceil(n / static_cast<double>(_b)),
                        b ? _b : 0);

                size_t old_psize = _pointers.size();
                size_t new_psize = std::ceil(n / static_cast<double>(_b * _k));
                if(new_psize > old_psize)
                {
                    _pointers.resize(new_psize);
                    for(size_t i = old_psize; i < new_psize; ++i)
                        _pointers[i] = _ids.size();
                }
            }
        }
        else if(n < _n)
        {
            size_t p = n / (_b * _k);
            size_t len = _pointers[p];
            for(size_t i = p * _k; i <  ((n - 1) / _b) + 1; ++i)
                len += _id_lengths[_classes[i]];

            _ids.resize(len);
            _classes.resize(std::ceil(n / static_cast<double>(_b)));
            _pointers.resize(std::ceil(n / static_cast<double>(_b * _k)));
        }

        _n = n;
    }


    // preallocates memory to accomodate n bits if necessary
    void reserve(size_t n)
    {
        _classes.reserve(std::ceil(n / static_cast<double>(_b)));
        _pointers.reserve(std::ceil(n / static_cast<double>(_b * _k)));

        if(n > _n)
        {
            size_t blocks = (n - _n) / _b;
            _ids.reserve(_ids.size() + (blocks * _id_lengths[_b / 2]));
        }
    }

    // returns the number of elements
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return 0
            + _classes.size() * sizeof(size_t)
            + _ids.size_of()
            + _id_lengths.size() * sizeof(size_t)
            + _pointers.size() * sizeof(size_t)
            + sizeof(_n)
            + sizeof(_b)
            + sizeof(_k);
    }

    // returns the popcount
    size_t count() const
    {
        size_t sum = 0;
        for(auto c : _classes)
            sum += c;

        return sum;
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


// implements the sampled pointers strategy for rank and the binary search strategy for select
template <typename T>
class rrr_bit_vector<T>::extension1
{
    const rrr_bit_vector& _bit_vector;
    std::vector<size_t> _ranks;
    size_t _k;

    // returns the popcount of the bit_vector inside (from, ..., to]
    size_t rank1_slow(size_t from, size_t to) const
    {
        size_t sum = 0;

        // sum popcount of whole blocks
        size_t w = from / _bit_vector._b;
        for(; w < (to - 1) / _bit_vector._b; ++w)
            sum += _bit_vector._classes[w];

        // sum popcount of bits until last position
        // TODO should I implement count(size_t, size_t) ?
        auto block = _bit_vector.retrieve_block(w);
        for(size_t i = 0; i < ((to - 1) % _bit_vector._b) + 1; ++i)
            sum += static_cast<size_t>(block.read_bit(i));

        return sum;
    }

public:

    extension1(const rrr_bit_vector& bit_vector, size_t k = CULCOMPACT_DEFAULT_K)
        : _bit_vector(bit_vector)
        , _k(k)
    {
        update();
    }

    // reconstruct structures so that rank/select operations remain consistent
    void update()
    {
        _ranks.resize(std::ceil(
                    _bit_vector.size() /
                    static_cast<double>(_bit_vector._b * _k)));

        size_t total_rank = 0;
        for(size_t i = 0; i < _bit_vector._classes.size(); ++i)
        {
            if(i % _k == 0)
                _ranks[i / _k] = total_rank;

            total_rank += _bit_vector._classes[i];
        }
    }

    // returns the number of bits cleared from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t rank0(size_t i) const
    {
        return i - rank1(i);
    }

    // returns the number of bits cleared from position 1 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t rank1(size_t i) const
    {
        if(i == 0)
            return 0;

        size_t p = (i - 1) / (_bit_vector._b * _k) ;

        // sum cumulative rank of previous words and the popcount of the nexts
        return _ranks[p] + rank1_slow(p * (_bit_vector._b * _k), i);
    }

    // select the position of the j-th zero, the fist zero has j = 1
    // uses a binary search on rank operation
    size_t select0(size_t j) const
    {
        size_t n = _bit_vector.size();

        return detail::lower_bound(0, n + 1,
            [j, this](size_t i) {
                return rank0(i) < j;
            });
    }

    // select the position of the j-th one, the fist one has j = 1
    // uses a binary search on rank operation
    size_t select1(size_t j) const
    {
        size_t n = _bit_vector.size();

        return detail::lower_bound(0, n + 1,
            [j, this](size_t i) {
                return rank1(i) < j;
            });
    }
};


// iterates through rrr_bit_vector maintaining the current block in memory,
// allows bit modification while iterating
template <typename T>
class bit_iterator<rrr_bit_vector<T>>
{
    rrr_bit_vector<T>* _pointer;
    size_t _i;
    plain_bit_vector<T> _current_block;

    plain_bit_vector<T> get_block(size_t i) const
    {
        if(i < _pointer->size())
            return _pointer->retrieve_block(i / _pointer->_b);
        else
            return plain_bit_vector<T>(0);
    }

public:

    using iterator_category = std::forward_iterator_tag;
    using value_type = bool;
    using reference = typename rrr_bit_vector<T>::reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    bit_iterator()
        : _pointer(nullptr)
        , _i(0)
        , _current_block(0)
    {
    }

    bit_iterator(rrr_bit_vector<T>* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
        , _current_block(get_block(i))
    {
    }

    reference operator*()
    {
        return reference(_pointer,
            _current_block.read_bit(_i % _pointer->_b), _i);
    }

    bit_iterator& operator++()
    {
        ++_i;

        if(_i % _pointer->_b == 0)
            _current_block = get_block(_i);

        return *this;
    }

    bit_iterator operator++(int)
    {
        bit_iterator r(*this);
        this->operator++();
        return r;
    }

    difference_type operator-(const bit_iterator& r) const
    {
        return _i - r._i;
    }

    bool operator!=(const bit_iterator& r) const
    {
        return _i != r._i;
    }

    bool operator==(const bit_iterator& r) const
    {
        return _i == r._i;
    }
};

// iterates through rrr_bit_vector maintaining the current block in memory,
// do not allow bit modification while iterating
template <typename T>
class const_bit_iterator<rrr_bit_vector<T>>
{
    const rrr_bit_vector<T>* _pointer;
    size_t _i;
    plain_bit_vector<T> _current_block;

    plain_bit_vector<T> get_block(size_t i) const
    {
        if(i < _pointer->size())
            return _pointer->retrieve_block(i / _pointer->_b);
        else
            return plain_bit_vector<T>(0);
    }

public:

    using iterator_category = std::forward_iterator_tag;
    using value_type = bool;
    using reference = typename rrr_bit_vector<T>::const_reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    const_bit_iterator()
        : _pointer(nullptr)
        , _i(0)
        , _current_block(0)
    {
    }

    const_bit_iterator(const rrr_bit_vector<T>* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
        , _current_block(get_block(i))
    {
    }

    reference operator*() const
    {
        return reference(_pointer,
            _current_block.read_bit(_i % _pointer->_b), _i);
    }

    const_bit_iterator& operator++()
    {
        ++_i;

        if(_i % _pointer->_b == 0)
            _current_block = get_block(_i);

        return *this;
    }

    const_bit_iterator operator++(int)
    {
        const_bit_iterator r(*this);
        this->operator++();
        return r;
    }

    difference_type operator-(const const_bit_iterator& r) const
    {
        return _i - r._i;
    }

    bool operator!=(const const_bit_iterator& r) const
    {
        return _i != r._i;
    }

    bool operator==(const const_bit_iterator& r) const
    {
        return _i == r._i;
    }
};

} /* culcompact */
