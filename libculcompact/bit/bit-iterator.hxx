#pragma once

#include <cstddef>
#include <iterator>

namespace culcompact
{

template <typename Bits>
class bit_iterator
{
    Bits* _pointer;
    size_t _i;

public:

    using iterator_category = std::random_access_iterator_tag;
    using value_type = bool;
    using reference = typename Bits::reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    bit_iterator()
        : _pointer(nullptr)
        , _i(0)
    {
    }

    bit_iterator(Bits* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
    {
    }

    /* bit_iterator(const bit_iterator& rhs) */
    /*     : _pointer(rhs._pointer) */
    /*     , _i(rhs._i) */
    /* { */
    /* } */

    bit_iterator& operator+=(difference_type rhs)
    {
        _i += rhs;
        return *this;
    }

    bit_iterator& operator-=(difference_type rhs)
    {
        _i -= rhs;
        return *this;
    }

    reference operator*() const
    {
        return reference(_pointer, _pointer->operator[](_i), _i);
    }

    reference operator[](difference_type rhs) const
    {
        return reference(_pointer, _pointer->operator[](rhs), _i);
    }

    bit_iterator& operator++()
    {
        ++_i;
        return *this;
    }

    bit_iterator& operator--()
    {
        --_i;
        return *this;
    }

    bit_iterator operator++(int)
    {
        bit_iterator r(*this);
        this->operator++();
        return r;
    }

    bit_iterator operator--(int)
    {
        bit_iterator r(*this);
        this->operator--();
        return r;
    }

    difference_type operator-(const bit_iterator& rhs) const
    {
        return _i - rhs._i;
    }

    bit_iterator operator+(difference_type rhs) const
    {
        return bit_iterator(_pointer, _i + rhs);
    }

    bit_iterator operator-(difference_type rhs) const
    {
        return bit_iterator(_pointer, _i - rhs);
    }

    friend bit_iterator operator+(difference_type lhs, const bit_iterator& rhs)
    {
        return bit_iterator(rhs._pointer, lhs + rhs._i);
    }

    friend bit_iterator operator-(difference_type lhs, const bit_iterator& rhs)
    {
        return bit_iterator(rhs._pointer, lhs - rhs._i);
    }

    bool operator==(const bit_iterator& rhs) const
    {
        return _i == rhs._i;
    }

    bool operator!=(const bit_iterator& rhs) const
    {
        return _i != rhs._i;
    }

    bool operator>(const bit_iterator& rhs) const
    {
        return _i > rhs._i;
    }

    bool operator<(const bit_iterator& rhs) const
    {
        return _i < rhs._i;
    }

    bool operator>=(const bit_iterator& rhs) const
    {
        return _i >= rhs._i;
    }

    bool operator<=(const bit_iterator& rhs) const
    {
        return _i <= rhs._i;
    }
};


template <typename Bits>
class const_bit_iterator
{
    const Bits* _pointer;
    size_t _i;

public:

    using iterator_category = std::random_access_iterator_tag;
    using value_type = bool;
    using reference = typename Bits::const_reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    const_bit_iterator()
        : _pointer(nullptr)
        , _i(0)
    {
    }

    const_bit_iterator(const Bits* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
    {
    }

    /* const_bit_iterator(const const_bit_iterator& rhs) */
    /*     : _pointer(rhs._pointer) */
    /*     , _i(rhs._i) */
    /* { */
    /* } */

    const_bit_iterator& operator+=(difference_type rhs)
    {
        _i += rhs;
        return *this;
    }

    const_bit_iterator& operator-=(difference_type rhs)
    {
        _i -= rhs;
        return *this;
    }

    reference operator*() const
    {
        return reference(_pointer, _pointer->read(_i), _i);
    }

    reference operator[](difference_type rhs) const
    {
        return reference(_pointer, _pointer->read(rhs), _i);
    }

    const_bit_iterator& operator++()
    {
        ++_i;
        return *this;
    }

    const_bit_iterator& operator--()
    {
        --_i;
        return *this;
    }

    const_bit_iterator operator++(int)
    {
        const_bit_iterator r(*this);
        this->operator++();
        return r;
    }

    const_bit_iterator operator--(int)
    {
        const_bit_iterator r(*this);
        this->operator--();
        return r;
    }

    difference_type operator-(const const_bit_iterator& rhs) const
    {
        return _i - rhs._i;
    }

    const_bit_iterator operator+(difference_type rhs) const
    {
        return const_bit_iterator(_pointer, _i + rhs);
    }

    const_bit_iterator operator-(difference_type rhs) const
    {
        return const_bit_iterator(_pointer, _i - rhs);
    }

    friend const_bit_iterator operator+(difference_type lhs, const const_bit_iterator& rhs)
    {
        return const_bit_iterator(rhs._pointer, lhs + rhs._i);
    }

    friend const_bit_iterator operator-(difference_type lhs, const const_bit_iterator& rhs)
    {
        return const_bit_iterator(rhs._pointer, lhs - rhs._i);
    }

    bool operator==(const const_bit_iterator& rhs) const
    {
        return _i == rhs._i;
    }

    bool operator!=(const const_bit_iterator& rhs) const
    {
        return _i != rhs._i;
    }

    bool operator>(const const_bit_iterator& rhs) const
    {
        return _i > rhs._i;
    }

    bool operator<(const const_bit_iterator& rhs) const
    {
        return _i < rhs._i;
    }

    bool operator>=(const const_bit_iterator& rhs) const
    {
        return _i >= rhs._i;
    }

    bool operator<=(const const_bit_iterator& rhs) const
    {
        return _i <= rhs._i;
    }
};

} /* culcompact */
