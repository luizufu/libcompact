#pragma once

#include <cstddef>

namespace culcompact
{

template <typename Bits>
class bit_reference
{
    Bits* _pointer;
    bool _bit;
    size_t _i;

public:

    bit_reference(Bits* pointer, bool bit, size_t i)
        : _pointer(pointer)
        , _bit(bit)
        , _i(i)
    {
    }

    operator bool() const
    {
        return _bit;
    }

    bit_reference& operator=(bit_reference other)
    {
        return operator=(other._bit);
    }

    bit_reference& operator=(bool bit)
    {
        _bit = bit;
        if(bit)
            _pointer->set_bit(_i);
        else
            _pointer->clear_bit(_i);

        return *this;
    }
};


// a proxy reference that enable only access to a bit
template <typename Bits>
class const_bit_reference
{
    const Bits* _pointer;
    bool _bit;
    size_t _i;

public:


    const_bit_reference(const Bits* pointer, bool bit, size_t i)
        : _pointer(pointer)
        , _bit(bit)
        , _i(i)
    {
    }

    operator bool() const
    {
        return _bit;
    }

    const_bit_reference& operator=(const_bit_reference other) = delete;
};

} /* culcompact */
