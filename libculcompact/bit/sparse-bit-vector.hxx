#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/bit/plain-bit-vector.hxx>
#include <libculcompact/bit/bit-reference.hxx>
#include <libculcompact/bit/bit-iterator.hxx>
#include <libculcompact/detail/math.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <libculcompact/detail/binary-search.hxx>
#include <array>
#include <vector>
#include <cmath>
#include <type_traits>
#include <algorithm>
#include <numeric>
#include <tuple>
#include <stdexcept>
#include <iostream>

namespace culcompact
{

// stores a static sequence of bits in a sparse format, the struct only stores ones
// the representation split the bit position in lower parts or size R and higher parts
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class sparse_bit_vector
{
    template <typename>
    friend class bit_iterator;
    template <typename>
    friend class const_bit_iterator;

    // TODO fixed_int_vector
    std::vector<size_t> _lower;
    plain_bit_vector<T> _higher;
    typename plain_bit_vector<T>::extension1 _higher_ext;
    size_t _n = 0;
    size_t _r = 0;

    // returns the number of bits set from position 0 to i,
    // and the limits for the binary search
    // it uses a binary search strategy
    std::tuple<size_t, size_t, size_t> rank1_impl(size_t i) const
    {
        if(i == 0 || _lower.size() == 0)
            return { 0, 0, 0 };

        // split i in higher and lower parts
        size_t hi = i >> _r;
        size_t li = i & ((1u << _r) - 1u);

        // find the limits for the binary search
        // for this, find the block of ones between two zeros in _higher
        size_t p1 = _higher_ext.select0(hi) - hi;
        size_t p2 = _higher_ext.select0(hi + 1) - (hi + 1);

        size_t j =  detail::lower_bound(p1, p2,
            [li, this](size_t i) {
                return _lower[i] <= li;
            });

        return { j, p1, p2 };
    }

    // give the limits p1 and p2, the selected 1-bit j and lower bits of i
    bool is_one(size_t p1, size_t p2, size_t j, size_t li) const
    {
        return j > p1 && j <= p2 && _lower[j - 1] == li;
    }

    // select the position of the j-th one, the fist zero has j = 1
    // uses a binary search on rank operation
    size_t select1_impl(size_t j) const
    {
        if(j == 0)
            return 0;

        if(j > count())
            return size() + 1;

        // find higher and lower values to find the position of j-th bit set
        size_t h = _higher_ext.select1(j) - j;
        size_t l = _lower[j - 1];

        // combine higher and lower
        return (h << _r) + l;
    }

    // returns position in _higher of the before last bit set
    size_t higher_bit_set_before_last() const
    {
        if(_lower.size() <= 1)
            return 0;

        return _higher_ext.select1(count() - 1);
    }

    // returns position in _higher of the last bit set
    size_t last_bit_set() const
    {
        if(_lower.size() == 0)
            return 0;

        return select1_impl(count());
    }


public:

    class extension1;
    using iterator = bit_iterator<sparse_bit_vector<T>>;
    using const_iterator = const_bit_iterator<sparse_bit_vector<T>>;
    using reference = bit_reference<sparse_bit_vector<T>>;
    using const_reference = const_bit_reference<sparse_bit_vector<T>>;


    sparse_bit_vector() = default;

    sparse_bit_vector(size_t n, size_t k = CULCOMPACT_DEFAULT_K)
        : _higher(0)
        , _higher_ext(_higher, k)
        , _n(n)
    {
    }

    // constructs a sequence of bits from a pair of iterators, where elements are castable to bool
    template <detail::BoolIterable It>
    sparse_bit_vector(It begin, It end, size_t k = CULCOMPACT_DEFAULT_K)
        : _higher(0)
        , _higher_ext(_higher, k)
        , _n(std::distance(begin, end))
    {

        size_t m = 0;
        size_t n = 0;
        for(auto it = begin; it != end; ++it, ++n)
            m += static_cast<bool>(*it);

        _r = std::log2(n / static_cast<double>(m));
        _lower.resize(m);
        _higher.resize(m + (n >> _r), 0);

        size_t i = 1;
        size_t j = 0;
        size_t mask = (1u << _r) - 1u;
        for(It it = begin; it != end; ++it, ++i)
        {
            if(static_cast<bool>(*it))
            {
                _lower[j] = i & mask;
                _higher.set_bit((i >> _r) + j);
                ++j;
            }
        }

        _higher_ext.update();
    }

    // constructs a sequence of bits from an initializer_list with elements castable to bool
    template <detail::BoolConvertible Bool>
    sparse_bit_vector(std::initializer_list<Bool> list, size_t k = CULCOMPACT_DEFAULT_K)
        : sparse_bit_vector(list.begin(), list.end(), k)
    {
    }

    // reads bit at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        if(_lower.size() == 0 || i > last_bit_set())
        {
            return reference(this, false, i);
        }
        else
        {
            ++i;
            // do binary search
            auto [rank, p1, p2] = rank1_impl(i);
            size_t li = i & ((1ul << _r) - 1);

            // return 1 if it was found inside limits
            // and it is in fact one (the lower parts are also equal
            return reference(this, is_one(p1, p2, rank, li), i);
        }
    }

    // reads bit at position i, this is a throwing and non-modifiable version
    const_reference read_bit(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        if(_lower.size() == 0 || i > last_bit_set())
        {
            return const_reference(this, false, i);
        }
        else
        {
            i++;
            // do binary search
            auto [rank, p1, p2] = rank1_impl(i);
            size_t li = i & ((1ul << _r) - 1);

            // return 1 if it was found inside limits
            // and it is in fact one (the lower parts are also equal
            return const_reference(this, is_one(p1, p2, rank, li), i);
        }
    }


    // reads as an integer the bits in range (from, to]
    uint64_t read_int(size_t from, size_t to) const
    {
        if(from >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(from, size()));

        if(to > size())
            throw std::out_of_range(detail::make_out_of_range_msg(to, size()));

        if(from > to)
            throw std::range_error(detail::make_range_error_msg(from, to));

        if(from == to)
            return 0;

        // do binary search
        auto [rank, p1, p2] = rank1_impl(from + 1);
        size_t li = (from + 1) & ((1ul << _r) - 1);

        // sum the 1 bits inside range (from, to]
        uint64_t value =  is_one(p1, p2, rank, li) << (to - from - 1);
        size_t next_one;
        while((next_one = select1_impl(++rank) - 1) < to)
            value |= T{1} << (to - next_one - 1);

        return value;
    }

    // inserts a bit with value b at the end
    // TODO probably not working
    void push_back(bool bit)
    {
        if(bit)
        {
            // push  lower part to _lower
            _lower.push_back(_n & ((1ull << _r) - 1));

            // push higher part to _higher add necessary zeros before
            size_t k = (_n >> _r) + _lower.size();
            _higher.resize(k + 1, false);
            _higher.set_bit(k);
            _higher_ext.update();
        }

        ++_n;
    }

    // removes the last bit
    // TODO probably not working
    void pop_back()
    {
        // if last bit is one
        if(!_lower.empty() && (_n - 1) == last_bit_set())
        {
            // remove last bit and all trailing zeros
            size_t k = ((_n - 1) >> _r) + _lower.size();
            _higher.clear_bit(k);
            if(count() > 1)
                _higher.resize(higher_bit_set_before_last() + 1);
            else
                _higher.resize(0);

            // update higher bitvector and remove last lower part
            _higher_ext.update();
            _lower.pop_back();
        }

        --_n;
    }

    // resizes the struture in order to accomodate n bits,
    // if n > size() then the value of b is used to fill new positions
    // TODO probably not working
    void resize(size_t n, bool bit = false)
    {
        if(n == 0)
        {
            _lower.resize(0);
            _higher.resize(0);
        }
        else if(n > _n && bit)
        {
            size_t block_size = (1ull << _r);
            size_t last_hi = ((_n - 1) >> _r);
            size_t next_hi = (_n >> _r);

            // bits to be added after the corresponding _n-1 bit in _higher
            size_t ones_to_add = n - _n;
            size_t zeros_to_add = next_hi == last_hi ?
                std::ceil(ones_to_add / static_cast<double>(block_size)) - 1 :
                std::ceil(ones_to_add / static_cast<double>(block_size));

            // fill with zeros until first one position
            size_t k = (_n >> _r) + _lower.size();
            if(k > _higher.size())
                _higher.resize(k, false);

            // keep some info before resizing
            size_t ones = _lower.size();
            size_t hbits = _higher.size();
            size_t next_lvalue = _lower.back() + 1;

            // fill _lower with n-_n zeros and _higher with sufficient ones
            _lower.resize(_lower.size() + ones_to_add);
            _higher.resize(_higher.size() + ones_to_add + zeros_to_add, true);

            // fill values for current block if it has space left
            if(next_hi == last_hi)
            {
                // compute start and begin indices of current block
                size_t istart = ones;
                size_t iend = ones + (block_size - next_lvalue);
                auto lstart = _lower.begin() + istart;
                auto lend = iend < _lower.size() ?
                    _lower.begin() + iend : _lower.end();

                // fill ascending low values
                std::iota(lstart, lend, next_lvalue);

                // update new last positions
                ones += block_size - next_lvalue;
                hbits += block_size - next_lvalue;
            }


            // fill values for whole subsequent blocks
            for(size_t i = 0; i < zeros_to_add; ++i)
            {
                // compute start and begin indices of next block
                size_t istart = ones + (block_size * i);
                size_t iend = ones + (block_size * (i + 1));
                auto lstart = _lower.begin() + istart;
                auto lend = iend < _lower.size() ?
                    _lower.begin() + iend : _lower.end();

                // fill ascending low values
                std::iota(lstart, lend, 0);

                // clear the starting position of next blocks)
                _higher.clear_bit(hbits + ((block_size + 1) * i));
            }
        }
        else if(n < _n)
        {
            auto [rank, p1, p2] = rank1_impl(n - 1);

            // resize properly so to left only number of ones same as rank
            if(_lower.size() != rank)
            {
                _lower.resize(rank);

                size_t hsize = rank > 0 ? _higher_ext.select1(rank) + 1 : 0;
                _higher.resize(hsize);
            }
        }

        _higher_ext.update();
        _n = n;
    }

    // preallocates memory to accomodate n bits if necessary
    //
    // TODO implementation...
    void reserve([[maybe_unused]] size_t n)
    {
    }

    // returns the number of elements in the bit_vector
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _lower.size() * sizeof(size_t) + _higher.size_of() + sizeof(_n) + sizeof(_r);
    }

    // returns the popcount
    size_t count() const
    {
        return _lower.size();
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


// implements the binary search strategy for rank and select0, and the constant strategy for select
template <typename T>
class sparse_bit_vector<T>::extension1
{
    sparse_bit_vector& _bit_vector;

public:
    extension1(sparse_bit_vector& bit_vector)
        : _bit_vector(bit_vector)
    {
        update();
    }

    // reconstruct structures so that rank/select operations remain consistent
    void update()
    {
        // do not need to reconstruct anything actually
        /* _bit_vector._higher_ext.update(); */
    }

    // returns the number of bits cleared from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t rank0(size_t i) const
    {
        return i - rank1(i);
    }

    // returns the number of bits set from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t rank1(size_t i) const
    {
        return std::get<0>(_bit_vector.rank1_impl(i));
    }

    // select the position of the j-th zero, the fist zero has j = 1
    // uses a binary search on rank operation
    size_t select0(size_t j) const
    {
        if(j == 0)
            return 0;

        if(j > (_bit_vector.size() - _bit_vector.count()))
            return _bit_vector.size() + 1;

        size_t i = detail::lower_bound(0, _bit_vector.count() + 1,
            [j, this](size_t k) {
                return (select1(k) - k) < j;
            });

        return i + j - 1;
    }

    // select the position of the j-th one, the fist zero has j = 1
    // uses a binary search on rank operation
    size_t select1(size_t j) const
    {
        return _bit_vector.select1_impl(j);
    }
};


// iterates through sparse_bit_vector maintaining the position of the next bit set, allows bit modification while iterating
template <typename T>
class bit_iterator<sparse_bit_vector<T>>
{
    sparse_bit_vector<T>* _pointer;
    size_t _i;
    size_t _next_one;
    size_t _j;

public:

    using iterator_category = std::forward_iterator_tag;
    using value_type = bool;
    using reference = typename sparse_bit_vector<T>::reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    bit_iterator()
        : _pointer(nullptr)
        , _i(0)
    {
    }

    bit_iterator(sparse_bit_vector<T>* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
        , _next_one(pointer->select1_impl(1) - 1)
        , _j(1)
    {
    }

    reference operator*() const
    {
        return reference(_pointer, _i == _next_one, _i);
    }

    bit_iterator& operator++()
    {
        ++_i;

        if(_i > _next_one)
            _next_one = _pointer->select1_impl(++_j) - 1;

        return *this;
    }

    bit_iterator operator++(int)
    {
        bit_iterator r(*this);
        this->operator++();
        return r;
    }

    difference_type operator-(const bit_iterator& r) const
    {
        return _i - r._i;
    }

    bool operator!=(const bit_iterator& r) const
    {
        return _i != r._i;
    }

    bool operator==(const bit_iterator& r) const
    {
        return _i == r._i;
    }
};


// iterates through sparse_bit_vector maintaining the position of the next bit set, do not allow bit modification while iterating
template <typename T>
class const_bit_iterator<sparse_bit_vector<T>>
{
    const sparse_bit_vector<T>* _pointer;
    size_t _i;
    size_t _next_one;
    size_t _j;

public:

    using iterator_category = std::forward_iterator_tag;
    using value_type = bool;
    using reference = typename sparse_bit_vector<T>::const_reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    const_bit_iterator()
        : _pointer(nullptr)
        , _i(0)
    {
    }

    const_bit_iterator(const sparse_bit_vector<T>* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
        , _next_one(pointer->select1_impl(1) - 1)
        , _j(1)
    {
    }

    reference operator*() const
    {
        return reference(_pointer, _i == _next_one, _i);
    }

    const_bit_iterator& operator++()
    {
        ++_i;

        if(_i > _next_one)
            _next_one = _pointer->select1_impl(++_j) - 1;

        return *this;
    }

    const_bit_iterator operator++(int)
    {
        const_bit_iterator r(*this);
        this->operator++();
        return r;
    }

    difference_type operator-(const const_bit_iterator& r) const
    {
        return _i - r._i;
    }

    bool operator!=(const const_bit_iterator& r) const
    {
        return _i != r._i;
    }

    bool operator==(const const_bit_iterator& r) const
    {
        return _i == r._i;
    }
};

} /* culcompact */
