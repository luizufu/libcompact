#pragma once

#include <libculcompact/config.hxx>
#include <libculcompact/aux/word.hxx>
#include <libculcompact/bit/bit-reference.hxx>
#include <libculcompact/bit/bit-iterator.hxx>
#include <libculcompact/int/fixed-int-array.hxx>
#include <libculcompact/detail/iterable-concept.hxx>
#include <libculcompact/detail/binary-search.hxx>
#include <vector>
#include <cmath>
#include <type_traits>
#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace culcompact
{

// stores a modifying sequence of bits in a plain format that concatenates words of type T to abstract the whole sequence
template <typename T = CULCOMPACT_DEFAULT_INT_TYPE>
class plain_bit_vector
{
    template <typename>
    friend class bit_iterator;
    template <typename>
    friend class const_bit_iterator;

    static constexpr size_t W = sizeof(T) * 8;
    std::vector<word<T>> _words;
    size_t _n = 0;

public:

    class extension1;
    using iterator = bit_iterator<plain_bit_vector<T>>;
    using const_iterator = const_bit_iterator<plain_bit_vector<T>>;
    using reference = bit_reference<plain_bit_vector<T>>;
    using const_reference = const_bit_reference<plain_bit_vector<T>>;


    plain_bit_vector() = default;

    // constructs a sequence with n bits of value b
    plain_bit_vector(size_t n, bool b = false)
        : _words(std::ceil(n / static_cast<double>(W)), b ? ~T{0} : T{0})
        , _n(n)
    {
    }

    plain_bit_vector(size_t n, T integer)
        : _words(std::ceil(n / static_cast<double>(W)), 0)
        , _n(n)
    {
        _words.back() = word<T>(integer << (W - (n % W)));
    }

    // constructs a sequence of bits from a pair of iterators, where elements are castable to bool
    template <detail::BoolIterable It>
    plain_bit_vector(It begin, It end)
        : plain_bit_vector(std::distance(begin, end))
    {
        size_t i = 0;
        for(It it = begin; it != end; ++it, ++i)
            if(static_cast<bool>(*it))
                set_bit(i);
    }

    // constructs a sequence of bits from an initializer_list with elements castable to bool
    template <detail::BoolConvertible Bool>
    plain_bit_vector(std::initializer_list<Bool> list)
        : plain_bit_vector(list.begin(), list.end())
    {
    }


    // reads bit at position i, this is a non-thowing and modifiable version
    reference operator[](size_t i)
    {
        return reference(this, _words[i / W].read_bit(W - (i % W) - 1), i);
    }


    // sets bit at position i
    void set_bit(size_t i)
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        _words[i / W].set_bit(W - (i % W) - 1);
    }

    // clears bit at position i
    void clear_bit(size_t i)
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        _words[i / W].clear_bit(W - (i % W) - 1);
    }

    // toggles bit at position i
    void toggle_bit(size_t i)
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        _words[i / W].toggle_bit(W - (i % W) - 1);
    }

    // reads bit at position i, this is a throwing and non-modifiable version
    const_reference read_bit(size_t i) const
    {
        if(i >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(i, size()));

        return const_reference(this,_words[i / W].read_bit(W - (i % W) - 1), i);
    }

    void set()
    {
        for(size_t i = 0; i < _words.size(); ++i)
            _words[i].write_int(0, W, ~T{0});
    }

    void clear()
    {
        for(size_t i = 0; i < _words.size(); ++i)
            _words[i].write_int(0, W, T{0});
    }

    // writes an integer value as a sequence of bits in range (from, to],
    // if value do not fill the range entirely, remaining bits will be cleared
    void write_int(size_t from, size_t to, uint64_t value)
    {
        if(from >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(from, size()));

        if(to > size())
            throw std::out_of_range(detail::make_out_of_range_msg(to, size()));

        if(from > to)
            throw std::range_error(detail::make_range_error_msg(from, to));

        if(from == to)
            return;

        // if interval is within the same word just write to corresponding bits
        if(from / W == (to - 1) / W)
        {
            _words[from / W].write_int(
                W - (((to - 1) % W) + 1), W - (from % W),
                value);
        }
        // if not, copy higher or lower bits of value to corresponding words
        else
        {
            _words[from / W].write_int(
                0, W - (from % W),
                value >> (((to - 1) % W) + 1));

            _words[(to - 1) / W].write_int(
                W - ((to - 1) % W + 1), W,
                value & ((1ull << (((to - 1) % W) + 1)) - 1));
        }
    }

    // reads as an integer the bits in range (from, to]
    uint64_t read_int(size_t from, size_t to) const
    {
        if(from >= size())
            throw std::out_of_range(detail::make_out_of_range_msg(from, size()));

        if(to > size())
            throw std::out_of_range(detail::make_out_of_range_msg(to, size()));

        if(from > to)
            throw std::range_error(detail::make_range_error_msg(from, to));

        if(from == to)
            return 0;

        // if interval is within the same word just read the corresponding bits
        if(from / W == (to - 1) / W)
        {
            return _words[from / W].read_int(
                W - (((to - 1) % W) + 1), W - (from % W));
        }
        // if not, sum higher and lower bits of corresponding words' bit
        else
        {
            return 0
                | (_words[from / W].read_int(0, W - (from % W))
                    << (((to - 1) % W) + 1))
                | _words[(to - 1) / W].read_int(W - ((to - 1) % W + 1), W);
        }
    }

    // inserts a bit with value b at the end
    void push_back(bool b)
    {
        if(_n + 1 > _words.size() * W)
            _words.emplace_back();

        // increment before so set_bit/clear_bit do not thow
        ++_n;

        if(b)
            set_bit(_n - 1);
        else
            clear_bit(_n - 1);

    }

    // removes the last bit
    void pop_back()
    {
        clear_bit(_n - 1);

        // decrement after so clear_bit do not thow
        --_n;

        if(_n <= (_words.size() - 1) * W)
            _words.pop_back();
    }

    // resizes the struture in order to accomodate n bits,
    // if n > size() then the value of b is used to fill new positions
    void resize(size_t n, bool b = false)
    {
        if(n > _n && _n % W != 0)
        {
            size_t to = W - (_n % W);
            size_t l = std::min(to, n - _n);

            uint64_t value = b ? (1ull << l) - 1 : 0;
            _words[(_n - 1) / W].write_int(to - l, to, value);
        }

        size_t new_size = std::ceil(n / static_cast<double>(W));
        if(new_size != _words.size())
            _words.resize(new_size, word<T>((b ? ~T{0} : 0)));

        _n = n;
    }

    // preallocates memory to accomodate n bits if necessary
    void reserve(size_t n)
    {
        _words.reserve(std::ceil(n / static_cast<double>(W)));
    }

    // returns the number of elements
    size_t size() const
    {
        return _n;
    }

    // returns the size in bytes
    size_t size_of() const
    {
        return _words.size() * sizeof(_words) + sizeof(_n);
    }

    // returns the popcount
    size_t count() const
    {
        size_t c = 0;
        for(size_t i = 0; i < _words.size() - 1; ++i)
            c += _words[i].count();

        c += _words.back().count(W - (((_n-1) % W) + 1), W);

        return c;
    }

    // returns a modifiable iterator to the first bit
    iterator begin()
    {
        return iterator(this, 0);
    }

    // returns a modifiable iterator after the last bit
    iterator end()
    {
        return iterator(this, size());
    }

    // returns a non-modifiable iterator to the first bit
    const_iterator begin() const
    {
        return const_iterator(this, 0);
    }

    // returns a non-modifiable iterator after the last bit
    const_iterator end() const
    {
        return const_iterator(this, size());
    }
};


// implements the sampled pointers strategy for rank and the binary search strategy for select
template <typename T>
class plain_bit_vector<T>::extension1
{
    static constexpr size_t W = sizeof(T) * 8;

    const plain_bit_vector& _bit_vector;
    fixed_int_array<T> _ranks;
    size_t _k;

    // returns the popcount of the bit_vector inside (from, ..., to]
    size_t rank1_slow(size_t from, size_t to) const
    {
        size_t sum = 0;

        // sum popcount of whole words
        size_t w = from / W;
        for(; w < (to - 1) / W; ++w)
            sum += _bit_vector._words[w].count();

        // sum popcount of bits until last position
        sum +=_bit_vector._words[w].count(W - (((to - 1) % W) + 1), W);

        return sum;
    }

public:

    extension1(const plain_bit_vector& bit_vector, size_t k = CULCOMPACT_DEFAULT_K)
        : _bit_vector(bit_vector)
        , _ranks(
            std::ceil(bit_vector.size() / static_cast<double>(k)),
            std::floor(std::log2(static_cast<double>(bit_vector.size()))) + 1)
        , _k(k)
    {
        update();
    }

    // reconstruct structures so that rank/select operations remain consistent
    void update()
    {
        if(_bit_vector.size() == 0)
            return;

        /* _ranks.resize(std::ceil(_bit_vector.size() / (double) (_k * W))); */
        _ranks = fixed_int_array<T>(
            std::ceil(_bit_vector.size() / static_cast<double>(_k)),
            std::floor(std::log2(static_cast<double>(_bit_vector.size()))) + 1);

        size_t i = 0, total_rank = 0;
        for(bool bit : _bit_vector)
        {
            if(i % (_k * W) == 0)
                _ranks[i / (_k * W)] = total_rank;

            if(bit)
                ++total_rank;

            ++i;
        }
    }

    // returns the number of bits cleared from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t rank0(size_t i) const
    {
        return i - rank1(i);
    }

    // returns the number of bits set from position 0 to i
    // uses a single layer strategy of pointers to speed up operation
    size_t rank1(size_t i) const
    {
        if(i == 0)
            return 0;

        size_t p = (i - 1) / (_k * W);

        // sum cumulative rank of previous words and the popcount of the nexts
        return _ranks.read(p) + rank1_slow(p * (_k * W), i);
    }

    // select the position of the j-th zero, the fist zero has j = 1
    // uses a binary search on rank operation
    size_t select0(size_t j) const
    {
        size_t n = _bit_vector.size();

        return detail::lower_bound(0, n + 1,
            [j, this](size_t i) {
                return rank0(i) < j;
            });
    }

    // select the position of the j-th one, the fist one has j = 1
    // uses a binary search on rank operation
    size_t select1(size_t j) const
    {
        size_t n = _bit_vector.size();

        return detail::lower_bound(0, n + 1,
            [j, this](size_t i) {
                return rank1(i) < j;
            });
    }
};


// iterates through plain_bit_vector maintaining a mask to extract the corresponding bit of subsequent words, allows bit modification while iterating
template <typename T>
class bit_iterator<plain_bit_vector<T>>
{
    static constexpr size_t W = sizeof(T) * 8;

    plain_bit_vector<T>* _pointer;
    size_t _i;
    T _current_mask;

public:

    using iterator_category = std::forward_iterator_tag;
    using value_type = bool;
    using reference = typename plain_bit_vector<T>::reference;
    using pointer = reference*;
    using difference_type = std::ptrdiff_t;


    bit_iterator()
        : _pointer(nullptr)
        , _i(0)
        , _current_mask(0)
    {
    }

    bit_iterator(plain_bit_vector<T>* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
        , _current_mask(T{1} << (W - 1))
    {
    }

    reference operator*()
    {
        return reference(_pointer,
            (_pointer->_words[_i / W].read_int() & _current_mask), _i);
    }

    bit_iterator& operator++()
    {
        ++_i;
        _current_mask = _current_mask >> 1;

        if(_i % W == 0)
            _current_mask = T{1} << (W - 1);

        return *this;
    }

    bit_iterator operator++(int)
    {
        bit_iterator r(*this);
        this->operator++();
        return r;
    }

    difference_type operator-(const bit_iterator& r) const
    {
        return _i - r._i;
    }

    bool operator!=(const bit_iterator& r) const
    {
        return _i != r._i;
    }

    bool operator==(const bit_iterator& r) const
    {
        return _i == r._i;
    }
};


// iterates through plain_bit_vector vector maintaining a mask to extract the corresponding bit of subsequent words, do not allow bit modification while iterating
template <typename T>
class const_bit_iterator<plain_bit_vector<T>>
{
    static constexpr size_t W = sizeof(T) * 8;

    const plain_bit_vector<T>* _pointer;
    size_t _i;
    T _current_mask;

public:

    using iterator_category = std::forward_iterator_tag;
    using value_type = bool;
    using reference = typename plain_bit_vector<T>::const_reference;
    using pointer = void;
    using difference_type = std::ptrdiff_t;


    const_bit_iterator()
        : _pointer(nullptr)
        , _i(0)
        , _current_mask(0)
    {
    }

    const_bit_iterator(const plain_bit_vector<T>* pointer, size_t i)
        : _pointer(pointer)
        , _i(i)
        , _current_mask(T{1} << (W - 1))
    {
    }

    reference operator*() const
    {
        return reference( _pointer,
            (_pointer->_words[_i / W].read_int() & _current_mask), _i);
    }

    const_bit_iterator& operator++()
    {
        ++_i;
        _current_mask = _current_mask >> 1;

        if(_i % W == 0)
            _current_mask = T{1} << (W - 1);

        return *this;
    }

    const_bit_iterator operator++(int)
    {
        const_bit_iterator r(*this);
        this->operator++();
        return r;
    }

    difference_type operator-(const const_bit_iterator & r) const
    {
        return _i - r._i;
    }

    bool operator!=(const const_bit_iterator& r) const
    {
        return _i != r._i;
    }

    bool operator==(const const_bit_iterator& r) const
    {
        return _i == r._i;
    }
};

} /* culcompact */
